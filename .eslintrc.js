//eslint-env node
module.exports = {
  root: true,
  plugins: ['react', 'jsx-a11y', 'react-hooks'],
  extends: [
    'eslint:recommended',
    'plugin:react/all',
    'plugin:jsx-a11y/recommended',
    'plugin:import/recommended',
    'plugin:import/typescript',
    'plugin:prettier/recommended',
    'prettier',
  ],

  parser: '@typescript-eslint/parser', // Specifies the ESLint parser

  // parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      impliedStrict: true,
    },
  },
  env: {
    es6: true,
    node: true,
    browser: true,
    jest: false,
  },

  settings: {
    react: { version: 'detect' },
    'import/ignore': ['\\.(md|svg|css)$'],
    'import/extensions': ['.js', '.jsx', '.ts', '.tsx'],
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
  },
  rules: {
    // links in comments easily break this rule
    'max-len': [
      'warn',
      {
        code: 100,
        ignoreComments: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
      },
    ],

    'no-unused-vars': ['warn', { ignoreRestSiblings: true }],
    'no-console': 'warn',
    'one-var': 0,
    'no-prototype-builtins': 0,

    'prettier/prettier': 0,
    'react/default-props-match-prop-types': [2, { allowRequiredDefaults: true }],
    'react/destructuring-assignment': [0, { ignoreClassFields: true }],
    'react/forbid-component-props': 0,
    'react/forbid-foreign-prop-types': 0,
    'react/forbid-prop-types': 0,
    'react/jsx-handler-names': 0,
    'react/jsx-filename-extension': 0,
    'react/jsx-child-element-spacing': 0,
    'react/jsx-curly-newline': 0,
    'react/jsx-indent-props': 0,
    'react/jsx-indent': 0,
    'react/jsx-max-depth': 0,
    'react/jsx-max-props-per-line': 0,
    'react/jsx-no-bind': [2, { allowBind: false, allowFunctions: true, allowArrowFunctions: true }],
    'react/jsx-no-literals': 0,
    'react/jsx-one-expression-per-line': 0,
    'react/jsx-props-no-spreading': 0,
    'react/jsx-sort-default-props': 0,
    'react/jsx-sort-props': 0,
    'react/no-adjacent-inline-elements': 0,
    'react/no-danger': 2,
    'react/no-deprecated': 2,
    'react/no-direct-mutation-state': 2,
    'react/no-multi-comp': 0,
    'react/no-set-state': 0,
    'react/no-redundant-should-component-update': 2,
    'react/no-render-return-value': 2,
    'react/no-string-refs': 2,
    'react/prefer-stateless-function': [1, { ignorePureComponents: true }],
    'react/prop-types': [2, { ignore: ['className', 'children'], skipUndeclared: false }],
    'react/react-in-jsx-scope': 2,
    'react/function-component-definition': 0,
    'react/require-default-props': 0,
    'react/sort-prop-types': 1,
    'react/state-in-constructor': 0,
    'react/void-dom-elements-no-children': 2,
    'react/sort-comp': [
      1,
      {
        order: [
          'props-definition',
          'static-methods',
          'instance-variables',
          'instance-methods',
          'everything-else',
          'lifecycle',
          'render',
        ],
        groups: {
          'props-definition': ['propTypes', 'defaultProps', 'contextType'],
        },
      },
    ],
    'jsx-a11y/label-has-for': [2, { allowChildren: true }],

    'import/default': 2,
    'import/no-absolute-path': 2,
    'import/no-amd': 2,
    'import/no-commonjs': 2,
    'import/no-cycle': 2,
    'import/no-dynamic-require': 2,
    'import/no-named-as-default': 0,
    'import/no-nodejs-modules': 2,
    'import/no-self-import': 2,
    'import/no-relative-parent-imports': 0,
    'import/no-unassigned-import': [2, { allow: ['**/*.css'] }],
    'import/no-unresolved': 2,
    'import/no-useless-path-segments': ['error', { noUselessIndex: true }],
    'import/no-webpack-loader-syntax': 2,
    'import/order': [
      'warn',
      {
        'newlines-between': 'always',
        groups: ['builtin', 'external', 'internal'],
        alphabetize: {
          order: 'ignore' /* sort in ascending order. Options: ['ignore', 'asc', 'desc'] */,
          caseInsensitive: true /* ignore case. Options: [true, false] */,
        },
        pathGroups: [
          {
            pattern: '*.css',
            group: 'external',
            position: 'before',
          },
          {
            pattern: 'react',
            group: 'external',
            position: 'before',
          },
          {
            pattern: 'ui/**',
            group: 'internal',
            position: 'before',
          },
          {
            pattern: 'components/**',
            group: 'internal',
            position: 'before',
          },
          // {
          //   pattern: 'const/**',
          //   group: 'internal',
          //   position: 'after',
          // },
        ],
      },
    ],
    'import/unambiguous': 2,

    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
  },
  overrides: [
    // ---------------- TypeScript -------------------------------------------------------------------------------
    {
      files: ['**/*.ts', '**/*.tsx'],

      extends: [
        'prettier', // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
      ],
      rules: {
        'no-unused-vars': 0, // it's bullshiting
        'react/prop-types': 0,
        '@typescript-eslint/no-explicit-any': 0,
      },
    },

    {
      files: ['**/*.mdx'],
      extends: ['plugin:mdx/recommended'],
      rules: {
        'max-len': 0,
      },
    },

    {
      files: ['**/__tests__/*.js', '**/__mocks__/*.js', '**/*.test.js'],
      env: { jest: true },
      rules: {
        'react/prop-types': 0,
      },
    },
    {
      files: ['*.config.js', '.*rc', '.*rc.js', 'jest/*', 'packages/*/jest/**'],
      env: { node: true },
      parserOptions: {
        sourceType: 'script',
      },
      rules: {
        'import/no-amd': 0,
        'import/no-commonjs': 0,
        'import/no-nodejs-modules': 0,
      },
    },
  ],
}
