import PropTypes from 'prop-types';
import * as React from 'react';
import React__default, { useContext, cloneElement, Children, useState, useCallback, useMemo } from 'react';
import CleaveInput from 'cleave.js/react';
import PhoneInput from 'react-phone-input-2';

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);

    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }

    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends$7() {
  _extends$7 = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends$7.apply(this, arguments);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;

  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _createSuper(Derived) {
  var hasNativeReflectConstruct = _isNativeReflectConstruct();

  return function _createSuperInternal() {
    var Super = _getPrototypeOf(Derived),
        result;

    if (hasNativeReflectConstruct) {
      var NewTarget = _getPrototypeOf(this).constructor;

      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return _possibleConstructorReturn(this, result);
  };
}

function _superPropBase(object, property) {
  while (!Object.prototype.hasOwnProperty.call(object, property)) {
    object = _getPrototypeOf(object);
    if (object === null) break;
  }

  return object;
}

function _get(target, property, receiver) {
  if (typeof Reflect !== "undefined" && Reflect.get) {
    _get = Reflect.get;
  } else {
    _get = function _get(target, property, receiver) {
      var base = _superPropBase(target, property);

      if (!base) return;
      var desc = Object.getOwnPropertyDescriptor(base, property);

      if (desc.get) {
        return desc.get.call(receiver);
      }

      return desc.value;
    };
  }

  return _get(target, property, receiver || target);
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}

function _iterableToArrayLimit(arr, i) {
  var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];

  if (_i == null) return;
  var _arr = [];
  var _n = true;
  var _d = false;

  var _s, _e;

  try {
    for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _createForOfIteratorHelper(o, allowArrayLike) {
  var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];

  if (!it) {
    if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
      if (it) o = it;
      var i = 0;

      var F = function () {};

      return {
        s: F,
        n: function () {
          if (i >= o.length) return {
            done: true
          };
          return {
            done: false,
            value: o[i++]
          };
        },
        e: function (e) {
          throw e;
        },
        f: F
      };
    }

    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  var normalCompletion = true,
      didErr = false,
      err;
  return {
    s: function () {
      it = it.call(o);
    },
    n: function () {
      var step = it.next();
      normalCompletion = step.done;
      return step;
    },
    e: function (e) {
      didErr = true;
      err = e;
    },
    f: function () {
      try {
        if (!normalCompletion && it.return != null) it.return();
      } finally {
        if (didErr) throw err;
      }
    }
  };
}

function _toPrimitive(input, hint) {
  if (typeof input !== "object" || input === null) return input;
  var prim = input[Symbol.toPrimitive];

  if (prim !== undefined) {
    var res = prim.call(input, hint || "default");
    if (typeof res !== "object") return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }

  return (hint === "string" ? String : Number)(input);
}

function _toPropertyKey(arg) {
  var key = _toPrimitive(arg, "string");

  return typeof key === "symbol" ? key : String(key);
}

var FormFieldEmitter = /*#__PURE__*/React__default.createContext({});
var FormPropsContext = /*#__PURE__*/React__default.createContext({
  disabled: false,
  readOnly: false,
  defaultValues: {}
});
var useFormProps = function useFormProps() {
  return useContext(FormPropsContext) || {
    defaultValues: {}
  };
};
var useFormValues = function useFormValues() {
  var _useFormProps = useFormProps(),
      fields = _useFormProps.fields;

  var entries = Object.entries(fields).map(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        name = _ref2[0],
        value = _ref2[1].value;

    return [name, value];
  });
  return Object.fromEntries(entries);
};
var useField = function useField(name) {
  var _useContext = useContext(FormPropsContext),
      fields = _useContext.fields;

  return fields && fields[name] || {};
};

/**
 *
 * @param  {...String|null|undefined|false} names
 * @returns {String}
 */
var cn = function cn() {
  for (var _len = arguments.length, names = new Array(_len), _key = 0; _key < _len; _key++) {
    names[_key] = arguments[_key];
  }

  return names.filter(function (value) {
    return !!value && typeof value === 'string';
  }).join(' ');
};
var isInputDateSupported = function isInputDateSupported() {
  if (typeof window === 'undefined' || typeof navigator === 'undefined') return false;
  var input = document.createElement('input');
  input.type = 'date';
  return input.type === 'date';
};

var ValidationMessage = React__default.forwardRef(function (_a, ref) {
    var className = _a.className, error = _a.error, success = _a.success, children = _a.children;
    if (error && success) {
        throw new Error('ValidationMessage can not be "error" and "success" at the same time');
    }
    return (React__default.createElement("div", { className: cn(className, 'validation-message', error && 'error', success && 'success'), ref: ref },
        React__default.createElement("span", null, children)));
});
ValidationMessage.displayName = 'ValidationMessage';

var _excluded$t = ["onSubmit", "children", "onChange", "redirect", "className", "validation", "readOnly", "defaultValues", "disabled"];
var Form = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(Form, _React$PureComponent);

  var _super = _createSuper(Form);

  function Form() {
    var _this;

    _classCallCheck(this, Form);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "element", /*#__PURE__*/React__default.createRef());

    _defineProperty(_assertThisInitialized(_this), "errorMessageRef", /*#__PURE__*/React__default.createRef());

    _defineProperty(_assertThisInitialized(_this), "fieldRefs", {});

    _defineProperty(_assertThisInitialized(_this), "setFieldStatus", function () {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref$value = _ref.value,
          value = _ref$value === void 0 ? null : _ref$value,
          name = _ref.name,
          _ref$enable = _ref.enable,
          enable = _ref$enable === void 0 ? true : _ref$enable,
          _ref$defaultValue = _ref.defaultValue,
          defaultValue = _ref$defaultValue === void 0 ? null : _ref$defaultValue,
          validationMessage = _ref.validationMessage,
          touched = _ref.touched,
          ref = _ref.ref;

      if (!name) throw 'field name is undefined';

      if (ref) {
        _this.fieldRefs[name] = ref;
      }

      return _this.updateFieldState(name, {
        value: value,
        touched: touched,
        validationMessage: validationMessage,
        defaultValue: defaultValue,
        enable: enable
      }).then(function () {
        _this.props.onChange && _this.props.onChange(_this.getValues());
      });
    });

    _defineProperty(_assertThisInitialized(_this), "state", {
      fields: {},
      pending: false,
      error: null,
      errors: {}
    });

    _defineProperty(_assertThisInitialized(_this), "handleSubmit", function (event) {
      event.preventDefault();
      event.stopPropagation();

      _this.validate().then(function () {
        return _this.doSubmit();
      }, function () {
        return _this.focusFirstInvalidField();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleReset", function () {
      Object.values(_this.fieldRefs).forEach(function (element) {
        var _element$reset;

        element === null || element === void 0 ? void 0 : (_element$reset = element.reset) === null || _element$reset === void 0 ? void 0 : _element$reset.call(element);
      });

      _this.resetValues();
    });

    return _this;
  }

  _createClass(Form, [{
    key: "doSubmit",
    value: function () {
      var _doSubmit = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var _this$props, submit, redirect, result, error, errors;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this$props = this.props, submit = _this$props.onSubmit, redirect = _this$props.redirect;
                _context.next = 3;
                return this.setStateAsync({
                  pending: true
                });

              case 3:
                _context.prev = 3;
                _context.next = 6;
                return submit(this.getValues());

              case 6:
                result = _context.sent;
                _context.next = 9;
                return this.resetTouched();

              case 9:
                _context.next = 11;
                return this.setStateAsync({
                  pending: false
                });

              case 11:
                redirect && redirect(result);
                _context.next = 23;
                break;

              case 14:
                _context.prev = 14;
                _context.t0 = _context["catch"](3);
                error = _context.t0.error;
                errors = _context.t0.errors;
                _context.next = 20;
                return this.setStateAsync({
                  pending: false
                });

              case 20:
                _context.next = 22;
                return this.setErrors(error, errors);

              case 22:
                this.scrollToTop();

              case 23:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[3, 14]]);
      }));

      function doSubmit() {
        return _doSubmit.apply(this, arguments);
      }

      return doSubmit;
    }()
    /**
     * @param {String} name
     * @param {{
     *  value:*,
     *  touched:Boolean,
     *  validationMessage:String,
     *  defaultValue:*,
     *  enable:Boolean
     * }} next
     */

  }, {
    key: "updateFieldState",
    value: function updateFieldState(name, _ref3) {
      var _this2 = this;

      var value = _ref3.value,
          touched = _ref3.touched,
          validationMessage = _ref3.validationMessage,
          defaultValue = _ref3.defaultValue,
          enable = _ref3.enable;
      // console.log(name, { value, touched, validationMessage, defaultValue, enable })
      if (this._unmounted) return Promise.resolve(); // must be a Promise

      return new Promise(function (resolve) {
        _this2.setState(function (state) {
          var _state$fields = state.fields,
              prev = _state$fields[name],
              fields = _objectWithoutProperties(_state$fields, [name].map(_toPropertyKey));

          if (!enable && !prev) {
            // was disabled, now disabled - nothing has changed
            resolve();
            return null;
          }

          if (prev && prev.value === value && prev.touched === touched && prev.validationMessage === validationMessage && prev.defaultValue === defaultValue && enable) {
            // when nothing has changed
            resolve();
            return null;
          }

          if (!enable) {
            // was enabled, now disabled - remove a field
            return {
              fields: fields
            };
          }

          var next = _objectSpread2(_objectSpread2({}, prev), {}, {
            value: value,
            touched: touched,
            validationMessage: validationMessage,
            defaultValue: defaultValue,
            enable: enable
          });

          return {
            fields: _objectSpread2(_objectSpread2({}, fields), {}, _defineProperty({}, name, next)),
            error: null
          };
        }, resolve);
      });
    }
  }, {
    key: "resetTouched",
    value: function resetTouched() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      if (this._unmounted) return Promise.resolve(); // must be a Promise

      return this.setStateAsync(function (state) {
        var entries = Object.entries(state.fields).map(function (_ref4) {
          var _ref5 = _slicedToArray(_ref4, 2),
              name = _ref5[0],
              fieldState = _ref5[1];

          return [name, _objectSpread2(_objectSpread2({}, fieldState), {}, {
            touched: value
          })];
        });
        return {
          fields: Object.fromEntries(entries)
        };
      });
    }
  }, {
    key: "resetValues",
    value: function resetValues() {
      if (this._unmounted) return Promise.resolve(); // must be a Promise

      return this.setStateAsync(function (state) {
        var entries = Object.entries(state.fields).map(function (_ref6) {
          var _ref7 = _slicedToArray(_ref6, 2),
              name = _ref7[0],
              fieldState = _ref7[1];

          return [name, _objectSpread2(_objectSpread2({}, fieldState), {}, {
            value: fieldState.defaultValue,
            touched: false
          })];
        });
        return {
          fields: Object.fromEntries(entries)
        };
      });
    }
  }, {
    key: "scrollToTop",
    value: function scrollToTop() {
      var _this$errorMessageRef;

      (_this$errorMessageRef = this.errorMessageRef.current) === null || _this$errorMessageRef === void 0 ? void 0 : _this$errorMessageRef.scrollIntoView({
        behavior: 'smooth'
      });
    }
  }, {
    key: "setErrors",
    value: function setErrors(error, errors) {
      if (this._unmounted) return Promise.resolve(); // must be a Promise

      if (!errors || !Object.keys(errors).length) {
        return this.setStateAsync(function () {
          return {
            error: error,
            pending: false
          };
        });
      }

      return this.setStateAsync(function (state) {
        var nextFields = _objectSpread2({}, state.fields);

        var unhandledMessages = [];
        Object.entries(errors).forEach(function (_ref8) {
          var _ref9 = _slicedToArray(_ref8, 2),
              name = _ref9[0],
              validationMessage = _ref9[1];

          if (nextFields.hasOwnProperty(name)) {
            nextFields[name] = _objectSpread2(_objectSpread2({}, nextFields[name]), {}, {
              validationMessage: validationMessage
            });
          } else {
            unhandledMessages.push("".concat(validationMessage, " (").concat(name, ")"));
          }
        });
        var nextState = {
          fields: nextFields,
          pending: false
        };

        if (unhandledMessages) {
          nextState.error = [error].concat(unhandledMessages).filter(Boolean).join(', ');
        } else {
          nextState.error = error;
        }

        return nextState;
      });
    }
  }, {
    key: "reset",
    value: function reset() {
      this.element.current && this.element.current.reset();
      this.handleReset();
    }
  }, {
    key: "getCustomValidationError",
    value: function getCustomValidationError() {
      var values = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValues();
      var validation = this.props.validation;
      return validation ? validation(values) : null;
    }
    /**
     * @returns {boolean}
     */

  }, {
    key: "isValid",
    value: function isValid() {
      var values = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValues();
      if (this.hasInvalidField()) return false;
      return !this.getCustomValidationError(values);
    }
    /**
     * @returns {boolean}
     */

  }, {
    key: "validate",
    value: function () {
      var _validate = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        var error;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.resetTouched(true);

              case 2:
                if (!this.hasInvalidField()) {
                  _context2.next = 4;
                  break;
                }

                throw new Error('fail');

              case 4:
                _context2.next = 6;
                return this.getCustomValidationError();

              case 6:
                error = _context2.sent;
                _context2.next = 9;
                return this.setStateAsync(function () {
                  return {
                    error: error
                  };
                });

              case 9:
                if (!error) {
                  _context2.next = 11;
                  break;
                }

                throw new Error('fail');

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function validate() {
        return _validate.apply(this, arguments);
      }

      return validate;
    }()
    /** @public */

  }, {
    key: "setValues",
    value: function setValues() {
      var _this3 = this;

      var values = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return Promise.all(Object.entries(values).map(function (_ref10) {
        var _ref11 = _slicedToArray(_ref10, 2),
            name = _ref11[0],
            value = _ref11[1];

        return _this3.setFieldStatus({
          value: value,
          name: name
        });
      }));
    }
    /** @public */

  }, {
    key: "getValues",
    value: function getValues() {
      var fields = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.fields;
      var entries = Object.entries(fields).map(function (_ref12) {
        var _ref13 = _slicedToArray(_ref12, 2),
            name = _ref13[0],
            fieldState = _ref13[1];

        return [name, fieldState.value];
      });
      return Object.fromEntries(entries);
    }
  }, {
    key: "hasInvalidField",
    value: function hasInvalidField() {
      var fields = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.fields;
      return Object.values(fields).some(function (fieldState) {
        return !!fieldState.validationMessage;
      });
    }
  }, {
    key: "getFirstInvalidFieldName",
    value: function getFirstInvalidFieldName() {
      var fields = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.fields;

      for (var _i = 0, _Object$entries = Object.entries(fields); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
            name = _Object$entries$_i[0],
            state = _Object$entries$_i[1];

        if (state.validationMessage) return name;
      }
    }
  }, {
    key: "hasModifiedField",
    value: function hasModifiedField() {
      var fields = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.fields;
      return Object.values(fields).some(function (_ref14) {
        var value = _ref14.value,
            defaultValue = _ref14.defaultValue;
        return value !== defaultValue;
      });
    }
  }, {
    key: "getFieldsValidity",
    value: function getFieldsValidity() {
      var fields = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.fields;
      var entries = Object.entries(fields).map(function (_ref15) {
        var _ref16 = _slicedToArray(_ref15, 2),
            name = _ref16[0],
            fieldState = _ref16[1];

        return [name, !fieldState.validationMessage];
      });
      return Object.fromEntries(entries);
    }
  }, {
    key: "focusFirstInvalidField",
    value: function focusFirstInvalidField() {
      var name = this.getFirstInvalidFieldName();
      var ref = this.fieldRefs[name];
      if (!ref) return;

      if (ref.focus) {
        return ref.focus();
      }

      if (ref.scrollIntoView) {
        return ref.scrollIntoView({
          behavior: 'smooth'
        });
      }
    }
  }, {
    key: "setStateAsync",
    value: function setStateAsync(state) {
      var _this4 = this;

      return Promise.race([new Promise(function (resolve) {
        _this4.setState(state, resolve);
      }), // not sure whether the React's setState
      // executes the callback when state has no changes
      new Promise(function (resolve) {
        return setTimeout(resolve, 1);
      })]);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this._unmounted = true;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props;
          _this$props2.onSubmit;
          var children = _this$props2.children;
          _this$props2.onChange;
          _this$props2.redirect;
          var className = _this$props2.className;
          _this$props2.validation;
          var readOnly = _this$props2.readOnly,
          defaultValues = _this$props2.defaultValues,
          disabled = _this$props2.disabled,
          props = _objectWithoutProperties(_this$props2, _excluded$t);

      var _this$state = this.state,
          error = _this$state.error,
          pending = _this$state.pending,
          fields = _this$state.fields,
          wasSubmitted = _this$state.wasSubmitted;
      var modified = this.hasModifiedField();
      var values = this.getValues();
      var valid = this.isValid(); // console.log('modified', modified)
      // console.log('values', values)
      // console.log('valid', valid)

      return /*#__PURE__*/React__default.createElement("form", _extends$7({}, props, {
        onSubmit: this.handleSubmit,
        ref: this.element,
        onReset: this.handleReset,
        className: cn(className, 'form'),
        noValidate: true
      }), /*#__PURE__*/React__default.createElement(ValidationMessage, {
        error: true,
        ref: this.errorMessageRef
      }, !this.hasInvalidField() && error || null), /*#__PURE__*/React__default.createElement(FormFieldEmitter.Provider, {
        value: this.setFieldStatus
      }, /*#__PURE__*/React__default.createElement(FormPropsContext.Provider, {
        // eslint-disable-next-line react/jsx-no-constructed-context-values
        value: {
          readOnly: readOnly,
          defaultValues: defaultValues,
          disabled: disabled,
          pending: pending,
          modified: modified,
          valid: valid,
          values: values,
          fields: fields,
          wasSubmitted: wasSubmitted
        }
      }, children)));
    }
  }]);

  return Form;
}(React__default.PureComponent);

_defineProperty(Form, "propTypes", {
  defaultValues: PropTypes.object,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func.isRequired,
  readOnly: PropTypes.bool,
  redirect: PropTypes.func,
  validation: PropTypes.func
});

_defineProperty(Form, "defaultProps", {
  defaultValues: {}
});

var IsModified = function IsModified(_ref) {
  var children = _ref.children,
      show = _ref.show,
      not = _ref.not;

  var _useFormProps = useFormProps(),
      modified = _useFormProps.modified;

  var result = show || modified;

  if (not) {
    result = !result;
  }

  return result ? children : false;
};
IsModified.propTypes = {
  show: PropTypes.bool
};

var IsValid = function IsValid(_ref) {
  var children = _ref.children;

  var _useFormProps = useFormProps(),
      valid = _useFormProps.valid;

  return valid ? children : false;
};

var IsInvalid = function IsInvalid(_ref) {
  var children = _ref.children;

  var _useFormProps = useFormProps(),
      valid = _useFormProps.valid;

  return valid ? false : children;
};

var IsFieldValue = function IsFieldValue(_ref) {
  var name = _ref.name,
      blank = _ref.blank,
      equals = _ref.equals,
      not = _ref.not,
      oneOf = _ref.oneOf,
      children = _ref.children;
  var field = useField(name);
  var result = false;

  if (blank) {
    result = !!field.value;
  }

  if (equals !== undefined) {
    result = result || field.value === equals;
  }

  if (Array.isArray(oneOf)) {
    result = result || oneOf.includes(field.value);
  }

  if (not) {
    result = !result;
  }

  return result ? children : false;
};
IsFieldValue.propTypes = {
  blank: PropTypes.bool,
  equals: PropTypes.any,
  // field name
  name: PropTypes.string.isRequired,
  not: PropTypes.bool,
  oneOf: PropTypes.arrayOf(PropTypes.any)
};
IsFieldValue.defaultProps = {
  blank: false,
  not: false
};

var createMatchValidatior = function createMatchValidatior(matchValue, name) {
  return function (value) {
    return value === matchValue ? null : "Must match ".concat(name);
  };
};

var AddMatchFieldValidation = function AddMatchFieldValidation(_ref) {
  var name = _ref.name,
      label = _ref.label,
      children = _ref.children;

  var _useField = useField(name),
      value = _useField.value;

  return /*#__PURE__*/cloneElement(Children.only(children), {
    validation: createMatchValidatior(value, label)
  });
};
AddMatchFieldValidation.propTypes = {
  children: PropTypes.node.isRequired,
  label: PropTypes.node.isRequired,
  name: PropTypes.string.isRequired
};

var FormControl = function FormControl(_ref) {
  var children = _ref.children;
  var values = useFormValues();
  return children({
    values: values
  });
};
FormControl.propTypes = {
  children: PropTypes.func.isRequired
};

var IsFieldModified = function IsFieldModified(_ref) {
  var name = _ref.name,
      not = _ref.not,
      children = _ref.children;

  var _useField = useField(name),
      value = _useField.value,
      defaultValue = _useField.defaultValue;

  var result = value !== defaultValue;

  if (not) {
    result = !result;
  }

  return result ? children : null;
};
IsFieldModified.propTypes = {
  // field name
  name: PropTypes.string.isRequired,
  not: PropTypes.bool
};

var BaseInput = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(BaseInput, _React$PureComponent);

  var _super = _createSuper(BaseInput);

  function BaseInput() {
    var _this;

    _classCallCheck(this, BaseInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "recentState", {
      value: undefined,
      valid: undefined,
      enable: undefined,
      validationMessage: undefined,
      defaultValue: null,
      touched: false
    });

    return _this;
  }

  _createClass(BaseInput, [{
    key: "checkCustomValidity",
    value:
    /**
     * @param {*} [value]
     * @returns {String}
     */
    function checkCustomValidity() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValue();
      var validation = this.props.validation;
      return validation ? validation(value) || '' : '';
    }
    /**
     * Returns true when field must be included into form validation
     * @param {Object} [props]
     * @param {Boolean} props.disabled
     * @param {Boolean} props.readOnly
     * @param {String} props.name
     * @param {Boolean}
     */

  }, {
    key: "willValidate",
    value: function willValidate() {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.props,
          disabled = _ref.disabled,
          name = _ref.name;

      return !!name && !disabled;
    }
    /**
     * @abstract
     */

  }, {
    key: "setValue",
    value: function setValue(value) {
      this.broadcastUpdates({
        value: value
      });
    }
  }, {
    key: "value",
    get: function get() {
      return this.getValue();
    }
    /**
     * @abstract
     */
    ,
    set: function set(val) {
      this.setValue(val);
    }
  }, {
    key: "getValue",
    value: function getValue() {
      throw 'define .getValue()';
    }
    /**
     * Trigger native validation
     * Sets <state.touched> = true
     */

  }, {
    key: "validate",
    value: function validate() {
      this.broadcastUpdates({
        touched: true
      });
    }
    /**
     * @abstract
     * @param {*} value
     * @returns {boolean}
     */

  }, {
    key: "isValid",
    value: function isValid() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValue();
      if (!this.willValidate()) return true;

      if (this.props.required) {
        return value !== null && value !== undefined && value !== '';
      }

      return true;
    }
    /**
     * @protected
     * @param {BroadcastUpdatesConfig} config
     */

  }, {
    key: "broadcastUpdates",
    value: function broadcastUpdates() {
      var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref2$value = _ref2.value,
          value = _ref2$value === void 0 ? this.getValue() : _ref2$value,
          _ref2$valid = _ref2.valid,
          valid = _ref2$valid === void 0 ? this.isValid(value) : _ref2$valid,
          _ref2$enable = _ref2.enable,
          enable = _ref2$enable === void 0 ? this.willValidate() : _ref2$enable,
          _ref2$validationMessa = _ref2.validationMessage,
          validationMessage = _ref2$validationMessa === void 0 ? this.getValidationMessage() : _ref2$validationMessa,
          _ref2$defaultValue = _ref2.defaultValue,
          defaultValue = _ref2$defaultValue === void 0 ? this.props.defaultValue : _ref2$defaultValue,
          _ref2$touched = _ref2.touched,
          touched = _ref2$touched === void 0 ? this.recentState.touched : _ref2$touched;

      var emitter = this.context;

      if (!emitter) {
        throw new Error("Missing statusListener");
      }

      if (value !== this.recentState.value || valid !== this.recentState.valid || enable !== this.recentState.enable || validationMessage !== this.recentState.validationMessage || defaultValue !== this.recentState.defaultValue || touched !== this.recentState.touched) {
        var _this$props$onChange, _this$props;

        var name = this.props.name;
        this.recentState = {
          value: value,
          valid: valid,
          enable: enable,
          validationMessage: validationMessage,
          defaultValue: defaultValue,
          touched: touched
        };
        emitter(_objectSpread2(_objectSpread2({}, this.recentState), {}, {
          name: name,
          ref: this
        }));
        (_this$props$onChange = (_this$props = this.props).onChange) === null || _this$props$onChange === void 0 ? void 0 : _this$props$onChange.call(_this$props, _objectSpread2({
          target: this,
          name: name
        }, this.recentState));
      }
    }
    /**
     * @protected
     */

  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      if (!this.willValidate() || this.isValid()) return null;
      var required = this.props.required;
      var value = this.getValue();

      if (value === null) {
        return required ? 'The field is required' : null;
      }

      return null;
    }
  }, {
    key: "reset",
    value: function reset() {
      this.setValue(this.props.defaultValue);
    }
  }, {
    key: "focus",
    value: function focus() {
      throw new Error('focus() not implemented');
    }
  }, {
    key: "scrollIntoView",
    value: function scrollIntoView() {
      throw new Error('scrollIntoView() not implemented');
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (prevProps && this.props.defaultValue !== prevProps.defaultValue) {
        this.setValue(this.props.defaultValue);
      } else {
        this.broadcastUpdates();
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.broadcastUpdates({
        enable: false
      });
    }
  }]);

  return BaseInput;
}(React__default.PureComponent);
/**
 * @typedef {Object} BroadcastUpdatesConfig
 * @prop {*} value - current user input (inc. invalid bullshit)
 * @prop {Boolean} [valid]
 * @prop {String} [name]
 * @prop {Boolean} [enable]
 * @prop {Boolean} [touched]
 * @prop {String} [validationMessage]
 */

_defineProperty(BaseInput, "propTypes", {
  defaultValue: PropTypes.any,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  validation: PropTypes.func
});

_defineProperty(BaseInput, "defaultProps", {});

_defineProperty(BaseInput, "contextType", FormFieldEmitter);

var BaseHtmlValidationInput = /*#__PURE__*/function (_BaseInput) {
  _inherits(BaseHtmlValidationInput, _BaseInput);

  var _super = _createSuper(BaseHtmlValidationInput);

  function BaseHtmlValidationInput() {
    var _this;

    _classCallCheck(this, BaseHtmlValidationInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "element", null);

    _defineProperty(_assertThisInitialized(_this), "setElement", function (element) {
      _this.element = element;
      if (!element) return;
      var value = _this.props.defaultValue || null; // update the DOM element validity

      _this.resetCustomValidity({
        value: value
      }); // let the parent form knows about this control


      _this.broadcastUpdates({
        value: value
      });
    });

    return _this;
  }

  _createClass(BaseHtmlValidationInput, [{
    key: "willValidate",
    value: function willValidate() {
      return this.props.name && !this.props.disabled;
    }
    /**
     * @override
     * @returns {boolean}
     */

  }, {
    key: "isValid",
    value: function isValid() {
      if (!this.element) return false;
      if (!this.element.validity.valid) return false;
      if (this.checkCustomValidity()) return false;
      return true;
    }
  }, {
    key: "resetCustomValidity",
    value: function resetCustomValidity() {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref$value = _ref.value,
          value = _ref$value === void 0 ? this.getValue() : _ref$value;

      var current = this.getValidationMessage();
      var willValidate = this.willValidate();

      if (!willValidate) {
        current && this.element.setCustomValidity('');
        return;
      }

      var customValidationMessage = this.checkCustomValidity(value);

      if (customValidationMessage) {
        this.element.setCustomValidity(customValidationMessage);
        return;
      }

      var validationMessage = this.props.validationMessage;

      if (validationMessage && validationMessage !== current && validationMessage !== this.recentState.validationMessage) {
        this.element.setCustomValidity(validationMessage);
        return;
      }

      this.element.setCustomValidity('');
    }
  }, {
    key: "checkValidityProps",
    value: function checkValidityProps(props) {
      var validationMessages = this.props.validationMessages;
      var validity = this.element.validity;
      var invalidOverriddenProp = validationMessages && props.find(function (prop) {
        return validity[prop] && validationMessages[prop];
      });
      var htmlValidationMessage = invalidOverriddenProp ? validationMessages[invalidOverriddenProp] : this.element.validationMessage;
      return htmlValidationMessage || null;
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      return this.checkValidityProps(VALIDITY_PROPS$2) || this.checkCustomValidity() || null;
    }
  }, {
    key: "focus",
    value: function focus() {
      this.element.focus();
    }
  }, {
    key: "scrollIntoView",
    value: function scrollIntoView() {
      var _this$element;

      (_this$element = this.element).scrollIntoView.apply(_this$element, arguments);
    }
  }, {
    key: "getValue",
    value: function getValue() {
      if (!this.element) return undefined;
      var value = String(this.element.value).trim();
      return value === '' ? null : value;
    }
    /**
     * @override
     */

  }, {
    key: "setValue",
    value: function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!this.element) return;
      this.element.value = value;
      this.resetCustomValidity();
      this.broadcastUpdates();
    }
    /**
     * @public
     * @override
     */

  }, {
    key: "validate",
    value: function validate() {
      this.resetCustomValidity();
      this.broadcastUpdates({
        touched: true
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      this.resetCustomValidity();

      _get(_getPrototypeOf(BaseHtmlValidationInput.prototype), "componentDidUpdate", this).call(this, prevProps, prevState);
    }
  }]);

  return BaseHtmlValidationInput;
}(BaseInput);

_defineProperty(BaseHtmlValidationInput, "propTypes", _objectSpread2(_objectSpread2({}, BaseInput.propTypes), {}, {
  validationMessage: PropTypes.string,
  validationMessages: PropTypes.shape({
    valueMissing: PropTypes.string,
    typeMismatch: PropTypes.string,
    patternMismatch: PropTypes.string,
    tooLong: PropTypes.string,
    tooShort: PropTypes.string
  })
}));

var VALIDITY_PROPS$2 = ['valueMissing', 'badInput', 'typeMismatch', 'patternMismatch', 'rangeOverflow', 'rangeUnderflow', 'stepMismatch', 'tooLong', 'tooShort'];

var _excluded$s = ["validation", "validationMessage", "validationMessages"];
var NumberInput = /*#__PURE__*/function (_BaseHtmlValidationIn) {
  _inherits(NumberInput, _BaseHtmlValidationIn);

  var _super = _createSuper(NumberInput);

  function NumberInput() {
    var _this;

    _classCallCheck(this, NumberInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "handleBlur", function (e) {
      _this.validate();

      _this.props.onBlur && _this.props.onBlur(e);
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (event) {
      var value = event.target.value; // update the DOM element validity

      _this.resetCustomValidity({
        value: value
      }); // let the parent form knows about changes


      _this.broadcastUpdates();
    });

    return _this;
  }

  _createClass(NumberInput, [{
    key: "getValidationMessage",
    value:
    /** @override */
    function getValidationMessage() {
      return this.checkValidityProps([VALIDITY_PROPS$1]) || this.checkCustomValidity();
    }
    /**
     * @override
     */

  }, {
    key: "setValue",
    value: function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!this.element) return;
      this.element.value = value;
      this.broadcastUpdates({
        value: this.getValue()
      });
    }
    /**
     * @override
     */

  }, {
    key: "getValue",
    value: function getValue() {
      if (!this.element) return undefined;
      var textValue = String(this.element.value).trim();
      if (textValue === '') return null;
      var number = parseFloat(this.element.value);
      return Number.isNaN(number) ? undefined : number;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props;
          _this$props.validation;
          _this$props.validationMessage;
          _this$props.validationMessages;
          var props = _objectWithoutProperties(_this$props, _excluded$s);

      return /*#__PURE__*/React__default.createElement("input", _extends$7({}, props, {
        type: "number",
        ref: this.setElement,
        id: props.id || props.name,
        onBlur: this.handleBlur,
        onChange: this.handleChange
      }));
    }
  }]);

  return NumberInput;
}(BaseHtmlValidationInput);

_defineProperty(NumberInput, "propTypes", {
  autoComplete: PropTypes.string,
  defaultValue: PropTypes.number,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  validation: PropTypes.func,
  validationMessages: PropTypes.exact({
    valueMissing: PropTypes.string,
    badInput: PropTypes.string,
    rangeOverflow: PropTypes.string,
    rangeUnderflow: PropTypes.string,
    stepMismatch: PropTypes.string
  })
});

_defineProperty(NumberInput, "defaultProps", {
  autoComplete: 'off'
});

var VALIDITY_PROPS$1 = ['valueMissing', 'badInput', 'rangeOverflow', 'rangeUnderflow', 'stepMismatch'];

var _excluded$r = ["value", "defaultValue"],
    _excluded2$6 = ["validation", "validationMessage", "defaultValue", "value", "className"];
var FormattedInput = /*#__PURE__*/function (_BaseInput) {
  _inherits(FormattedInput, _BaseInput);

  var _super = _createSuper(FormattedInput);

  function FormattedInput() {
    var _this;

    _classCallCheck(this, FormattedInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "element", null);

    _defineProperty(_assertThisInitialized(_this), "setElement", function (element) {
      // console.log('setElement', this.props.name)
      _this.element = element;
    });

    _defineProperty(_assertThisInitialized(_this), "cleaveInstance", null);

    _defineProperty(_assertThisInitialized(_this), "setCleaveInstance", function (instance) {
      // console.log('setCleaveInstance', this.props.name)
      _this.cleaveInstance = instance;

      _this.broadcastUpdates();
    });

    _defineProperty(_assertThisInitialized(_this), "onBlur", function (e) {
      _this.validate();

      _this.props.onBlur && _this.props.onBlur(e);
    });

    _defineProperty(_assertThisInitialized(_this), "onChange", function (event) {
      var _event$target = event.target,
          rawValue = _event$target.rawValue,
          value = _event$target.value; // let the parent form knows about changes

      if (_this.getNormalizedValue(value) === null) {
        _this.broadcastUpdates({
          value: null
        });
      } else {
        _this.broadcastUpdates({
          value: rawValue
        });
      }
    });

    return _this;
  }

  _createClass(FormattedInput, [{
    key: "reset",
    value: function reset() {
      var _this2 = this;

      this.setState({
        touched: false
      }, function () {
        _this2.setValue(_this2.getDefaultValue());
      });
    }
  }, {
    key: "setValue",
    value: function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!this.cleaveInstance) return;
      var normalValue = this.getNormalizedValue(value); // if (normalValue === null || normalValue === undefined) {
      //   this.element.value = ''
      // } else {

      this.cleaveInstance.setRawValue(normalValue); // }

      var validationMessage = this.getValidationMessage(normalValue);
      this.broadcastUpdates({
        value: normalValue,
        validationMessage: validationMessage,
        valid: !validationMessage
      });
    }
    /**
     * @override
     */

  }, {
    key: "getValue",
    value: function getValue() {
      return this.getNormalizedValue(this.cleaveInstance ? this.cleaveInstance.getRawValue() : undefined);
    }
  }, {
    key: "getNormalizedValue",
    value: function getNormalizedValue(value) {
      if (value === undefined || value === null) {
        return null;
      }

      if (typeof value === 'string' && String(value).trim() === '') {
        return null;
      }

      if (this.props.options.numeral) {
        return typeof value === 'number' ? value : parseFloat(value.replace(/[^0-9.]/g, ''), 10);
      }

      return value;
    }
  }, {
    key: "getDefaultValue",
    value: function getDefaultValue() {
      var defaultValue = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.props.defaultValue;
      return this.getNormalizedValue(defaultValue);
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValue();
      if (!this.willValidate()) return null;
      var _this$props = this.props,
          required = _this$props.required,
          validation = _this$props.validation;

      if (value === null && required) {
        return 'The field is required';
      }

      return validation && validation(value) || null;
    }
  }, {
    key: "broadcastUpdates",
    value: function broadcastUpdates() {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref$value = _ref.value,
          value = _ref$value === void 0 ? this.getValue() : _ref$value,
          _ref$defaultValue = _ref.defaultValue,
          defaultValue = _ref$defaultValue === void 0 ? this.getDefaultValue() : _ref$defaultValue,
          props = _objectWithoutProperties(_ref, _excluded$r);

      return _get(_getPrototypeOf(FormattedInput.prototype), "broadcastUpdates", this).call(this, _objectSpread2(_objectSpread2({}, props), {}, {
        value: this.getNormalizedValue(value),
        defaultValue: this.getNormalizedValue(defaultValue)
      }));
    }
  }, {
    key: "validate",
    value: function validate() {
      this.broadcastUpdates({
        touched: true
      });
    }
  }, {
    key: "focus",
    value: function focus() {
      var _this$element;

      (_this$element = this.element) === null || _this$element === void 0 ? void 0 : _this$element.focus();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props;
          _this$props2.validation;
          _this$props2.validationMessage;
          _this$props2.defaultValue;
          _this$props2.value;
          var className = _this$props2.className,
          props = _objectWithoutProperties(_this$props2, _excluded2$6);

      return /*#__PURE__*/React__default.createElement(CleaveInput, _extends$7({}, props, {
        className: cn('cleave', className),
        htmlRef: this.setElement,
        onInit: this.setCleaveInstance,
        id: props.id || props.name,
        onBlur: this.onBlur,
        onChange: this.onChange,
        value: this.getDefaultValue()
      }));
    }
  }]);

  return FormattedInput;
}(BaseInput);

_defineProperty(FormattedInput, "propTypes", _objectSpread2(_objectSpread2(_objectSpread2({}, BaseInput.propTypes), CleaveInput.propTypes), {}, {
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  validation: PropTypes.func
}));

_defineProperty(FormattedInput, "defaultProps", {
  type: 'text',
  autoComplete: 'off'
});

var _excluded$q = ["validation", "validationMessages", "validationMessage"];
var DATE_SUPPORTED = isInputDateSupported();

var NativeDateInput = /*#__PURE__*/function (_BaseHtmlValidationIn) {
  _inherits(NativeDateInput, _BaseHtmlValidationIn);

  var _super = _createSuper(NativeDateInput);

  function NativeDateInput() {
    var _this;

    _classCallCheck(this, NativeDateInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "handleBlur", function (e) {
      _this.validate();

      _this.props.onBlur && _this.props.onBlur(e);
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function () {
      // update the DOM element validity
      _this.resetCustomValidity(); // let the parent form knows about changes


      _this.broadcastUpdates();
    });

    return _this;
  }

  _createClass(NativeDateInput, [{
    key: "setValue",
    value:
    /**
     * @override
     */
    function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!this.element) return;
      this.element.value = value;
      this.resetCustomValidity();
      this.broadcastUpdates();
    }
    /**
     * @override
     */

  }, {
    key: "getValue",
    value: function getValue() {
      if (!this.element) return undefined;
      if (!this.element.value) return null;
      var date = this.element.valueAsDate === undefined ? new Date(this.element.value) : this.element.valueAsDate;
      return date ? date.toISOString().replace(/T.*$/, '') : null;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props;
          _this$props.validation;
          _this$props.validationMessages;
          _this$props.validationMessage;
          var props = _objectWithoutProperties(_this$props, _excluded$q);

      return /*#__PURE__*/React__default.createElement("input", _extends$7({}, props, {
        type: "date",
        ref: this.setElement,
        id: props.id || props.name,
        onBlur: this.handleBlur,
        onChange: this.handleChange
      }));
    }
  }]);

  return NativeDateInput;
}(BaseHtmlValidationInput);

_defineProperty(NativeDateInput, "propTypes", {
  autoComplete: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  validation: PropTypes.func,
  validationMessages: PropTypes.exact({
    valueMissing: PropTypes.string,
    badInput: PropTypes.string,
    rangeOverflow: PropTypes.string,
    rangeUnderflow: PropTypes.string,
    stepMismatch: PropTypes.string
  })
});

_defineProperty(NativeDateInput, "defaultProps", {
  autoComplete: 'off',
  validationMessages: {}
});

var PseudoDateInput = /*#__PURE__*/function (_FormattedInput) {
  _inherits(PseudoDateInput, _FormattedInput);

  var _super2 = _createSuper(PseudoDateInput);

  function PseudoDateInput() {
    _classCallCheck(this, PseudoDateInput);

    return _super2.apply(this, arguments);
  }

  _createClass(PseudoDateInput, [{
    key: "getValue",
    value: function getValue() {
      var value = this.cleaveInstance.getISOFormatDate() || this.cleaveInstance.getRawValue() || null;
      return value;
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      var value = this.getValue();
      var _this$props2 = this.props,
          required = _this$props2.required,
          min = _this$props2.min,
          max = _this$props2.max;

      if (value === null) {
        return required ? 'Required' : null;
      }

      var date = new Date(value);

      if (isNaN(date.valueOf())) {
        return 'Invalid date';
      }

      if (min && date < new Date(min)) {
        return "Must be greater than or equals to ".concat(min);
      }

      if (max && date > new Date(max)) {
        return "Must be lesser than or equals to ".concat(max);
      }

      return this.checkCustomValidity();
    }
  }, {
    key: "getNormalizedValue",
    value: function getNormalizedValue(date) {
      if (!date) return null;

      if (date instanceof Date) {
        return formatDate(date);
      }

      if (typeof date === 'string' && /^\d{4}-\d{2}-\d{2}/.test(date)) {
        return formatDate(new Date(date));
      }

      return null;
    }
  }]);

  return PseudoDateInput;
}(FormattedInput);

_defineProperty(PseudoDateInput, "defaultProps", {
  placeholder: 'mm/dd/yyyy',
  options: {
    date: true,
    datePattern: ['m', 'd', 'Y']
  }
});

var DateInput = DATE_SUPPORTED ? NativeDateInput : PseudoDateInput;

var formatDate = function formatDate(date) {
  return date instanceof Date ? //Cleave needs format '01/01/2000' '1/01/2000' doesn't work correctly
  date.toLocaleDateString('en-US', {
    month: '2-digit',
    day: '2-digit',
    year: 'numeric'
  }) : null;
};

var address = ['street-address', // A street address. This can be multiple lines of text, and should fully identify the location of the address within its second administrative level (typically a city or town), but should not include the city name, ZIP or postal code, or country name.
'address-line1', // Each individual line of the street address. These should only be present if the "street-address" is also present.
'address-line2', // Each individual line of the street address. These should only be present if the "street-address" is also present.
'address-line3', // Each individual line of the street address. These should only be present if the "street-address" is also present.
'address-level4', // The finest-grained administrative level, in addresses which have four levels.
'address-level3', // The third administrative level, in addresses with at least three administrative levels.
'address-level2', // The second administrative level, in addresses with at least two of them. In countries with two administrative levels, this would typically be the city, town, village, or other locality in which the address is located.
'address-level1', // The first administrative level in the address. This is typically the province in which the address is located. In the United States, this would be the state. In Switzerland, the canton. In the United Kingdom, the post town.
'country', // A country code.
'country-name', // A country name.
'postal-code' // A postal code (in the United States, this is the ZIP code).
];
var billingAddress = address.map(function (x) {
  return "billing ".concat(x);
});
var cc = ['cc-name', // The full name as printed on or associated with a payment instrument such as a credit card. Using a full name field is preferred, typically, over breaking the name into pieces.
'cc-given-name', // A given (first) name as given on a payment instrument like a credit card.
'cc-additional-name', // A middle name as given on a payment instrument or credit card.
'cc-family-name', // A family name, as given on a credit card.
'cc-number', // A credit card number or other number identifying a payment method, such as an account number.
'cc-exp', // A payment method expiration date, typically in the form "MM/YY" or "MM/YYYY".
'cc-exp-month', // The month in which the payment method expires.
'cc-exp-year', // The year in which the payment method expires.
'cc-csc', // The security code for the payment instrument; on credit cards, this is the 3-digit verification number on the back of the card.
'cc-type' // The type of payment instrument (such as "Visa" or "Master Card").
];
var name = ['name', 'honorific-prefix', 'given-name', 'additional-name', 'family-name', // The family (or "last") name.
'honorific-suffix', // The suffix, such as "Jr.", "B.Sc.", "PhD.", "MBASW", or "IV".
'nickname' // A nickname or handle.
];
var password = ['password', 'one-time-code', 'new-password', // A new password. When creating a new account or changing passwords, this should be used for an "Enter your new password" or "Confirm new password" field, as opposed to a general "Enter your current password" field that might be present. This may be used by the browser both to avoid accidentally filling in an existing password and to offer assistance in creating a secure password (see also Preventing autofilling with autocomplete="new-password").
'current-password'];
var validValues = ['on', 'off', 'username', // A username or account name.
'email', // An email address.
'email username', // A username which equals email.
'organization-title', // A job title, or the title a person has within an organization, such as "Senior Technical Writer", "President", or "Assistant Troop Leader".
'organization'].concat(cc, name, address, _toConsumableArray(billingAddress), password, ['transaction-currency', // The currency in which the transaction is to take place.
'transaction-amount', // The amount, given in the currency specified by "transaction-currency", of the transaction, for a payment form.
'language', // A preferred language, given as a valid BCP 47 language tag.
'bday', // A birth date, as a full date.
'bday-day', // The day of the month of a birth date.
'bday-month', // The month of the year of a birth date.
'bday-year', // The year of a birth date.
'sex', // A gender identity (such as "Female", "Fa'afafine", "Male"), as freeform text without newlines.
'tel', // A full telephone number, including the country code. If you need to break the phone number up into its components, you can use these values for those fields:
'tel-country-code', // The country code, such as "1" for the United States, Canada, and other areas in North America and parts of the Caribbean.
'tel-national', // The entire phone number without the country code component, including a country-internal prefix. For the phone number "1-855-555-6502", this field's value would be "855-555-6502".
'tel-area-code', // The area code, with any country-internal prefix applied if appropriate.
'tel-local', // The phone number without the country or area code. This can be split further into two parts, for phone numbers which have an exchange number and then a number within the exchange. For the phone number "555-6502", use "tel-local-prefix" for "555" and "tel-local-suffix" for "6502".
'tel-extension', // A telephone extension code within the phone number, such as a room or suite number in a hotel or an office extension in a company.
'impp', // A URL for an instant messaging protocol endpoint, such as "xmpp:username@example.net".
'url', // A URL, such as a home page or company web site address as appropriate given the context of the other fields in the form.
'photo' // The URL of an image representing the person, company, or contact information given in the other fields in the form.
]);
var AutoComplete = PropTypes.oneOf(validValues);

var _excluded$p = ["validation", "validationMessages", "validationMessage"];
var TextInput = /*#__PURE__*/function (_BaseHtmlValidationIn) {
  _inherits(TextInput, _BaseHtmlValidationIn);

  var _super = _createSuper(TextInput);

  function TextInput() {
    var _this;

    _classCallCheck(this, TextInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "handleBlur", function (e) {
      _this.validate();

      _this.props.onBlur && _this.props.onBlur(e);
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (event) {
      var value = event.target.value; // update the DOM element validity

      _this.resetCustomValidity({
        value: value
      }); // let the parent form knows about changes


      _this.broadcastUpdates();
    });

    return _this;
  }

  _createClass(TextInput, [{
    key: "isValid",
    value:
    /**
     * @override
     * @returns {boolean}
     */
    function isValid() {
      //! with 'minLength' ValidityState.tooShort returns false-positive validity in Chrome
      if (isInputValueTooShort(this.props.minLength, this.element)) {
        return false;
      }

      return _get(_getPrototypeOf(TextInput.prototype), "isValid", this).call(this);
    }
    /**
     * @override
     */

  }, {
    key: "setValue",
    value: function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!this.element) return;
      this.element.value = value;
      this.resetCustomValidity();
      this.broadcastUpdates();
    }
    /**
     * @override
     */

  }, {
    key: "getValue",
    value: function getValue() {
      if (!this.element) return undefined;
      var value = ['confirmPassword', 'password'].includes(this.props.name) ? this.element.value : String(this.element.value).trim();
      return String(value).trim() === '' ? null : value;
    }
  }, {
    key: "checkCustomValidity",
    value: function checkCustomValidity() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValue();
      return _get(_getPrototypeOf(TextInput.prototype), "checkCustomValidity", this).call(this, value) || isInputValueTooShort(this.props.minLength, this.element) && 'Too short' || '';
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      return this.checkValidityProps(VALIDITY_PROPS) || this.checkCustomValidity();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props;
          _this$props.validation;
          _this$props.validationMessages;
          _this$props.validationMessage;
          var props = _objectWithoutProperties(_this$props, _excluded$p);

      return /*#__PURE__*/React__default.createElement("input", _extends$7({}, props, {
        ref: this.setElement,
        id: props.id || props.name,
        onBlur: this.handleBlur,
        onChange: this.handleChange
      }));
    }
  }]);

  return TextInput;
}(BaseHtmlValidationInput);

_defineProperty(TextInput, "propTypes", _objectSpread2(_objectSpread2({}, BaseHtmlValidationInput.propTypes), {}, {
  autoComplete: AutoComplete,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  type: PropTypes.oneOf(['text', 'email', 'tel', 'search', 'password', 'url']),
  validationMessages: PropTypes.exact({
    valueMissing: PropTypes.string,
    typeMismatch: PropTypes.string,
    patternMismatch: PropTypes.string,
    tooLong: PropTypes.string,
    tooShort: PropTypes.string
  })
}));

_defineProperty(TextInput, "defaultProps", {
  type: 'text',
  validationMessages: {}
});

var VALIDITY_PROPS = ['valueMissing', 'typeMismatch', 'patternMismatch', 'tooLong', 'tooShort'];

function isInputValueTooShort(minLength, element) {
  return Number.isFinite(+minLength) && element.value.length < +minLength;
}

var _excluded$o = ["validation"];
var HiddenInput = /*#__PURE__*/function (_BaseInput) {
  _inherits(HiddenInput, _BaseInput);

  var _super = _createSuper(HiddenInput);

  function HiddenInput() {
    var _this;

    _classCallCheck(this, HiddenInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "element", null);

    _defineProperty(_assertThisInitialized(_this), "setElement", function (element) {
      _this.element = element;
      if (!element) return;
      var value = _this.props.defaultValue || null; // let the parent form knows about this control

      _this.broadcastUpdates({
        value: value
      });
    });

    return _this;
  }

  _createClass(HiddenInput, [{
    key: "isValid",
    value: function isValid() {
      return true;
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      return null;
    }
  }, {
    key: "getValue",
    value: function getValue() {
      if (!this.element) return undefined;
      var value = String(this.element.value).trim();
      return value === '' ? null : value;
    }
    /**
     * @override
     */

  }, {
    key: "setValue",
    value: function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!this.element) return;
      this.element.value = value;
      this.broadcastUpdates();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props;
          _this$props.validation;
          var props = _objectWithoutProperties(_this$props, _excluded$o);

      return /*#__PURE__*/React__default.createElement("input", _extends$7({}, props, {
        type: "hidden",
        ref: this.setElement,
        id: props.id || props.name
      }));
    }
  }]);

  return HiddenInput;
}(BaseInput);

_defineProperty(HiddenInput, "propTypes", {
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired
});

_defineProperty(HiddenInput, "defaultProps", {});

var _excluded$n = ["validation", "validationMessages", "validationMessage"];
var TextArea = /*#__PURE__*/function (_BaseHtmlValidationIn) {
  _inherits(TextArea, _BaseHtmlValidationIn);

  var _super = _createSuper(TextArea);

  function TextArea() {
    var _this;

    _classCallCheck(this, TextArea);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "handleBlur", function (e) {
      _this.validate();

      _this.props.onBlur && _this.props.onBlur(e);
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (event) {
      var value = event.target.value; // update the DOM element validity

      _this.resetCustomValidity({
        value: value
      }); // let the parent form knows about changes


      _this.broadcastUpdates();
    });

    return _this;
  }

  _createClass(TextArea, [{
    key: "setValue",
    value:
    /**
     * @override
     */
    function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!this.element) return;
      this.element.value = value;
      this.resetCustomValidity();
      this.broadcastUpdates();
    }
    /**
     * @override
     */

  }, {
    key: "getValue",
    value: function getValue() {
      if (!this.element) return undefined;
      var value = String(this.element.value).trim();
      return value === '' ? null : value;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props;
          _this$props.validation;
          _this$props.validationMessages;
          _this$props.validationMessage;
          var props = _objectWithoutProperties(_this$props, _excluded$n);

      return /*#__PURE__*/React__default.createElement("textarea", _extends$7({}, props, {
        ref: this.setElement,
        id: props.id || props.name,
        onBlur: this.handleBlur,
        onChange: this.handleChange
      }));
    }
  }]);

  return TextArea;
}(BaseHtmlValidationInput);

_defineProperty(TextArea, "propTypes", _objectSpread2(_objectSpread2({}, BaseHtmlValidationInput.propTypes), {}, {
  autoComplete: AutoComplete,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  validationMessages: PropTypes.exact({
    valueMissing: PropTypes.string,
    typeMismatch: PropTypes.string,
    patternMismatch: PropTypes.string,
    tooLong: PropTypes.string,
    tooShort: PropTypes.string
  })
}));

_defineProperty(TextArea, "defaultProps", {
  validationMessages: {}
});

var _excluded$m = ["disabled", "validation", "validationMessage", "options", "placeholder", "readOnly", "disabledOptions", "className"];
var Select$1 = /*#__PURE__*/function (_BaseHtmlValidationIn) {
  _inherits(Select, _BaseHtmlValidationIn);

  var _super = _createSuper(Select);

  function Select() {
    var _this;

    _classCallCheck(this, Select);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "onBlur", function (e) {
      _this.validate();

      _this.props.onBlur && _this.props.onBlur(e);
    });

    _defineProperty(_assertThisInitialized(_this), "onChange",
    /** @type {impoSyntheticEvent} */
    function (event) {
      var value = event.target.value; // update the DOM element validity

      _this.resetCustomValidity({
        value: value
      }); // let the parent form knows about changes


      _this.broadcastUpdates({
        value: value
      });
    });

    return _this;
  }

  _createClass(Select, [{
    key: "setValue",
    value:
    /** @override */
    function setValue(value) {
      if (!this.element) return;
      this.element.value = value;
      this.broadcastUpdates({
        value: value || null
      });
    }
  }, {
    key: "getValue",
    value: function getValue() {
      return this.element.value || null;
    }
  }, {
    key: "renderOption",
    value: function renderOption(_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          value = _ref2[0],
          label = _ref2[1];

      var disabled = this.props.disabledOptions.includes(value);
      return /*#__PURE__*/React__default.createElement("option", {
        value: value,
        key: this.props.name + '-option-' + value,
        disabled: disabled
      }, label);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled;
          _this$props.validation;
          _this$props.validationMessage;
          var options = _this$props.options,
          placeholder = _this$props.placeholder,
          readOnly = _this$props.readOnly;
          _this$props.disabledOptions;
          var className = _this$props.className,
          props = _objectWithoutProperties(_this$props, _excluded$m);

      return /*#__PURE__*/React__default.createElement("select", _extends$7({}, props, {
        ref: this.setElement,
        id: props.id || props.name,
        onBlur: this.onBlur,
        onChange: this.onChange,
        className: cn(className, readOnly && 'read-only'),
        readOnly: readOnly,
        disabled: readOnly || disabled
      }), /*#__PURE__*/React__default.createElement("option", {
        value: ""
      }, placeholder), options && Object.entries(options).map(this.renderOption, this));
    }
  }]);

  return Select;
}(BaseHtmlValidationInput);

_defineProperty(Select$1, "propTypes", _objectSpread2(_objectSpread2({}, BaseHtmlValidationInput.propTypes), {}, {
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  disabledOptions: PropTypes.arrayOf(PropTypes.string),
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  options: PropTypes.object
}));

_defineProperty(Select$1, "defaultProps", {
  placeholder: 'Please Select...',
  disabledOptions: []
});

var _excluded$l = ["disabled", "validation", "validationMessage", "validationMessages", "onReset", "options", "className", "id", "children", "required", "optionComponent", "readOnly"];
var RadioInputGroup = /*#__PURE__*/function (_BaseInput) {
  _inherits(RadioInputGroup, _BaseInput);

  var _super = _createSuper(RadioInputGroup);

  function RadioInputGroup() {
    var _this;

    _classCallCheck(this, RadioInputGroup);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "element", null);

    _defineProperty(_assertThisInitialized(_this), "setElement", function (element) {
      _this.element = element;
      if (!element) return;
      var value = _this.props.defaultValue || null; //Validate default value
      // if (value) this.validate() // TELL ME WHY?

      element.addEventListener('change', function (event) {
        if (event.target.matches('input')) {
          _this.handleChange(event);
        }
      }); // let the parent form knows about this control

      _this.broadcastUpdates({
        value: value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (event) {
      var _event$target = event.target,
          value = _event$target.value,
          checked = _event$target.checked;
      if (!checked) return; // let the parent form knows about changes
      // start validation on change

      _this.broadcastUpdates({
        value: value,
        touched: true
      });
    });

    return _this;
  }

  _createClass(RadioInputGroup, [{
    key: "isValid",
    value: function isValid() {
      if (!this.element) return false;
      if (this.props.required && this.getValue() === null) return false;
      if (this.checkCustomValidity()) return false;
      return true;
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      if (this.props.required && this.getValue() === null) return 'Required field';
      return this.checkCustomValidity() || null;
    }
  }, {
    key: "setValue",
    value: function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      if (!this.element) return;
      var radio = this.element.querySelector("input[value=\"".concat(value, "\"]"));

      if (radio) {
        radio.checked = true;
        this.broadcastUpdates({
          value: value
        });
      }
    }
  }, {
    key: "getValue",
    value: function getValue() {
      if (!this.element) return undefined;
      var radio = this.element.querySelector("input[name=".concat(this.props.name, "]:checked"));
      var value = radio && radio.value;
      return value === '' ? null : value;
    }
  }, {
    key: "focus",
    value: function focus() {
      var option = this.element.querySelector('input:checked') || this.element.querySelector('input');
      option === null || option === void 0 ? void 0 : option.focus();
    }
  }, {
    key: "renderOptions",
    value: function renderOptions(options, props) {
      var _this$props = this.props,
          Option = _this$props.optionComponent,
          name = _this$props.name;

      if (!Option) {
        throw new Error('RadioInputGroup: invalid prop: optionComponent');
      }

      return Object.entries(options).map(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            value = _ref2[0],
            label = _ref2[1];

        return /*#__PURE__*/React__default.createElement(Option, _extends$7({}, props, {
          value: value,
          name: name,
          key: name + value
        }), label);
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          disabled = _this$props2.disabled;
          _this$props2.validation;
          _this$props2.validationMessage;
          _this$props2.validationMessages;
          _this$props2.onReset;
          var options = _this$props2.options,
          className = _this$props2.className,
          id = _this$props2.id,
          children = _this$props2.children;
          _this$props2.required;
          _this$props2.optionComponent;
          var readOnly = _this$props2.readOnly,
          restProps = _objectWithoutProperties(_this$props2, _excluded$l);

      var props = _objectSpread2(_objectSpread2({}, restProps), {}, {
        // onChange: this.handleChange,
        disabled: readOnly || disabled
      });

      return /*#__PURE__*/React__default.createElement("div", {
        ref: this.setElement,
        id: id,
        className: className
      }, options ? this.renderOptions(options, props) : React__default.Children.map(children, function (child) {
        return /*#__PURE__*/React__default.cloneElement(child, props);
      }));
    }
  }]);

  return RadioInputGroup;
}(BaseInput);

_defineProperty(RadioInputGroup, "propTypes", _objectSpread2(_objectSpread2({}, BaseInput.propTypes), {}, {
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  optionComponent: PropTypes.elementType,
  options: PropTypes.objectOf(PropTypes.node),
  validation: PropTypes.func
}));

var _excluded$k = ["disabled", "validation", "validationMessage", "validationMessages", "onReset", "options", "className", "id", "children", "required", "optionComponent", "readOnly"];
var CheckboxInputGroup = /*#__PURE__*/function (_BaseInput) {
  _inherits(CheckboxInputGroup, _BaseInput);

  var _super = _createSuper(CheckboxInputGroup);

  function CheckboxInputGroup() {
    var _this;

    _classCallCheck(this, CheckboxInputGroup);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "element", null);

    _defineProperty(_assertThisInitialized(_this), "setElement", function (element) {
      _this.element = element;
      if (!element) return;
      var value = _this.props.defaultValue || null; // let the parent form knows about this control

      _this.broadcastUpdates({
        value: value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "values", _this.getValuesArray(_this.props.defaultValue));

    _defineProperty(_assertThisInitialized(_this), "handleBlur", function () {
      var _this$props$onBlur, _this$props;

      _this.broadcastUpdates({
        touched: true
      });

      (_this$props$onBlur = (_this$props = _this.props).onBlur) === null || _this$props$onBlur === void 0 ? void 0 : _this$props$onBlur.call(_this$props);
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (event) {
      var _event$target = event.target,
          value = _event$target.value,
          checked = _event$target.checked;

      var includes = _this.values.includes(value);

      if (checked && !includes) {
        _this.values = [].concat(_toConsumableArray(_this.values), [value]).sort();
      } else if (!checked && includes) {
        _this.values = _this.values.filter(function (val) {
          return value !== val;
        });
      } // let the parent form knows about changes
      // start validation on change


      _this.broadcastUpdates({
        touched: true
      });
    });

    return _this;
  }

  _createClass(CheckboxInputGroup, [{
    key: "getValue",
    value: function getValue() {
      return this.values.join(this.props.delimiter) || null;
    }
  }, {
    key: "setValue",
    value: function setValue(value) {
      var _this2 = this;

      this.values = this.getValuesArray(value);
      Array.from(this.element.querySelectorAll('input')).forEach(function (input) {
        input.checked = _this2.values.includes(input.value);
      });
      this.broadcastUpdates({
        value: value
      });
    }
  }, {
    key: "getValuesArray",
    value: function getValuesArray(value) {
      var delimiter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.props.delimiter;
      if (!value) return [];
      return value.split(delimiter).sort();
    }
  }, {
    key: "isValid",
    value: function isValid() {
      if (this.props.required && this.getValue() === null) return false;
      if (this.checkCustomValidity()) return false;
      return true;
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      if (this.props.required && this.getValue() === null) return 'Required field';
      return this.checkCustomValidity() || null;
    }
  }, {
    key: "focus",
    value: function focus() {
      var option = this.element.querySelector('input:checked') || this.element.querySelector('input:not([disabled])');
      option === null || option === void 0 ? void 0 : option.focus();
    }
  }, {
    key: "reset",
    value: function reset() {
      this.values = this.getValuesArray(this.props.defaultValue);

      _get(_getPrototypeOf(CheckboxInputGroup.prototype), "reset", this).call(this);
    }
  }, {
    key: "renderOptions",
    value: function renderOptions(options, props) {
      var _this3 = this;

      var _this$props2 = this.props,
          Option = _this$props2.optionComponent,
          name = _this$props2.name;
      return Object.entries(options).map(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            value = _ref2[0],
            label = _ref2[1];

        return /*#__PURE__*/React__default.createElement(Option, _extends$7({}, props, {
          value: value,
          name: name,
          key: name + value,
          defaultChecked: _this3.values.includes(value)
        }), label);
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.broadcastUpdates();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          disabled = _this$props3.disabled;
          _this$props3.validation;
          _this$props3.validationMessage;
          _this$props3.validationMessages;
          _this$props3.onReset;
          var options = _this$props3.options,
          className = _this$props3.className,
          id = _this$props3.id,
          children = _this$props3.children;
          _this$props3.required;
          _this$props3.optionComponent;
          var readOnly = _this$props3.readOnly,
          restProps = _objectWithoutProperties(_this$props3, _excluded$k);

      var props = _objectSpread2(_objectSpread2({}, restProps), {}, {
        onChange: this.handleChange,
        disabled: readOnly || disabled
      });

      return /*#__PURE__*/React__default.createElement("div", {
        ref: this.setElement,
        id: id,
        className: className,
        onBlur: this.handleBlur
      }, options ? this.renderOptions(options, props) : React__default.Children.map(children, function (child) {
        return /*#__PURE__*/React__default.cloneElement(child, props);
      }));
    }
  }]);

  return CheckboxInputGroup;
}(BaseInput);

_defineProperty(CheckboxInputGroup, "propTypes", _objectSpread2(_objectSpread2({}, BaseInput.propTypes), {}, {
  delimiter: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  optionComponent: PropTypes.elementType,
  options: PropTypes.objectOf(PropTypes.node),
  validation: PropTypes.func
}));

_defineProperty(CheckboxInputGroup, "defaultProps", {
  delimiter: ','
});

var _excluded$j = ["className", "type", "theme", "small", "wide", "children", "rightIcon", "leftIcon"];
var Button = /*#__PURE__*/React__default.forwardRef(function (_ref, ref) {
  var customClassName = _ref.className,
      type = _ref.type,
      theme = _ref.theme,
      small = _ref.small,
      wide = _ref.wide,
      children = _ref.children,
      RightIcon = _ref.rightIcon,
      LeftIcon = _ref.leftIcon,
      props = _objectWithoutProperties(_ref, _excluded$j);

  var className = cn('button', wide && 'wide', small && 'small', theme, !theme && type === 'submit' && 'primary', !theme && type === 'reset' && 'secondary', customClassName);
  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/button-has-type
    React__default.createElement("button", _extends$7({}, props, {
      type: type,
      className: className,
      ref: ref
    }), LeftIcon && /*#__PURE__*/React__default.createElement(LeftIcon, {
      className: "left"
    }), children, RightIcon && /*#__PURE__*/React__default.createElement(RightIcon, {
      className: "right"
    }))
  );
});
Button.displayName = 'Button';
Button.propTypes = {
  leftIcon: PropTypes.func,
  rightIcon: PropTypes.func,
  small: PropTypes.bool,
  theme: PropTypes.oneOf(['primary', 'secondary', 'alert', 'success', 'inline', 'flat', 'option-like']),
  type: PropTypes.oneOf(['submit', 'reset', 'button']),
  wide: PropTypes.bool
};
Button.defaultProps = {
  type: 'button',
  wide: false,
  small: false
};

var _excluded$i = ["className"];
var IconButton = function IconButton(_ref) {
  var className = _ref.className,
      props = _objectWithoutProperties(_ref, _excluded$i);

  return /*#__PURE__*/React__default.createElement("button", _extends$7({
    type: "button"
  }, props, {
    className: cn('icon-button', className)
  }));
};

var _excluded$h = ["open", "className", "onToggle", "onClick"];
var ToggleButton = /*#__PURE__*/React__default.forwardRef(function (_ref, ref) {
  var _ref$open = _ref.open,
      _open = _ref$open === void 0 ? false : _ref$open,
      className = _ref.className,
      onToggle = _ref.onToggle,
      onClick = _ref.onClick,
      props = _objectWithoutProperties(_ref, _excluded$h);

  var _useState = useState(_open),
      _useState2 = _slicedToArray(_useState, 2),
      open = _useState2[0],
      setOpen = _useState2[1];

  var toggle = useCallback(function (event) {
    setOpen(!open);
    onClick && onClick(event);
    onToggle && onToggle(!open);
  }, [onClick, onToggle, open]);
  return /*#__PURE__*/React__default.createElement(Button, _extends$7({}, props, {
    className: cn('toggle', className),
    "aria-expanded": open ? true : undefined,
    onClick: toggle,
    ref: ref
  }));
});
ToggleButton.displayName = 'ToggleButton';
ToggleButton.propTypes = {
  onClick: PropTypes.func,
  onToggle: PropTypes.func,
  open: PropTypes.bool
};

var _excluded$g = ["disabled", "disableUnmodified"],
    _excluded2$5 = ["disabled", "disableUnmodified"],
    _excluded3$1 = ["disabled", "disableUnmodified"],
    _excluded4$1 = ["disabled", "disableUnmodified"];
var SubmitButton = /*#__PURE__*/React__default.forwardRef(function (_ref, ref) {
  var disabled = _ref.disabled,
      disableUnmodified = _ref.disableUnmodified,
      props = _objectWithoutProperties(_ref, _excluded$g);

  var _useFormProps = useFormProps(),
      pending = _useFormProps.pending,
      modified = _useFormProps.modified; // TODO: Add some global Defaults/Config - to set whether a button should be disabled for invalid form


  return /*#__PURE__*/React__default.createElement(Button, _extends$7({}, props, {
    type: "submit",
    ref: ref,
    disabled: disabled || pending || disableUnmodified && !modified
  }));
});
SubmitButton.displayName = 'SubmitButton';
var OptimisticSubmitButton = /*#__PURE__*/React__default.forwardRef(function (_ref2, ref) {
  var disabled = _ref2.disabled,
      disableUnmodified = _ref2.disableUnmodified,
      props = _objectWithoutProperties(_ref2, _excluded2$5);

  var _useFormProps2 = useFormProps(),
      pending = _useFormProps2.pending,
      modified = _useFormProps2.modified;

  return /*#__PURE__*/React__default.createElement(Button, _extends$7({}, props, {
    type: "submit",
    ref: ref,
    disabled: disabled || pending || disableUnmodified && !modified
  }));
});
OptimisticSubmitButton.displayName = 'OptimisticSubmitButton';
var SaveButton = /*#__PURE__*/React__default.forwardRef(function (_ref3, ref) {
  var disabled = _ref3.disabled;
      _ref3.disableUnmodified;
      var props = _objectWithoutProperties(_ref3, _excluded3$1);

  var _useFormProps3 = useFormProps(),
      pending = _useFormProps3.pending,
      modified = _useFormProps3.modified; // TODO: Add some global Defaults/Config - to set whether a button should be disabled for invalid form


  return /*#__PURE__*/React__default.createElement(Button, _extends$7({}, props, {
    type: "submit",
    disabled: !modified || disabled || pending,
    ref: ref
  }));
});
SaveButton.displayName = 'SaveButton';
var ResetButton = function ResetButton(_ref4) {
  var disabled = _ref4.disabled,
      disableUnmodified = _ref4.disableUnmodified,
      props = _objectWithoutProperties(_ref4, _excluded4$1);

  var _useFormProps4 = useFormProps(),
      pending = _useFormProps4.pending,
      modified = _useFormProps4.modified;

  return /*#__PURE__*/React__default.createElement(Button, _extends$7({}, props, {
    type: "reset",
    disabled: disableUnmodified && !modified || disabled || pending
  }));
};
SubmitButton.propTypes = {
  disableUnmodified: PropTypes.bool,
  disabled: PropTypes.bool
};
OptimisticSubmitButton.propTypes = {
  disableUnmodified: PropTypes.bool,
  disabled: PropTypes.bool
};
SaveButton.propTypes = {
  disableUnmodified: PropTypes.bool,
  disabled: PropTypes.bool
};
ResetButton.propTypes = {
  disableUnmodified: PropTypes.bool,
  disabled: PropTypes.bool
};

var FieldValue = function FieldValue(_ref) {
  var name = _ref.name;

  var _useField = useField(name),
      value = _useField.value;

  return value === undefined ? null : String(value);
};
FieldValue.propTypes = {
  name: PropTypes.string.isRequired
}; // deprecated api - for compatibility -------------------------------------------

var _excluded$f = ["className", "children", "as"],
    _excluded2$4 = ["name", "label", "children", "className"];
var LabelText = function LabelText(_ref) {
  var className = _ref.className,
      children = _ref.children,
      Component = _ref.as,
      props = _objectWithoutProperties(_ref, _excluded$f);

  return children ? /*#__PURE__*/React__default.createElement(Component, _extends$7({}, props, {
    className: cn('label-text', className)
  }), children) : false;
};
LabelText.defaultProps = {
  as: 'span'
};
LabelText.propTypes = {
  as: PropTypes.oneOfType([PropTypes.func, PropTypes.string]).isRequired
};
var Label = function Label(_ref2) {
  var name = _ref2.name,
      label = _ref2.label,
      children = _ref2.children,
      className = _ref2.className,
      props = _objectWithoutProperties(_ref2, _excluded2$4);

  return /*#__PURE__*/React__default.createElement("label", _extends$7({}, props, {
    htmlFor: name,
    className: cn('label', className)
  }), children, /*#__PURE__*/React__default.createElement(LabelText, null, label));
};
Label.propTypes = {
  label: PropTypes.node,
  name: PropTypes.string.isRequired
};

var _excluded$e = ["validationMessage", "label", "hideValidationMessage", "className", "children", "touched"];
var BaseField = /*#__PURE__*/function (_React$PureComponent) {
  _inherits(BaseField, _React$PureComponent);

  var _super = _createSuper(BaseField);

  function BaseField() {
    var _this;

    _classCallCheck(this, BaseField);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "element", /*#__PURE__*/React__default.createRef());

    return _this;
  }

  _createClass(BaseField, [{
    key: "setValue",
    value: function setValue(value) {
      if (this.element.current) this.element.current.value = value;
    }
  }, {
    key: "value",
    get: function get() {
      return this.getValue();
    }
    /** @private */
    ,
    set: function set(val) {
      this.setValue(val);
    }
  }, {
    key: "getValue",
    value: function getValue() {
      var _this$element$current;

      return (_this$element$current = this.element.current) === null || _this$element$current === void 0 ? void 0 : _this$element$current.value;
    }
  }, {
    key: "label",
    get: function get() {
      return this.props.label;
    }
    /** @protected */

  }, {
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      return cn.apply(void 0, ['field', this.props.className].concat(args));
    }
    /** @private */

  }, {
    key: "className",
    get: function get() {
      return this.getWrapperClassName(this.props.className);
    }
  }, {
    key: "focus",
    value: function focus() {
      var _this$element, _this$element$current2;

      ((_this$element = this.element) === null || _this$element === void 0 ? void 0 : (_this$element$current2 = _this$element.current) === null || _this$element$current2 === void 0 ? void 0 : _this$element$current2.focus) && this.element.current.focus();
    }
    /** @protected */

  }, {
    key: "shouldShowValidity",
    value: function shouldShowValidity() {
      var _this$props = this.props,
          hideValidationMessage = _this$props.hideValidationMessage,
          touched = _this$props.touched,
          validationMessage = _this$props.validationMessage;
      return !hideValidationMessage && touched && !!validationMessage;
    }
    /** @protected */

  }, {
    key: "getInputClassName",
    value: function getInputClassName() {
      var _this$props2 = this.props,
          touched = _this$props2.touched,
          validationMessage = _this$props2.validationMessage;
      return cn('input', touched && 'touched', this.shouldShowValidity() && validationMessage && 'invalid');
    }
    /** @protected */

  }, {
    key: "getInputProps",
    value: function getInputProps() {
      var _this$props3 = this.props,
          validationMessage = _this$props3.validationMessage;
          _this$props3.label;
          _this$props3.hideValidationMessage;
          _this$props3.className;
          _this$props3.children;
          _this$props3.touched;
          var props = _objectWithoutProperties(_this$props3, _excluded$e);

      return _objectSpread2(_objectSpread2({}, props), {}, {
        ref: this.element,
        validationMessage: validationMessage,
        className: this.getInputClassName()
      });
    }
    /** @protected */

  }, {
    key: "renderField",
    value: function renderField() {
      throw 'override renderField(props)';
    }
    /** @protected */

  }, {
    key: "renderValidationMessage",
    value: function renderValidationMessage() {
      var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.props;
      var validationMessage = props.validationMessage;
      return /*#__PURE__*/React__default.createElement(ValidationMessage, {
        error: true
      }, validationMessage);
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React__default.createElement(Label, {
        label: this.label,
        name: this.props.name,
        className: this.className
      }, this.renderField(this.getInputProps()), this.props.children, this.shouldShowValidity() && this.renderValidationMessage());
    }
  }]);

  return BaseField;
}(React__default.PureComponent);

_defineProperty(BaseField, "propTypes", {
  hideValidationMessage: PropTypes.bool,
  label: PropTypes.node,
  name: PropTypes.string.isRequired,
  touched: PropTypes.bool,
  validationMessage: PropTypes.string
});

_defineProperty(BaseField, "defaultProps", {
  hideValidationMessage: false
});

function withFormDefaultValues(Component) {
  // eslint-disable-next-line react/prop-types
  var WrappedComponent = /*#__PURE__*/React__default.forwardRef(function (props, ref) {
    var name = props.name;

    var _useFormProps = useFormProps(),
        defaultValues = _useFormProps.defaultValues,
        disabled = _useFormProps.disabled,
        readOnly = _useFormProps.readOnly;

    var _useField = useField(name),
        touched = _useField.touched,
        validationMessage = _useField.validationMessage;

    var defaultValue = defaultValues.hasOwnProperty(name) ? defaultValues[name] : undefined;
    return /*#__PURE__*/React__default.createElement(Component, _extends$7({
      defaultValue: defaultValue,
      readOnly: readOnly,
      disabled: disabled
    }, props, {
      touched: touched,
      validationMessage: validationMessage,
      ref: ref
    }));
  });
  var displayName = Component.displayName || Component.name || 'Field';
  WrappedComponent.displayName = "withFormDefaults(".concat(displayName, ")");
  WrappedComponent.propTypes = Component.propTypes;
  WrappedComponent.defaultProps = Component.defaultProps;
  return WrappedComponent;
}

var validationMessages = {
  patternMismatch: 'Account Number should contain only digits'
};
var Field$h = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "renderField",
    value: function renderField(props) {
      // wrong native validation for number with 16,17 lengths digits
      // return <NumberInput {...props} min={100} max={999999999999999} />
      return /*#__PURE__*/React__default.createElement(TextInput, _extends$7({}, props, {
        minLength: 3,
        maxLength: 17,
        inputMode: "numeric",
        pattern: "[0-9]*",
        validationMessages: validationMessages
      }));
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$h, "displayName", 'AccountNumberField');

var AccountNumberField = withFormDefaultValues(Field$h);

var _excluded$d = ["disabled", "validation", "validationMessage", "defaultValue"];
var CheckboxInput = /*#__PURE__*/function (_BaseInput) {
  _inherits(CheckboxInput, _BaseInput);

  var _super = _createSuper(CheckboxInput);

  function CheckboxInput() {
    var _this;

    _classCallCheck(this, CheckboxInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "element", null);

    _defineProperty(_assertThisInitialized(_this), "setElement", function (element) {
      _this.element = element;
      if (!element) return; // let the parent form knows about this control

      _this.broadcastUpdates({
        value: _this.props.defaultValue || false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (event) {
      var checked = event.target.checked; // let the parent form knows about changes

      _this.broadcastUpdates({
        value: checked,
        touched: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleBlur", function (event) {
      _this.validate();

      _this.props.onBlur && _this.props.onBlur(event);
    });

    return _this;
  }

  _createClass(CheckboxInput, [{
    key: "getValidationMessage",
    value:
    /**
     * @public
     * @override
     */
    function getValidationMessage() {
      if (this.props.required && !this.element.checked) {
        return 'Required';
      }

      return this.checkCustomValidity();
    }
    /**
     * @override
     * @returns {boolean}
     */

  }, {
    key: "isValid",
    value: function isValid() {
      if (!this.element) return false;
      if (this.props.required && !this.element.checked) return false;
      if (this.checkCustomValidity()) return false;
      return true;
    }
    /**
     * @override
     */

  }, {
    key: "getValue",
    value: function getValue() {
      if (!this.element) return undefined;
      return this.element.checked || false;
    }
  }, {
    key: "setValue",
    value: function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      if (!this.element) return;
      this.element.checked = value;
      this.broadcastUpdates();
    }
  }, {
    key: "focus",
    value: function focus() {
      this.element.focus();
    }
  }, {
    key: "scrollIntoView",
    value: function scrollIntoView() {
      var _this$element;

      (_this$element = this.element).scrollIntoView.apply(_this$element, arguments);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled;
          _this$props.validation;
          _this$props.validationMessage;
          var defaultValue = _this$props.defaultValue,
          props = _objectWithoutProperties(_this$props, _excluded$d);

      return /*#__PURE__*/React__default.createElement("input", _extends$7({}, props, {
        ref: this.setElement,
        id: props.id || props.name,
        onChange: this.handleChange,
        onBlur: this.handleBlur,
        disabled: disabled,
        type: "checkbox",
        defaultChecked: defaultValue
      }));
    }
  }]);

  return CheckboxInput;
}(BaseInput);

_defineProperty(CheckboxInput, "propTypes", _objectSpread2(_objectSpread2({}, BaseInput.propTypes), {}, {
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  validation: PropTypes.func
}));

_defineProperty(CheckboxInput, "defaultProps", {
  defaultValue: false
});

var _path$6;

function _extends$6() { _extends$6 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$6.apply(this, arguments); }

function SvgCheckboxChecked(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$6({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "3 3 18 18"
  }, props), _path$6 || (_path$6 = /*#__PURE__*/React.createElement("path", {
    d: "M19 3H5a2 2 0 00-2 2v14a2 2 0 002 2h14a2 2 0 002-2V5a2 2 0 00-2-2zm-9 14l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"
  })));
}

var CheckboxTick = function CheckboxTick() {
  return /*#__PURE__*/React__default.createElement("span", {
    className: "checkbox-tick"
  }, /*#__PURE__*/React__default.createElement(SvgCheckboxChecked, {
    className: "checkbox-tick-icon"
  }));
};

var Field$g = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'checkbox');
    }
  }, {
    key: "renderField",
    value: function renderField(props) {
      return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement(CheckboxInput, props), /*#__PURE__*/React__default.createElement(CheckboxTick, null));
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$g, "displayName", 'CheckboxField');

var Checkbox = withFormDefaultValues(Field$g);

var _excluded$c = ["value", "defaultValue", "className", "children", "style"];
var CheckboxOption = function CheckboxOption(_ref) {
  var value = _ref.value;
      _ref.defaultValue;
      var className = _ref.className,
      children = _ref.children,
      style = _ref.style,
      props = _objectWithoutProperties(_ref, _excluded$c);

  return props.name ? /*#__PURE__*/React__default.createElement("label", {
    className: cn('option checkbox-option', className),
    style: style
  }, /*#__PURE__*/React__default.createElement("input", _extends$7({}, props, {
    type: "checkbox",
    value: value
  })), /*#__PURE__*/React__default.createElement(CheckboxTick, null), /*#__PURE__*/React__default.createElement("span", {
    className: "option-label"
  }, children)) : false;
};
CheckboxOption.propTypes = {
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  style: PropTypes.shape({
    '--index': PropTypes.number
  }),
  value: PropTypes.string
};

var _excluded$b = ["label", "hideValidationMessage", "validationMessage", "className", "children", "horizontal", "vertical", "touched"];
var Field$f = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "renderField",
    value: function renderField(props) {
      return /*#__PURE__*/React__default.createElement(CheckboxInputGroup, _extends$7({}, props, {
        optionComponent: CheckboxOption
      }), this.props.children);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          label = _this$props.label;
          _this$props.hideValidationMessage;
          _this$props.validationMessage;
          _this$props.className;
          _this$props.children;
          var horizontal = _this$props.horizontal;
          _this$props.vertical;
          _this$props.touched;
          var props = _objectWithoutProperties(_this$props, _excluded$b);

      var editable = !props.disabled && !props.readOnly;
      return /*#__PURE__*/React__default.createElement("fieldset", {
        name: props.name,
        className: this.getWrapperClassName('checkbox-group', horizontal && 'horizontal')
      }, /*#__PURE__*/React__default.createElement(LabelText, {
        as: "legend",
        className: cn(editable && props.required && 'required')
      }, label), this.renderField(this.getInputProps()), this.shouldShowValidity() && this.renderValidationMessage());
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$f, "displayName", 'CheckboxGroup');

var CheckboxGroup = withFormDefaultValues(Field$f);

var _excluded$a = ["min", "max", "multipliesOf"];
var CLEAVE_OPTIONS = {
  numeral: true,
  numeralThousandsGroupStyle: 'thousand',
  numeralPositiveOnly: true,
  numeralDecimalScale: 2,
  stripLeadingZeroes: true
};
var Field$e = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    var _this;

    _classCallCheck(this, Field);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "validate", function (value) {
      var _this$props = _this.props,
          min = _this$props.min,
          max = _this$props.max,
          multipliesOf = _this$props.multipliesOf,
          validation = _this$props.validation;

      if (Number.isFinite(value)) {
        var hasMin = min !== undefined && min !== null;

        if (hasMin && value < min) {
          return "Must be greater than or equals to ".concat(min);
        }

        if (max !== undefined && max !== null && value > max) {
          return "Must be lesser than or equals to ".concat(max);
        }

        if (multipliesOf !== undefined && multipliesOf !== null) {
          if ((value * 100).toFixed(0) % (multipliesOf * 100).toFixed(0) !== 0) {
            return "Must be entered in multiples of ".concat(multipliesOf);
          }
        }
      }

      if (validation) {
        return validation(value);
      }
    });

    return _this;
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'currency');
    }
  }, {
    key: "renderField",
    value: function renderField(_ref) {
      _ref.min;
          _ref.max;
          _ref.multipliesOf;
          var props = _objectWithoutProperties(_ref, _excluded$a);

      return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement(FormattedInput, _extends$7({}, props, {
        validation: this.validate,
        options: CLEAVE_OPTIONS
      })), /*#__PURE__*/React__default.createElement("span", {
        className: "currency-label"
      }, "$"));
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$e, "displayName", 'CurrencyField');

_defineProperty(Field$e, "propTypes", _objectSpread2(_objectSpread2({}, BaseField.propTypes), {}, {
  max: PropTypes.number,
  min: PropTypes.number,
  multipliesOf: PropTypes.number
}));

var CurrencyField = withFormDefaultValues(Field$e);

var Field$d = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'date');
    }
  }, {
    key: "renderField",
    value: function renderField(props) {
      return /*#__PURE__*/React__default.createElement(DateInput, props);
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$d, "displayName", 'DateField');

var DateField = withFormDefaultValues(Field$d);
DateField.propTypes = {
  autoComplete: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.node,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  validation: PropTypes.func
};

var Field$c = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'text');
    }
  }, {
    key: "renderField",
    value: function renderField(_ref) {
      var props = _extends$7({}, _ref);

      return /*#__PURE__*/React__default.createElement(TextInput, props);
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$c, "displayName", 'TextField');

var TextField = withFormDefaultValues(Field$c);

var Field$b = /*#__PURE__*/function (_TextField) {
  _inherits(Field, _TextField);

  var _super = _createSuper(Field);

  function Field() {
    var _this;

    _classCallCheck(this, Field);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "displayName", 'EmailField');

    return _this;
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'email');
    }
  }, {
    key: "getInputProps",
    value: function getInputProps() {
      return _objectSpread2(_objectSpread2({}, _get(_getPrototypeOf(Field.prototype), "getInputProps", this).call(this)), {}, {
        type: 'email',
        autoComplete: 'email'
      });
    }
  }]);

  return Field;
}(Field$c);

var EmailField = withFormDefaultValues(Field$b);
EmailField.propTypes = {
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.node,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool
};
EmailField.defaultProps = {
  label: 'Email'
};

var _excluded$9 = ["validation", "validationMessage", "validationMessages", "className", "defaultValue", "valueToFilename", "upload", "label", "required", "children"];
var FileInput = /*#__PURE__*/function (_BaseInput) {
  _inherits(FileInput, _BaseInput);

  var _super = _createSuper(FileInput);

  function FileInput() {
    var _this;

    _classCallCheck(this, FileInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", {
      value: _this.props.defaultValue,
      filename: _this.props.valueToFilename(_this.props.defaultValue),
      uploading: false,
      validationMessage: null
    });

    _defineProperty(_assertThisInitialized(_this), "setElement", function (element) {
      _this.element = element;
    });

    _defineProperty(_assertThisInitialized(_this), "handleChange", /*#__PURE__*/function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(event) {
        var WrongFormatError, file, _this$props, upload, valueToFilename;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                WrongFormatError = 'Wrong file format, you should use on of ( * .pdf,* .docx,* .txt)';
                file = event.target.files[0];

                if (!file.type.includes('image')) {
                  _context2.next = 7;
                  break;
                }

                _this.setState({
                  validationMessage: WrongFormatError,
                  uploading: false,
                  touched: true
                });

                _context2.next = 6;
                return _this.broadcastUpdates({
                  value: null,
                  validationMessage: WrongFormatError,
                  valid: false,
                  touched: true
                });

              case 6:
                return _context2.abrupt("return");

              case 7:
                _this$props = _this.props, upload = _this$props.upload, valueToFilename = _this$props.valueToFilename;

                if (!(file && upload)) {
                  _context2.next = 12;
                  break;
                }

                _context2.next = 11;
                return _this.broadcastUpdates({
                  validationMessage: 'Uploading',
                  touched: false,
                  //workarond do hide the error message
                  valid: false
                });

              case 11:
                _this.setState({
                  validationMessage: null,
                  filename: file.name,
                  uploading: true
                }, /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                  var uploadResult, error;
                  return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.prev = 0;
                          _context.next = 3;
                          return upload(file);

                        case 3:
                          uploadResult = _context.sent;

                          _this.setState({
                            value: uploadResult,
                            validationMessage: null,
                            filename: valueToFilename(uploadResult),
                            uploading: false
                          }, function () {
                            _this.broadcastUpdates({
                              value: uploadResult,
                              validationMessage: null,
                              valid: true,
                              touched: true
                            });
                          });

                          _context.next = 13;
                          break;

                        case 7:
                          _context.prev = 7;
                          _context.t0 = _context["catch"](0);
                          error = _context.t0.error;

                          _this.setState({
                            validationMessage: error,
                            uploading: false,
                            touched: true
                          });

                          _context.next = 13;
                          return _this.broadcastUpdates({
                            value: null,
                            validationMessage: error,
                            valid: false,
                            touched: true
                          });

                        case 13:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee, null, [[0, 7]]);
                })));

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _defineProperty(_assertThisInitialized(_this), "handleBrowseFile", function () {
      _this.element && _this.element.click();
    });

    return _this;
  }

  _createClass(FileInput, [{
    key: "reset",
    value: function reset() {
      var _this2 = this;

      var _this$props2 = this.props,
          defaultValue = _this$props2.defaultValue,
          valueToFilename = _this$props2.valueToFilename;
      var value = defaultValue || null;
      this.setState({
        filename: valueToFilename(value)
      }, function () {
        _this2.broadcastUpdates({
          value: value,
          touched: false
        });
      });
    }
    /**
     * @override
     */

  }, {
    key: "setValue",
    value: function setValue(value) {
      var _this3 = this;

      this.setState({
        value: value,
        fileName: this.props.valueToFilename(value)
      }, function () {
        _this3.broadcastUpdates({
          value: value || null
        });
      });
    }
    /**
     * @override
     */

  }, {
    key: "getValue",
    value: function getValue() {
      return this.state.value;
    }
  }, {
    key: "isValid",
    value: function isValid() {
      if (this.props.required && !this.getValue()) return false;
      return !this.state.uploading && !this.state.validationMessage;
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      var _this$state = this.state,
          uploading = _this$state.uploading,
          validationMessage = _this$state.validationMessage;
      if (uploading) return 'Uploading';
      if (validationMessage) return this.state.validationMessage;
      if (this.props.required && !this.getValue()) return 'Required';
      return null;
    }
  }, {
    key: "willValidate",
    value: function willValidate() {
      return this.element.willValidate;
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.broadcastUpdates({
        value: this.props.defaultValue
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$state2 = this.state,
          filename = _this$state2.filename,
          uploading = _this$state2.uploading;

      var _this$props3 = this.props;
          _this$props3.validation;
          _this$props3.validationMessage;
          _this$props3.validationMessages;
          _this$props3.className;
          _this$props3.defaultValue;
          _this$props3.valueToFilename;
          _this$props3.upload;
          _this$props3.label;
          _this$props3.required;
          _this$props3.children;
          var props = _objectWithoutProperties(_this$props3, _excluded$9);

      return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement("input", _extends$7({}, props, {
        noValidate: true,
        tabIndex: "-1",
        className: "input",
        type: "file",
        accept: "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf",
        ref: this.setElement,
        id: props.id || props.name,
        onChange: this.handleChange,
        name: props.name
      })), /*#__PURE__*/React__default.createElement("button", {
        className: "browse-button",
        type: "button",
        onClick: this.handleBrowseFile,
        disabled: uploading || props.disabled || props.readOnly
      }, filename && /*#__PURE__*/React__default.createElement("span", {
        className: "file-name"
      }, filename), uploading ? 'Uploading...' : 'Browse...'));
    }
  }]);

  return FileInput;
}(BaseInput);

_defineProperty(FileInput, "propTypes", {
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  upload: PropTypes.func.isRequired,
  validation: PropTypes.func,
  valueToFilename: PropTypes.func.isRequired
});

_defineProperty(FileInput, "defaultProps", {
  valueToFilename: function valueToFilename(f) {
    return f;
  }
});

var Field$a = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'file', this.props.required && 'required', this.props.disabled && 'disabled', this.props.readOnly && 'read-only');
    }
  }, {
    key: "renderField",
    value: function renderField(props) {
      return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement(FileInput, _extends$7({
        valueToFilename: valueToFilename
      }, props)), /*#__PURE__*/React__default.createElement("div", {
        className: "pseudo-input"
      }));
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$a, "displayName", 'FileField');

_defineProperty(Field$a, "propTypes", {
  name: PropTypes.string.isRequired,
  upload: PropTypes.func.isRequired,
  validation: PropTypes.func
});

var FileField = withFormDefaultValues(Field$a);

var valueToFilename = function valueToFilename(value) {
  var _value$file;

  return value ? value === null || value === void 0 ? void 0 : (_value$file = value.file) === null || _value$file === void 0 ? void 0 : _value$file.split('_', 2)[1] : null;
};

var _excluded$8 = ["validationMessage", "label", "hideValidationMessage", "className", "children", "tooltip", "touched"];
var Field$9 = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "render",
    value: function render() {
      var _this$props = this.props;
          _this$props.validationMessage;
          _this$props.label;
          _this$props.hideValidationMessage;
          _this$props.className;
          _this$props.children;
          _this$props.tooltip;
          _this$props.touched;
          var props = _objectWithoutProperties(_this$props, _excluded$8);

      return /*#__PURE__*/React__default.createElement(HiddenInput, _extends$7({}, props, {
        ref: this.element
      }));
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$9, "displayName", 'HiddenField');

var HiddenField = withFormDefaultValues(Field$9);

var _path$5;

function _extends$5() { _extends$5 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$5.apply(this, arguments); }

function SvgValidationOk(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$5({
    xmlns: "http://www.w3.org/2000/svg",
    width: 24,
    height: 24
  }, props), _path$5 || (_path$5 = /*#__PURE__*/React.createElement("path", {
    d: "M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10S2 17.514 2 12 6.486 2 12 2zm0-2C5.373 0 0 5.373 0 12s5.373 12 12 12 12-5.373 12-12S18.627 0 12 0zm6.25 8.891l-1.421-1.409-6.105 6.218-3.078-2.937-1.396 1.436 4.5 4.319 7.5-7.627z"
  })));
}

var _path$4, _path2;

function _extends$4() { _extends$4 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$4.apply(this, arguments); }

function SvgValidationBad(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$4({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 24 24"
  }, props), _path$4 || (_path$4 = /*#__PURE__*/React.createElement("path", {
    d: "M4.47 19h15.06L12 5.99 4.47 19zM13 18h-2v-2h2v2zm0-4h-2v-4h2v4z",
    opacity: 0.3
  })), _path2 || (_path2 = /*#__PURE__*/React.createElement("path", {
    d: "M1 21h22L12 2 1 21zm3.47-2L12 5.99 19.53 19H4.47zM11 16h2v2h-2zm0-6h2v4h-2z"
  })));
}

var _path$3;

function _extends$3() { _extends$3 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$3.apply(this, arguments); }

function SvgValidationUntouched(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$3({
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 24 24"
  }, props), _path$3 || (_path$3 = /*#__PURE__*/React.createElement("path", {
    d: "M12 4c4.41 0 8 3.59 8 8s-3.59 8-8 8-8-3.59-8-8 3.59-8 8-8m0-2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2z"
  })));
}

var validation = [{
  test: function test(password) {
    return !!password && password.length >= 8 && password.length <= 24;
  },
  message: 'Password must be 8-24 characters and'
}, {
  test: function test(password) {
    return !!password && password.match(/.*[A-Z].*/);
  },
  message: 'Must contain at least 1 upper case'
}, {
  test: function test(password) {
    return !!password && password.match(/.*\d.*/);
  },
  message: 'Must contain at least 1 number'
}, {
  test: function test(password) {
    return !!password && password.match(/.*\W.*/);
  },
  message: 'Must contain at least 1 special character'
}];

var validatePassword = function validatePassword(password) {
  var allErrors = '';

  var _iterator = _createForOfIteratorHelper(validation),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var _step$value = _step.value,
          test = _step$value.test,
          message = _step$value.message;

      if (!test(password)) {
        allErrors = allErrors.concat(",".concat(message));
      }
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return allErrors;
};

var Field$8 = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    var _this;

    _classCallCheck(this, Field);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "validate", function (value) {
      if (!value) return 'required';
      return validatePassword(value);
    });

    return _this;
  }

  _createClass(Field, [{
    key: "renderField",
    value: function renderField(_ref) {
      var props = _extends$7({}, _ref);

      return /*#__PURE__*/React__default.createElement(TextInput, _extends$7({}, props, {
        type: "password",
        autoComplete: "new-password",
        validation: this.validate
      }));
    }
  }, {
    key: "shouldShowValidity",
    value: function shouldShowValidity() {
      return true;
    }
  }, {
    key: "renderValidationMessage",
    value: function renderValidationMessage() {
      var _this$props = this.props,
          validationMessage = _this$props.validationMessage,
          touched = _this$props.touched;
      var value = this.getValue();

      if (validationMessage) {
        return validation.map(function (_ref2) {
          var test = _ref2.test,
              message = _ref2.message;
          var valid = !!test(value);
          return /*#__PURE__*/React__default.createElement(ValidationMessage, {
            key: message,
            success: valid,
            error: !valid && touched
          }, /*#__PURE__*/React__default.createElement("span", {
            className: "password-message"
          }, valid ? /*#__PURE__*/React__default.createElement(SvgValidationOk, null) : !touched ? /*#__PURE__*/React__default.createElement(SvgValidationUntouched, null) : /*#__PURE__*/React__default.createElement(SvgValidationBad, null), message));
        });
      }

      return /*#__PURE__*/React__default.createElement(ValidationMessage, {
        success: true
      }, /*#__PURE__*/React__default.createElement("span", {
        className: "password-message"
      }, /*#__PURE__*/React__default.createElement(SvgValidationOk, null), "All password requirements met"));
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$8, "displayName", 'NewPasswordField');

_defineProperty(Field$8, "propTypes", {
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.node,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool
});

_defineProperty(Field$8, "defaultProps", {
  label: 'Password'
});

var NewPasswordField = withFormDefaultValues(Field$8);

var Field$7 = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'number');
    }
  }, {
    key: "renderField",
    value: function renderField(props) {
      return /*#__PURE__*/React__default.createElement(NumberInput, props);
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$7, "displayName", 'NumberField');

var NumberField = withFormDefaultValues(Field$7);

var _path$2;

function _extends$2() { _extends$2 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$2.apply(this, arguments); }

function SvgVisibilityHide(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$2({
    viewBox: "0 0 25 25"
  }, props), _path$2 || (_path$2 = /*#__PURE__*/React.createElement("path", {
    d: "M12.506 6.632c3.137 0 5.684 2.593 5.684 5.79a5.71 5.71 0 01-.41 2.118l3.32 3.381a13.676 13.676 0 003.9-5.5c-1.967-5.083-6.821-8.684-12.506-8.684-1.591 0-3.115.29-4.524.81l2.455 2.501a5.43 5.43 0 012.08-.416zM1.136 3.47L3.73 6.11l.523.532A13.648 13.648 0 000 12.421c1.967 5.083 6.821 8.684 12.506 8.684 1.762 0 3.444-.347 4.98-.972l.477.486L21.293 24l1.445-1.47L2.58 2 1.137 3.47zm6.288 6.403l1.762 1.794a3.324 3.324 0 00-.091.753c0 1.922 1.523 3.474 3.41 3.474.25 0 .5-.035.74-.093l1.762 1.795a5.552 5.552 0 01-2.501.614c-3.138 0-5.685-2.594-5.685-5.79 0-.915.228-1.772.603-2.547zm4.9-.903l3.581 3.647.023-.185c0-1.922-1.524-3.474-3.41-3.474l-.194.012z"
  })));
}

var _path$1;

function _extends$1() { _extends$1 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$1.apply(this, arguments); }

function SvgVisibilityShow(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$1({
    viewBox: "0 0 25 25"
  }, props), _path$1 || (_path$1 = /*#__PURE__*/React.createElement("path", {
    d: "M12.5 4C6.818 4 1.966 7.525 0 12.5 1.966 17.475 6.818 21 12.5 21s10.534-3.525 12.5-8.5C23.034 7.525 18.182 4 12.5 4zm0 14.167c-3.136 0-5.682-2.539-5.682-5.667 0-3.128 2.546-5.667 5.682-5.667 3.136 0 5.682 2.539 5.682 5.667 0 3.128-2.546 5.667-5.682 5.667zm0-9.067a3.4 3.4 0 100 6.8 3.4 3.4 0 100-6.8z"
  })));
}

var Field$6 = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    var _this;

    _classCallCheck(this, Field);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", {
      visible: false
    });

    _defineProperty(_assertThisInitialized(_this), "toggleVisibility", function () {
      _this.setState(function (state) {
        return {
          visible: !state.visible
        };
      });
    });

    return _this;
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'secret-field');
    }
  }, {
    key: "renderField",
    value: function renderField(props) {
      var visible = this.state.visible;
      return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement(TextInput, _extends$7({}, props, {
        type: visible ? 'text' : 'password'
      })), /*#__PURE__*/React__default.createElement(IsFieldValue, {
        name: this.props.name,
        not: true,
        blank: true
      }, /*#__PURE__*/React__default.createElement(IconButton, {
        onClick: this.toggleVisibility,
        className: "secret-field-icon",
        disabled: props.disabled
      }, visible ? /*#__PURE__*/React__default.createElement(SvgVisibilityHide, null) : /*#__PURE__*/React__default.createElement(SvgVisibilityShow, null))));
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$6, "displayName", 'SecretField');

var SecretField = withFormDefaultValues(Field$6);

var _excluded$7 = ["className"];
var PasswordField = /*#__PURE__*/React__default.forwardRef(function (_ref, ref) {
  var className = _ref.className,
      props = _objectWithoutProperties(_ref, _excluded$7);

  return /*#__PURE__*/React__default.createElement(SecretField, _extends$7({}, props, {
    ref: ref,
    className: cn('password', className)
  }));
});
PasswordField.displayName = 'PasswordField';
PasswordField.propTypes = {
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.node,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool
};
PasswordField.defaultProps = {
  autoComplete: 'password',
  label: 'Password'
};

var _excluded$6 = ["value", "defaultValue"],
    _excluded2$3 = ["validation", "validationMessage", "defaultValue", "value", "required", "readOnly", "disabled", "enableSearch", "placeholder", "country", "className"];
var BasePhoneInput = /*#__PURE__*/function (_BaseInput) {
  _inherits(BasePhoneInput, _BaseInput);

  var _super = _createSuper(BasePhoneInput);

  function BasePhoneInput() {
    var _thisSuper, _this;

    _classCallCheck(this, BasePhoneInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", _objectSpread2(_objectSpread2({}, _get((_thisSuper = _assertThisInitialized(_this), _getPrototypeOf(BasePhoneInput.prototype)), "state", _thisSuper)), {}, {
      value: _this.props.defaultValue || null,
      country: 'us',
      dialCode: '',
      reset: false
    }));

    _defineProperty(_assertThisInitialized(_this), "element", null);

    _defineProperty(_assertThisInitialized(_this), "defaultCountry", 'us');

    _defineProperty(_assertThisInitialized(_this), "dialCode", '');

    _defineProperty(_assertThisInitialized(_this), "setElement", function (element) {
      _this.element = element;
      var value = _this.props.defaultValue || null; // let the parent form knows about this control

      _this.broadcastUpdates({
        value: value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onBlur", function (e) {
      _this.validate();

      _this.props.onBlur && _this.props.onBlur(e);
    });

    _defineProperty(_assertThisInitialized(_this), "onChange", function (value, data) {
      _this.setState({
        value: value
      });

      _this.dialCode = data.dialCode;

      _this.setValue(value);
    });

    return _this;
  }

  _createClass(BasePhoneInput, [{
    key: "setValue",
    value: function setValue() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var normalValue = this.getNormalizedValue(value);
      var validationMessage = this.getValidationMessage(normalValue);
      this.broadcastUpdates({
        value: normalValue,
        validationMessage: validationMessage,
        valid: !validationMessage
      });
    }
    /**
     * @override
     */

  }, {
    key: "getValue",
    value: function getValue() {
      return this.getNormalizedValue(this.state.value);
    }
  }, {
    key: "getNormalizedValue",
    value: function getNormalizedValue(value) {
      if (value === undefined || value === null) {
        return '';
      }

      if (typeof value === 'string' && String(value).trim() === '') {
        return '';
      }

      return value.replace(/\D/g, '');
    }
  }, {
    key: "getDefaultValue",
    value: function getDefaultValue() {
      var defaultValue = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.props.defaultValue;
      return this.getNormalizedValue(defaultValue);
    }
  }, {
    key: "getValidationMessage",
    value: function getValidationMessage() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.getValue();
      if (!this.willValidate()) return null;
      var _this$props = this.props,
          required = _this$props.required,
          validation = _this$props.validation;

      if ((!value || this.dialCode && !value.slice(this.dialCode.length)) && required) {
        return 'The field is required';
      }

      return validation && validation(value) || null;
    }
  }, {
    key: "broadcastUpdates",
    value: function broadcastUpdates() {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref$value = _ref.value,
          value = _ref$value === void 0 ? this.getValue() : _ref$value,
          _ref$defaultValue = _ref.defaultValue,
          defaultValue = _ref$defaultValue === void 0 ? this.getDefaultValue() : _ref$defaultValue,
          props = _objectWithoutProperties(_ref, _excluded$6);

      return _get(_getPrototypeOf(BasePhoneInput.prototype), "broadcastUpdates", this).call(this, _objectSpread2(_objectSpread2({}, props), {}, {
        value: this.getNormalizedValue(value),
        defaultValue: this.getNormalizedValue(defaultValue)
      }));
    }
  }, {
    key: "validate",
    value: function validate() {
      this.broadcastUpdates({
        touched: true
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.props.defaultValue) {
        this.defaultCountry = this.element.state.selectedCountry.iso2; //is needed for validation

        this.dialCode = this.element.state.selectedCountry.dialCode;
      }
    }
  }, {
    key: "reset",
    value: function reset() {
      var _this2 = this;

      var reset = this.state.reset; //give know for a library that it needs to reset

      var resetState = function resetState() {
        return _this2.setState({
          reset: !reset
        });
      }; //create promise from setState


      var updateReset = new Promise(function (resolve) {
        return setTimeout(resolve, resetState());
      }); //update value to initial after reset, is needed for the formatting default value

      updateReset.then(function () {
        return _this2.setState({
          value: _this2.props.defaultValue
        });
      });
    }
  }, {
    key: "focus",
    value: function focus() {
      var _this$element, _this$element$numberI, _this$element$numberI2;

      (_this$element = this.element) === null || _this$element === void 0 ? void 0 : (_this$element$numberI = _this$element.numberInputRef) === null || _this$element$numberI === void 0 ? void 0 : (_this$element$numberI2 = _this$element$numberI.focus) === null || _this$element$numberI2 === void 0 ? void 0 : _this$element$numberI2.call(_this$element$numberI);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props;
          _this$props2.validation;
          _this$props2.validationMessage;
          _this$props2.defaultValue;
          _this$props2.value;
          var required = _this$props2.required,
          readOnly = _this$props2.readOnly,
          disabled = _this$props2.disabled,
          enableSearch = _this$props2.enableSearch,
          placeholder = _this$props2.placeholder,
          country = _this$props2.country,
          className = _this$props2.className,
          props = _objectWithoutProperties(_this$props2, _excluded2$3);

      var touched = className === null || className === void 0 ? void 0 : className.includes('touched');
      var invalid = className === null || className === void 0 ? void 0 : className.includes('invalid');
      return /*#__PURE__*/React__default.createElement(PhoneInputWithReset, _extends$7({}, props, {
        ref: this.setElement,
        id: props.id || props.name,
        onBlur: this.onBlur,
        onChange: this.onChange,
        value: this.state.value || '',
        defaultCountry: this.defaultCountry,
        country: this.props.defaultValue ? '' : country || 'us',
        disableDropdown: disabled || readOnly,
        enableSearch: enableSearch,
        buttonClass: cn(disabled && 'disabled', readOnly && 'read-only', touched && 'touched', invalid && 'invalid'),
        inputClass: className,
        inputProps: {
          id: props.id || props.name,
          name: props.id || props.name,
          required: required,
          readOnly: readOnly,
          disabled: disabled
        },
        placeholder: placeholder || '',
        reset: this.state.reset
      }));
    }
  }]);

  return BasePhoneInput;
}(BaseInput);

_defineProperty(BasePhoneInput, "propTypes", _objectSpread2(_objectSpread2({}, BaseInput.propTypes), {}, {
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  validation: PropTypes.func
}));

_defineProperty(BasePhoneInput, "defaultProps", {
  type: 'text',
  autoComplete: 'off'
});

var PhoneInputWithReset = /*#__PURE__*/function (_PhoneInput) {
  _inherits(PhoneInputWithReset, _PhoneInput);

  var _super2 = _createSuper(PhoneInputWithReset);

  function PhoneInputWithReset() {
    _classCallCheck(this, PhoneInputWithReset);

    return _super2.apply(this, arguments);
  }

  _createClass(PhoneInputWithReset, [{
    key: "componentDidUpdate",
    value: //added reset for library
    function componentDidUpdate(prevProps, prevState) {
      var _this3 = this;

      _get(_getPrototypeOf(PhoneInputWithReset.prototype), "componentDidUpdate", this) && _get(_getPrototypeOf(PhoneInputWithReset.prototype), "componentDidUpdate", this).call(this, prevProps, prevState);

      if (prevProps.reset !== this.props.reset) {
        var _this$state$onlyCount;

        var defaultSelectedCountry = (_this$state$onlyCount = this.state.onlyCountries) === null || _this$state$onlyCount === void 0 ? void 0 : _this$state$onlyCount.find(function (o) {
          return o.iso2 === _this3.props.defaultCountry;
        });

        _get(_getPrototypeOf(PhoneInputWithReset.prototype), "setState", this).call(this, {
          country: this.props.defaultCountry,
          selectedCountry: defaultSelectedCountry
        });
      }
    }
  }]);

  return PhoneInputWithReset;
}(PhoneInput);

var Field$5 = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'phone-field');
    }
  }, {
    key: "renderField",
    value: function renderField(props) {
      return /*#__PURE__*/React__default.createElement(BasePhoneInput, props);
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$5, "displayName", 'PhoneField');

var PhoneField = withFormDefaultValues(Field$5);

var _excluded$5 = ["country", "autoCompletePrefix"],
    _excluded2$2 = ["country", "countryField"];
var US = 'US';
var CA = 'CA';
var GB = 'GB';

var PostalCode = function PostalCode(_ref) {
  var country = _ref.country,
      _ref$autoCompletePref = _ref.autoCompletePrefix,
      autoCompletePrefix = _ref$autoCompletePref === void 0 ? 'billing' : _ref$autoCompletePref,
      props = _objectWithoutProperties(_ref, _excluded$5);

  var isUS = country && country.toUpperCase() === US;
  var label = isUS ? 'ZIP' : 'Postal Code';
  var autoComplete = autoCompletePrefix ? "".concat(autoCompletePrefix, " postal-code") : 'postal-code';
  return /*#__PURE__*/React__default.createElement(TextField, _extends$7({}, props, {
    label: label,
    placeholder: "Enter ".concat(label),
    autoComplete: autoComplete,
    maxLength: isUS ? 5 : null
  }));
};

PostalCode.propTypes = {
  autoCompletePrefix: PropTypes.string,
  country: PropTypes.string
};
PostalCode.defaultProps = {
  country: US
};

var createValidation = function createValidation(country) {
  return function (value) {
    var reUS = /^[0-9]{5}(?:-[0-9]{4})?$/;
    var reCA = /^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$/;
    var reGB = /^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$/;

    switch (country && country.toUpperCase()) {
      case US:
        return reUS.test(value) ? '' : 'Invalid ZIP code';

      case CA:
        return reCA.test(value) ? '' : 'not valid';

      case GB:
        return reGB.test(value) ? '' : 'not valid';

      default:
        return '';
    }
  };
};

var PostalCodeField = /*#__PURE__*/React__default.forwardRef(function (_ref2, ref) {
  var country = _ref2.country,
      countryField = _ref2.countryField,
      props = _objectWithoutProperties(_ref2, _excluded2$2);

  var _useFormValues = useFormValues(),
      countryValue = _useFormValues[countryField];

  var validation = useMemo(function () {
    return createValidation(country !== null && country !== void 0 ? country : countryValue);
  }, [country, countryValue]);
  return /*#__PURE__*/React__default.createElement(PostalCode, _extends$7({}, props, {
    ref: ref,
    country: country !== null && country !== void 0 ? country : countryValue,
    validation: validation
  }));
});
PostalCodeField.displayName = 'PostalCodeField';
PostalCodeField.propTypes = {
  autoCompletePrefix: PropTypes.string,
  country: PropTypes.string,
  countryField: PropTypes.string
};

var RadioButton = function RadioButton() {
  return /*#__PURE__*/React__default.createElement("span", {
    className: "radio-button"
  });
};

var _excluded$4 = ["value", "defaultValue", "className", "children", "style", "buttonComponent"];
var RadioOption = function RadioOption(_ref) {
  var value = _ref.value,
      defaultValue = _ref.defaultValue,
      className = _ref.className,
      children = _ref.children,
      style = _ref.style,
      Button = _ref.buttonComponent,
      props = _objectWithoutProperties(_ref, _excluded$4);

  return props.name ? /*#__PURE__*/React__default.createElement("label", {
    className: cn('radio-option option', Button && 'hidden-input', className),
    style: style
  }, /*#__PURE__*/React__default.createElement("input", _extends$7({}, props, {
    type: "radio",
    defaultChecked: value === defaultValue,
    value: value
  })), Button && /*#__PURE__*/React__default.createElement(Button, null), /*#__PURE__*/React__default.createElement("span", {
    className: "option-label"
  }, children)) : false;
};
RadioOption.propTypes = {
  buttonComponent: PropTypes.elementType,
  defaultValue: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  style: PropTypes.shape({
    '--index': PropTypes.number
  }),
  value: PropTypes.string
};
RadioOption.defaultProps = {
  buttonComponent: RadioButton
};

var _excluded$3 = ["horizontal"],
    _excluded2$1 = ["label", "hideValidationMessage", "validationMessage", "className", "children", "horizontal", "vertical", "touched"];
var Field$4 = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "renderField",
    value: function renderField(props) {
      return /*#__PURE__*/React__default.createElement(RadioInputGroup, _extends$7({}, props, {
        optionComponent: RadioOption
      }), this.props.children);
    }
  }, {
    key: "getInputProps",
    value: function getInputProps() {
      var _get$call = _get(_getPrototypeOf(Field.prototype), "getInputProps", this).call(this);
          _get$call.horizontal;
          var props = _objectWithoutProperties(_get$call, _excluded$3);

      return props;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          label = _this$props.label;
          _this$props.hideValidationMessage;
          _this$props.validationMessage;
          _this$props.className;
          _this$props.children;
          var horizontal = _this$props.horizontal;
          _this$props.vertical;
          _this$props.touched;
          var props = _objectWithoutProperties(_this$props, _excluded2$1);

      var editable = !props.disabled && !props.readOnly;
      return /*#__PURE__*/React__default.createElement("fieldset", {
        name: props.name,
        className: this.getWrapperClassName('radio-button-group', horizontal && 'horizontal')
      }, /*#__PURE__*/React__default.createElement(LabelText, {
        as: "legend",
        className: cn(editable && props.required && 'required')
      }, label), this.renderField(this.getInputProps()), this.shouldShowValidity() && this.renderValidationMessage());
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$4, "displayName", 'RadioButtonGroup');

var RadioButtonGroup = withFormDefaultValues(Field$4);

var _excluded$2 = ["defaultValue"];
var SecureField = /*#__PURE__*/function (_BaseField) {
  _inherits(SecureField, _BaseField);

  var _super = _createSuper(SecureField);

  function SecureField() {
    var _this;

    _classCallCheck(this, SecureField);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", {
      isOpen: false
    });

    _defineProperty(_assertThisInitialized(_this), "toggleOpen", function () {
      _this.setState(function (state) {
        return {
          isOpen: !state.isOpen
        };
      });
    });

    return _this;
  }

  _createClass(SecureField, [{
    key: "className",
    get:
    /** @private */
    function get() {
      return this.getWrapperClassName(this.props.className);
    }
  }, {
    key: "wrapLabel",
    value: function wrapLabel(label) {
      var isEditable = !this.props.disabled && !this.props.readOnly;
      return !isEditable ? label : !this.props.defaultValue ? label : this.state.isOpen ? /*#__PURE__*/React__default.createElement(React__default.Fragment, null, label, "\xA0", /*#__PURE__*/React__default.createElement(Button, {
        className: "inline",
        onClick: this.toggleOpen
      }, "Discard")) : /*#__PURE__*/React__default.createElement(React__default.Fragment, null, label, "\xA0", /*#__PURE__*/React__default.createElement(Button, {
        className: "inline",
        onClick: this.toggleOpen
      }, "Edit"));
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      _get(_getPrototypeOf(SecureField.prototype), "componentDidUpdate", this) && _get(_getPrototypeOf(SecureField.prototype), "componentDidUpdate", this).call(this); //touched changed true => false when form was reset

      if (!this.props.touched && prevProps !== null && prevProps !== void 0 && prevProps.touched) {
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState({
          isOpen: false
        });
      }
    }
  }, {
    key: "renderSecuredField",
    value: function renderSecuredField(_ref) {
      var defaultValue = _ref.defaultValue,
          props = _objectWithoutProperties(_ref, _excluded$2);

      var isOpen = this.state.isOpen;

      if (defaultValue && !isOpen) {
        return /*#__PURE__*/React__default.createElement(Field$c, _extends$7({}, props, {
          defaultValue: defaultValue.replace(/\*/g, '∗'),
          disabled: true,
          type: "text"
        }));
      }

      return this.renderField(_objectSpread2({}, props));
    }
    /** @protected */

  }, {
    key: "renderField",
    value: function renderField() {
      throw 'override renderField(props)';
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React__default.createElement(Label, {
        label: this.wrapLabel(this.label),
        name: this.props.name,
        className: this.className
      }, this.renderSecuredField(this.getInputProps()), this.props.children, this.shouldShowValidity() && this.renderValidationMessage());
    }
  }]);

  return SecureField;
}(BaseField);

_defineProperty(SecureField, "propTypes", {
  component: PropTypes.elementType,
  defaultValue: PropTypes.any,
  disabled: PropTypes.bool,
  excludedProps: PropTypes.array,
  label: PropTypes.node,
  name: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  touched: PropTypes.bool,
  validation: PropTypes.func
});

var Field$3 = /*#__PURE__*/function (_SecureField) {
  _inherits(Field, _SecureField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'date');
    }
  }, {
    key: "renderField",
    value: function renderField(props) {
      return /*#__PURE__*/React__default.createElement(DateInput, props);
    }
  }]);

  return Field;
}(SecureField);

_defineProperty(Field$3, "displayName", 'SecureDOBField');

var SecureDOBField = withFormDefaultValues(Field$3);
SecureDOBField.propTypes = {
  autoComplete: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.node,
  name: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  readOnly: PropTypes.bool,
  required: PropTypes.bool,
  validation: PropTypes.func
};

var _PAYMENT_METHOD_TYPE, _INVESTOR_TYPE_OPTION;
PropTypes.oneOfType([PropTypes.string, PropTypes.exact({
  pathname: PropTypes.string.isRequired,
  state: PropTypes.exact({
    noScroll: PropTypes.bool,
    redirect: PropTypes.string
  })
})]); // Payments
// ---------------------------------------------------------------------------

var ACH = 'ach';
var WIRE = 'wire';
var PAYMENT_METHOD_TYPE = (_PAYMENT_METHOD_TYPE = {}, _defineProperty(_PAYMENT_METHOD_TYPE, WIRE, 'Wire'), _defineProperty(_PAYMENT_METHOD_TYPE, ACH, 'Ach'), _PAYMENT_METHOD_TYPE);
PropTypes.oneOf(Object.keys(PAYMENT_METHOD_TYPE)); // Country
// -----------------------------------------------------------------------------

PropTypes.shape({
  code: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
}); // State
// -----------------------------------------------------------------------------

PropTypes.shape({
  code: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
}); // LISTS
// ------------

var INVESTOR_TYPE = {
  PERSON: 'person',
  COMPANY: 'company'
};
(_INVESTOR_TYPE_OPTION = {}, _defineProperty(_INVESTOR_TYPE_OPTION, INVESTOR_TYPE.COMPANY, 'Company'), _defineProperty(_INVESTOR_TYPE_OPTION, INVESTOR_TYPE.PERSON, 'Person'), _INVESTOR_TYPE_OPTION);

var _excluded$1 = ["country", "ownerType"],
    _excluded2 = ["defaultValue"],
    _excluded3 = ["ownerType", "country", "defaultValue"],
    _excluded4 = ["defaultValue"],
    _excluded5 = ["ownerType", "country", "countryField", "ownerTypeField"];
var US_CONFIG = {
  delimiter: ' - ',
  blocks: [3, 2, 4],
  numericOnly: true
};
var US_CONFIG_COMPANY = {
  delimiter: ' - ',
  blocks: [2, 7],
  numericOnly: true
};
var CONFIG = {};
var Field$2 = /*#__PURE__*/function (_SecureField) {
  _inherits(Field, _SecureField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getInputProps",
    value: function getInputProps() {
      var _get$call = _get(_getPrototypeOf(Field.prototype), "getInputProps", this).call(this);
          _get$call.country;
          _get$call.ownerType;
          var props = _objectWithoutProperties(_get$call, _excluded$1);

      return props;
    }
  }, {
    key: "renderNonUsInput",
    value: function renderNonUsInput(_ref) {
      _ref.defaultValue;
          var props = _objectWithoutProperties(_ref, _excluded2);

      return /*#__PURE__*/React__default.createElement(FormattedInput, _extends$7({}, props, {
        key: "non-us",
        options: CONFIG
      }));
    }
  }, {
    key: "renderUsPersonalInput",
    value: function renderUsPersonalInput(_ref2) {
      _ref2.ownerType;
          _ref2.country;
          _ref2.defaultValue;
          var props = _objectWithoutProperties(_ref2, _excluded3);

      return /*#__PURE__*/React__default.createElement(FormattedInput, _extends$7({}, props, {
        key: "us-personal",
        options: US_CONFIG,
        placeholder: "XXX-XX-XXXX"
      }));
    }
  }, {
    key: "renderUsCompanyInput",
    value: function renderUsCompanyInput(_ref3) {
      _ref3.defaultValue;
          var props = _objectWithoutProperties(_ref3, _excluded4);

      return /*#__PURE__*/React__default.createElement(FormattedInput, _extends$7({}, props, {
        key: "us-company",
        options: US_CONFIG_COMPANY,
        placeholder: "XX-XXXXXXX"
      }));
    }
  }, {
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'field box-field tax-id-field');
    }
  }, {
    key: "label",
    get: function get() {
      var _this$props = this.props,
          country = _this$props.country,
          ownerType = _this$props.ownerType;
      var investorCountry = country;
      var investorOwnerType = ownerType;
      var isUS = (investorCountry || '').toUpperCase() === 'US';
      return isUS ? investorOwnerType === INVESTOR_TYPE.PERSON ? /*#__PURE__*/React__default.createElement("abbr", {
        title: "Social Security Number"
      }, "SSN") : /*#__PURE__*/React__default.createElement("abbr", {
        title: "Employer Identification Number"
      }, "EIN") : 'Tax ID';
    }
  }, {
    key: "renderField",
    value: function renderField(props) {
      var _this$props2 = this.props,
          ownerType = _this$props2.ownerType,
          country = _this$props2.country;

      if ((country || '').toUpperCase() !== 'US') {
        return this.renderNonUsInput(props);
      }

      if (ownerType === INVESTOR_TYPE.PERSON) {
        return this.renderUsPersonalInput(props);
      }

      if (ownerType === INVESTOR_TYPE.COMPANY) {
        return this.renderUsCompanyInput(props);
      }

      throw new Error('Unexpected state');
    }
  }]);

  return Field;
}(SecureField);

_defineProperty(Field$2, "displayName", 'SecureTaxIdField');

_defineProperty(Field$2, "propTypes", _objectSpread2(_objectSpread2({}, _get(_getPrototypeOf(Field$2), "propTypes", Field$2)), {}, {
  country: PropTypes.string,
  // optional
  ownerType: PropTypes.oneOf(Object.values(INVESTOR_TYPE)) // optional

}));

_defineProperty(Field$2, "defaultProps", _objectSpread2(_objectSpread2({}, _get(_getPrototypeOf(Field$2), "defaultProps", Field$2)), {}, {
  country: 'US',
  ownerType: INVESTOR_TYPE.PERSON
}));

var BoundField = withFormDefaultValues(Field$2);
var SecuredTaxIdField = /*#__PURE__*/React__default.forwardRef(function (_ref4, ref) {
  var ownerType = _ref4.ownerType,
      country = _ref4.country,
      countryField = _ref4.countryField,
      ownerTypeField = _ref4.ownerTypeField,
      props = _objectWithoutProperties(_ref4, _excluded5);

  var _useFormValues = useFormValues(),
      countryFieldValue = _useFormValues[countryField],
      ownerTypeFieldValue = _useFormValues[ownerTypeField];

  return /*#__PURE__*/React__default.createElement(BoundField, _extends$7({}, props, {
    country: country !== null && country !== void 0 ? country : countryFieldValue,
    ownerType: ownerType !== null && ownerType !== void 0 ? ownerType : ownerTypeFieldValue,
    ref: ref
  }));
});
SecuredTaxIdField.displayName = "with(".concat(BoundField.displayName, ")");
SecuredTaxIdField.propTypes = {
  country: PropTypes.string,
  countryField: PropTypes.string,
  ownerType: PropTypes.string,
  ownerTypeField: PropTypes.string
};

var _path;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function SvgDown(props) {
  return /*#__PURE__*/React.createElement("svg", _extends({
    xmlns: "http://www.w3.org/2000/svg",
    width: 15,
    height: 15
  }, props), _path || (_path = /*#__PURE__*/React.createElement("path", {
    d: "M7.364 8.45l5.297-5.296a.5.5 0 01.717-.004l.7.7a.507.507 0 01-.003.718L7.72 10.922a.507.507 0 01-.717-.004l-6.35-6.35A.5.5 0 01.65 3.85l.7-.7a.507.507 0 01.718.004L7.364 8.45z"
  })));
}

var _excluded = ["className"];
var Field$1 = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'select-container');
    }
  }, {
    key: "renderField",
    value: function renderField(_ref) {
      var className = _ref.className,
          props = _objectWithoutProperties(_ref, _excluded);

      return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement(Select$1, _extends$7({}, props, {
        className: cn(className, 'select')
      })), /*#__PURE__*/React__default.createElement(SvgDown, {
        className: "select-icon"
      }));
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field$1, "displayName", 'Select');

var Select = withFormDefaultValues(Field$1);

var Field = /*#__PURE__*/function (_BaseField) {
  _inherits(Field, _BaseField);

  var _super = _createSuper(Field);

  function Field() {
    _classCallCheck(this, Field);

    return _super.apply(this, arguments);
  }

  _createClass(Field, [{
    key: "getWrapperClassName",
    value: function getWrapperClassName() {
      return _get(_getPrototypeOf(Field.prototype), "getWrapperClassName", this).call(this, 'textarea');
    }
  }, {
    key: "renderField",
    value: function renderField(_ref) {
      var props = _extends$7({}, _ref);

      return /*#__PURE__*/React__default.createElement(TextArea, props);
    }
  }]);

  return Field;
}(BaseField);

_defineProperty(Field, "displayName", 'TextArea');

var TextareaField = withFormDefaultValues(Field);

export { AccountNumberField, AddMatchFieldValidation, BaseInput, Button, Checkbox, CheckboxGroup, CurrencyField, SecureDOBField as DOBField, DateField, EmailField, FieldValue, FileField, Form, FormControl, HiddenField, IconButton, IsFieldModified, IsFieldValue, IsInvalid, IsModified, IsValid, NewPasswordField, NumberField, PasswordField, PhoneField, PostalCodeField, RadioButtonGroup, ResetButton, SaveButton, SecretField, Select, SubmitButton, SecuredTaxIdField as TaxIdField, TextField, TextareaField, ToggleButton, useField, useFormProps, useFormValues };
//# sourceMappingURL=index.modern.js.map
