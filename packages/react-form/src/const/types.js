import PropTypes from 'prop-types'

export const RoutePath = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.exact({
    pathname: PropTypes.string.isRequired,
    state: PropTypes.exact({
      noScroll: PropTypes.bool,
      redirect: PropTypes.string,
    }),
  }),
])

// Payments
// ---------------------------------------------------------------------------
export const ACH = 'ach'
export const WIRE = 'wire'
export const PAYMENT_METHOD_TYPE = {
  [WIRE]: 'Wire',
  [ACH]: 'Ach',
}
export const PAYMENT_METHODS = Object.keys(PAYMENT_METHOD_TYPE)
export const ACCOUNT_TYPE = {
  checking: 'Checking',
  savings: 'Saving',
}
export const CHECK_TYPE = {
  business: 'Business',
  personal: 'Personal',
}
export const PaymentMethod = PropTypes.oneOf(Object.keys(PAYMENT_METHOD_TYPE))

// Country
// -----------------------------------------------------------------------------
export const Country = PropTypes.shape({
  code: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
})

// State
// -----------------------------------------------------------------------------
export const State = PropTypes.shape({
  code: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
})

// LISTS
// -----------------------------------------------------------------------------
export const ORDER = {
  ASC: 'ASC',
  DESC: 'DESC',
  DEFAULT: 'ASC',
}

//
// ------------
export const INVESTOR_TYPE = {
  PERSON: 'person',
  COMPANY: 'company',
}
export const INVESTOR_TYPE_OPTIONS = {
  [INVESTOR_TYPE.COMPANY]: 'Company',
  [INVESTOR_TYPE.PERSON]: 'Person',
}
