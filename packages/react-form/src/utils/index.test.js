import { cn, toServerDate, toISODate, toHumanDateTime, todayDate, nowTimestamp, isFutureTimestamp, timestampAfter, isInputDateSupported } from '.'

describe('utils index test', () => {
  describe('cn test', () => {
    it('must returned "default test" when parameter is true', () => {
      const trueBull = true
      expect(cn('default', trueBull && 'test')).toEqual('default test')
    })
    it('must returned "default" when parameter is false', () => {
      const trueBull = false
      expect(cn('default', trueBull && 'test')).toEqual('default')
    })
  })
  describe('toServerDate test', () => {
    const date = '2019-01-21'
    it('if has value should return value in format "M/D/YYYY"', () => {
      expect(toServerDate(date)).toEqual('1/21/2019')
    })
    it('if no value should return "" ', () => {
      expect(toServerDate('')).toEqual(null)
    })
  })
  describe('toHumanDateTime', () => {
    it('It should convert to date with name of Month and time with time zone', () => {
      const date = '2019-01-21T03:24:00Z'
      expect(toHumanDateTime(date)).toEqual('Jan 21, 2019, 3:24am')
    })
    it('It should return empty string if no date', () => {
      const date = ''
      expect(toHumanDateTime(date)).toEqual('')
    })
  })
  describe('toISODate', () => {
    it('It must match YYYY-DD-MM format', () => {
      expect(toISODate('11/21/2019')).toMatch(/(\d{4})[- /.](\d{2})[- /.](\d{2})/g)
    })
  })
  describe('todayDate', () => {
    it('It must match YYYY-DD-MM format', () => {
      expect(todayDate()).toMatch(/(\d{4})[- /.](\d{2})[- /.](\d{2})/g)
    })
  })
  describe('nowTimestamp', () => {
    it('It must return number type', () => {
      expect(typeof nowTimestamp()).toBe('number')
    })
  })
  describe('isFutureTimestamp', () => {
    it('It must return true for the future date', () => {
      let targetDate = new Date()
      targetDate.setDate(targetDate.getDate() + 10)
      expect(isFutureTimestamp(targetDate.valueOf())).toBe(true)
    })
    it('It must return false for the past date', () => {
      let targetDate = new Date()
      targetDate.setDate(targetDate.getDate() - 10)
      expect(isFutureTimestamp(targetDate.valueOf())).toBe(false)
    })
    it('It must return false for the same date', () => {
      expect(isFutureTimestamp(new Date().valueOf())).toBe(false)
    })
  })
  describe('timestampAfter', () => {
    it('It must bigger timestamp', () => {
      const timeStamp = new Date('11/21/2019').valueOf()
      expect(timestampAfter(timeStamp) > timeStamp).toBe(true)
    })
  })
  describe('isInputDateSupported', () => {
    it('It should emulate creating input and return true', () => {
      expect(isInputDateSupported()).toBe(true)
    })
  })
})
