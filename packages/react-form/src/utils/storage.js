let localStorage = null
try {
  localStorage = window.localStorage
} catch (e) {
  //
}
const read = (key) => {
  try {
    return JSON.parse(localStorage.getItem(key))
  } catch (e) {
    return null
  }
}
const write = (key, value) => {
  if (value === null || value === undefined) {
    localStorage.removeItem(key)
    return
  }
  try {
    localStorage.setItem(key, JSON.stringify(value))
  } catch (e) {
    // limit exceeded?
    // eslint-disable-next-line no-console
    console.error(e)
  }
}

const addListener = (key, cb) => {
  const listener = (event) => {
    if (event.key === null || (event.key === key && event.newValue !== event.oldValue)) {
      let value
      try {
        value = event.newValue ? JSON.parse(event.newValue) : event.newValue
      } catch (e) {
        value = null
      }
      cb(value)
    }
  }

  window.addEventListener('storage', listener)

  return () => {
    window.removeEventListener('storage', listener)
  }
}

export const storage = {
  read,
  write,
  addListener,
}
