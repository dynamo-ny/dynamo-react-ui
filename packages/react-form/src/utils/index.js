/**
 *
 * @param  {...String|null|undefined|false} names
 * @returns {String}
 */
export const cn = (...names) =>
  names.filter((value) => !!value && typeof value === 'string').join(' ')

/**
 * @param {String} value - date in ISO (YYYY-MM-DD) format
 * @returns {String} date in server-side format
 */
export const toServerDate = (value) => (value ? new Date(value).toLocaleDateString('en-US') : null)

export const toISODate = (value) => {
  const date = new Date(Date.parse(value))
  if (!date || isNaN(date)) return null
  return [
    date.getFullYear(),
    (date.getMonth() + 1).toString().padStart(2, '0'),
    date.getDay().toString().padStart(2, '0'),
  ].join('-')
}

export const toHumanDateTime = (value) => {
  if (!value) return value
  const date = new Date(value)
  return date
    .toLocaleString('en-US', {
      timeZone: 'UTC',
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: undefined,
    })
    .replace(' PM', 'pm')
    .replace(' AM', 'am')
}

export const todayDate = () => new Date().toISOString().replace(/T.*/, '')

export const nowTimestamp = () => new Date().valueOf()
export const timestampAfter = (seconds) => (seconds ? nowTimestamp() + (seconds - 1) * 1000 : null)

export const isFutureTimestamp = (timestamp) => nowTimestamp() < timestamp

export const isIOS = () => {
  if (typeof window === 'undefined' || typeof navigator === 'undefined') return false
  navigator.userAgent.match(/ipad|iphone/i)
}

export const iosCopy = (input) => {
  if (typeof window === 'undefined' || typeof navigator === 'undefined') return
  const range = document.createRange()
  range.selectNodeContents(input)
  const selection = window.getSelection()
  selection.removeAllRanges()
  selection.addRange(range)
  input.setSelectionRange(0, 999999)
  document.execCommand('copy')
}

export const isInputDateSupported = () => {
  if (typeof window === 'undefined' || typeof navigator === 'undefined') return false
  const input = document.createElement('input')
  input.type = 'date'
  return input.type === 'date'
}
