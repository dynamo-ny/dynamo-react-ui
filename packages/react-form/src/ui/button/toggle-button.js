import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

import { Button } from './button'

export const ToggleButton = React.forwardRef(
  ({ open: _open = false, className, onToggle, onClick, ...props }, ref) => {
    const [open, setOpen] = useState(_open)
    const toggle = useCallback(
      (event) => {
        setOpen(!open)
        onClick && onClick(event)
        onToggle && onToggle(!open)
      },
      [onClick, onToggle, open],
    )

    return (
      <Button
        {...props}
        className={cn('toggle', className)}
        aria-expanded={open ? true : undefined}
        onClick={toggle}
        ref={ref}
      />
    )
  },
)
ToggleButton.displayName = 'ToggleButton'
ToggleButton.propTypes = {
  onClick: PropTypes.func,
  onToggle: PropTypes.func,
  open: PropTypes.bool,
}
