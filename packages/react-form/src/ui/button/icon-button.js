import './icon-button.css'
import React from 'react'

import { cn } from 'utils'

export const IconButton = ({ className, ...props }) => (
  <button type="button" {...props} className={cn('icon-button', className)} />
)
