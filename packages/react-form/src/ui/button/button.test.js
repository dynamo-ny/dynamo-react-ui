import React from 'react'
import { shallow } from 'enzyme'

import { Button, IconButton } from '.'

describe('<Button />', () => {
  it('match snapshot', () => {
    expect(shallow(<Button>Test</Button>)).toMatchSnapshot()
  })
  it('type=submit snapshot', () => {
    expect(shallow(<Button type="submit">Test</Button>)).toMatchSnapshot()
  })
  it('type=reset snapshot', () => {
    expect(shallow(<Button type="reset">Test</Button>)).toMatchSnapshot()
  })
})

describe('<IconButton />', () => {
  it('match snapshot', () => {
    expect(shallow(<IconButton>x</IconButton>)).toMatchSnapshot()
  })
})
