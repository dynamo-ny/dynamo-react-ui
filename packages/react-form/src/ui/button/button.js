import './button.css'
import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

export const Button = React.forwardRef(
  (
    {
      className: customClassName,
      type,
      theme,
      small,
      wide,
      children,
      rightIcon: RightIcon,
      leftIcon: LeftIcon,
      ...props
    },
    ref,
  ) => {
    const className = cn(
      'button',
      wide && 'wide',
      small && 'small',
      theme,
      !theme && type === 'submit' && 'primary',
      !theme && type === 'reset' && 'secondary',
      customClassName,
    )

    return (
      // eslint-disable-next-line react/button-has-type
      <button {...props} type={type} className={className} ref={ref}>
        {LeftIcon && <LeftIcon className="left" />}
        {children}
        {RightIcon && <RightIcon className="right" />}
      </button>
    )
  },
)

Button.displayName = 'Button'
Button.propTypes = {
  leftIcon: PropTypes.func,
  rightIcon: PropTypes.func,
  small: PropTypes.bool,
  theme: PropTypes.oneOf([
    'primary',
    'secondary',
    'alert',
    'success',
    'inline',
    'flat',
    'option-like',
  ]),
  type: PropTypes.oneOf(['submit', 'reset', 'button']),
  wide: PropTypes.bool,
}
Button.defaultProps = {
  type: 'button',
  wide: false,
  small: false,
}
