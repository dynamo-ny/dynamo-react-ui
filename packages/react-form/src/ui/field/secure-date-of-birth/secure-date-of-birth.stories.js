import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'

import { SecureDOBField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: SecureDOBField,
  title: 'Form/Fields/Secured Date of Birth',
  decorators: [withKnobs],
}

export const secured_data_of_birth = () => (
  <Demo component={SecureDOBField} defaultValue={text('defaultValue', '1985-**-**')} />
)
