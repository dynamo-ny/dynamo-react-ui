import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'

import { NewPasswordField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: NewPasswordField,
  title: 'Form/Fields/New Password',
  decorators: [withKnobs],
}

export const new_password = () => (
  <Demo component={NewPasswordField} placeholder={text('placeholder', 'placeholder')} />
)
