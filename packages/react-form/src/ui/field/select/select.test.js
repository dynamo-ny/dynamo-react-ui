/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { Form } from 'ui/form/form'

import { Select } from '.'

describe('<Select />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(
        shallow(
          <Select
            name="select1"
            defaultValue="a"
            options={{ a: 1, b: 2, c: 3 }}
            label="with default value"
          />,
        ),
      ).toMatchSnapshot()
    })
    it('wrapped in label', () => {
      const wrapper = render(
        <Select
          name="select1"
          defaultValue="a"
          options={{ a: 1, b: 2, c: 3 }}
          label="with default value"
        />,
      )
      expect(wrapper.is('label')).toEqual(true)
      expect(wrapper.find('.label-text').text()).toEqual('with default value')
    })
    it('select control respects name, is not disabled nor read-only', () => {
      const wrapper = render(
        <Select
          name="select1"
          defaultValue="a"
          options={{ a: 1, b: 2, c: 3 }}
          label="with default value"
        />,
      )
      const input = wrapper.find('select')
      expect(input.is('[name=select1]')).toBe(true)
      expect(input.is(':disabled')).toBe(false)
      expect(input.is('[readOnly]')).toBe(false)
    })
  })
  describe('props', () => {
    it('label: outputs label text', () => {
      const wrapper = render(
        <Select name="select1" defaultValue="a" options={{ a: 1, b: 2, c: 3 }} label="Test" />,
      )
      expect(wrapper.text()).toEqual(expect.stringContaining('Test'))
    })
    it('disabled: passed to input', () => {
      const wrapper = render(
        <Select
          name="select1"
          defaultValue="a"
          disabled
          options={{ a: 1, b: 2, c: 3 }}
          label="Test"
        />,
      )
      expect(wrapper.find('select').is(':disabled')).toBe(true)
    })
    it('readOnly: passed to input', () => {
      const wrapper = render(
        <Select
          name="select1"
          defaultValue="a"
          readOnly
          options={{ a: 1, b: 2, c: 3 }}
          label="Test"
        />,
      )
      expect(wrapper.find('select').is('[readOnly]')).toBe(true)
    })
  })
  describe('methods', () => {
    it('instance has "value" getter & setter', () => {
      const submit = jest.fn()
      const ref = React.createRef()
      const props = {
        name: 'select1',
        defaultValue: 'b',
        label: 'Test',
        options: { a: 'Option', b: 'Option', c: 'Option' },
      }
      const wrapper = mount(
        <Form onSubmit={submit}>
          <Select {...props} ref={ref} />
        </Form>,
      )
      expect(ref.current.value).toEqual('b')
      ref.current.value = 'c'
      expect(wrapper.find(Form).instance().getValues()).toEqual({ select1: 'c' })
      expect(ref.current.value).toEqual('c')
    })
  })

  describe('parent <Form />', () => {
    it('receives value updates', () => {
      const ref = React.createRef()
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Select name="select-field-test" label="Test" defaultValue="init" ref={ref} />
        </Form>,
      )
      ref.current.value = 'changed'
      expect(wrapper.instance().getValues()).toEqual({ 'select-field-test': 'changed' })
    })
    it('receives validity updates', () => {
      const ref = React.createRef()
      const options = { a: 'aaa', b: 'bbb' }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Select
            name="select-field-test-2"
            label="Test"
            defaultValue="a"
            options={options}
            required
            ref={ref}
          />
        </Form>,
      )
      expect(wrapper.instance().getFieldsValidity()).toEqual({ 'select-field-test-2': true })
      ref.current.value = null
      expect(wrapper.instance().getFieldsValidity()).toEqual({ 'select-field-test-2': false })
    })

    it('on reset, value=defaultValue', () => {
      const ref = React.createRef()
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Select
            name="test"
            label="Test"
            defaultValue="init"
            ref={ref}
            options={{ init: 'Init', changed: 'Changed' }}
          />
        </Form>,
      )
      const form = wrapper.instance()
      expect(form.getValues()).toEqual({ test: 'init' })
      ref.current.value = 'changed'
      expect(form.getValues()).toEqual({ test: 'changed' })
      form.reset()
      expect(form.getValues()).toEqual({ test: 'init' })
    })
  })

  describe('Inherits props from <Form />', () => {
    it('disabled', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()} disabled>
          <Select name="test" label="Test" defaultValue="init" />
        </Form>,
      )
      expect(wrapper.find('select').is('[disabled]')).toBe(true)
    })
    it('readOnly', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()} readOnly>
          <Select name="test" label="Test" defaultValue="init" />
        </Form>,
      )
      expect(wrapper.find('select').is('[readOnly]')).toBe(true)
    })

    it('defaultValues', () => {
      const ref = React.createRef()
      mount(
        <Form onSubmit={jest.fn()} defaultValues={{ test: 'b' }}>
          <Select name="test" label="Test" ref={ref} options={{ a: 'a', b: 'b' }} />
        </Form>,
      )
      expect(ref.current.value).toEqual('b')
    })
  })
})
