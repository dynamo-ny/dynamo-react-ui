import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'

import { EmailField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: EmailField,
  title: 'Form/Fields/Email',
  decorators: [withKnobs],
}

export const email = () => (
  <Demo component={EmailField} defaultValue={text('defaultValue', 'user@email.com')} />
)
