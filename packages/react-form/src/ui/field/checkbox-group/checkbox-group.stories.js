import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'

import { Field, CheckboxGroup } from '.'
import { Demo } from '../storybook-field-demo'

const options = {
  value1: 'First Option',
  value2: 'Second Option',
  value3: 'Third Option with some quite long text that is expected to wrap into a few lines',
  value4: 'Fourth Option',
}

export default {
  component: Field,
  title: 'Form/Fields/Checkbox Group',
  decorators: [withKnobs],
}

export const checkbox_group = () => (
  <Demo
    component={CheckboxGroup}
    defaultValue={text('defaultValue', 'value1')}
    delimiter={text('delimiter', ',')}
    options={options}
  />
)
