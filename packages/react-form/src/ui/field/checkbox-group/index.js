import './checkbox-group.css'
import React from 'react'

import { CheckboxInputGroup } from 'ui/form'

import { cn } from 'utils'

import { BaseField, withFormDefaultValues } from '../base-field'
import { LabelText } from '../label'
import { CheckboxOption } from './option/checkbox-option'

export class Field extends BaseField {
  static displayName = 'CheckboxGroup'

  renderField(props) {
    return (
      <CheckboxInputGroup {...props} optionComponent={CheckboxOption}>
        {this.props.children}
      </CheckboxInputGroup>
    )
  }

  render() {
    const {
      label,
      hideValidationMessage,
      validationMessage,
      className,
      children,
      horizontal,
      vertical,
      touched,
      ...props
    } = this.props

    const editable = !props.disabled && !props.readOnly
    return (
      <fieldset
        name={props.name}
        className={this.getWrapperClassName('checkbox-group', horizontal && 'horizontal')}
      >
        <LabelText as="legend" className={cn(editable && props.required && 'required')}>
          {label}
        </LabelText>
        {this.renderField(this.getInputProps())}
        {this.shouldShowValidity() && this.renderValidationMessage()}
      </fieldset>
    )
  }
}

export const CheckboxGroup = withFormDefaultValues(Field)
