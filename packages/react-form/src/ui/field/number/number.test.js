/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { Form } from 'ui/form/form'
import { NumberInput } from 'ui/form/number-input'

import { NumberField } from '.'

describe('<NumberInput />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(shallow(<NumberField name="test" label="Test" />)).toMatchSnapshot()
    })

    it('<input/>', () => {
      const wrapper = render(<NumberField name="test" label="Test" />)
      const input = wrapper.find('input')
      expect(input.is('[type=number]')).toBe(true)
      expect(input.is('[name=test]')).toBe(true)
      expect(input.is(':disabled')).toBe(false)
      expect(input.is('[readOnly]')).toBe(false)
    })
    it('is <label />', () => {
      const wrapper = render(<NumberField name="test" label="Test" />)
      expect(wrapper.is('label')).toEqual(true)
    })
    it('outputs label', () => {
      const wrapper = render(<NumberField name="test" label="Test" />)
      expect(wrapper.text()).toEqual(expect.stringContaining('Test'))
    })
    it('disabled -> <input/>', () => {
      const wrapper = render(<NumberField name="test" label="Test" disabled />)
      expect(wrapper.find('input').is(':disabled')).toBe(true)
    })
    it('readOnly -> <input/>', () => {
      const wrapper = render(<NumberField name="test" label="Test" readOnly />)
      expect(wrapper.find('input').is('[readOnly]')).toBe(true)
    })
    it('value -> <input/>', () => {
      const wrapper = render(<NumberField name="test" label="Test" defaultValue={1} />)
      expect(wrapper.find('input').val()).toEqual('1')
    })
  })
  describe('methods', () => {
    it('instance has "value" getter & setter', () => {
      const submit = jest.fn()
      const props = {
        name: 'test',
        defaultValue: 1,
        label: 'Test',
      }
      const wrapper = mount(
        <Form onSubmit={submit}>
          <NumberField {...props} />
        </Form>,
      )
      expect(wrapper.find(NumberInput).instance().getValue()).toEqual(1)
      wrapper.find(NumberInput).instance().value = 2
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: 2 })
      expect(wrapper.find(NumberInput).instance().value).toEqual(2)
    })
    it('input type number changed only on number type', () => {
      const submit = jest.fn()
      const props = {
        name: 'test',
        defaultValue: 10,
        label: 'Test',
      }
      const wrapper = mount(
        <Form onSubmit={submit}>
          <NumberField {...props} />
        </Form>,
      )
      expect(wrapper.find(NumberInput).instance().getValue()).toEqual(10)
      wrapper.find(NumberInput).instance().value = 'changed value'
      expect(wrapper.find(NumberInput).instance().getValue()).toEqual(null)
      wrapper.find(NumberInput).instance().value = 15
      expect(wrapper.find(NumberInput).instance().getValue()).toEqual(15)
    })
  })

  describe('on prop update', () => {
    it('defaultValue: value=defaultValue', () => {
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      const Tst = ({ defaultValue }) => (
        <Form onSubmit={jest.fn()}>
          <NumberField name="test" label="Test" defaultValue={defaultValue} />
        </Form>
      )
      const wrapper = mount(<Tst defaultValue={10} />)
      wrapper.find('input').simulate('change', { target: { value: '15' } })
      wrapper.setProps({ defaultValue: 20 })
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: 20 })
    })
  })
})
