import React from 'react'
import { withKnobs, number } from '@storybook/addon-knobs'

import { Field, NumberField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: Field,
  title: 'Form/Fields/Number',
  decorators: [withKnobs],
}

export const number_ = () => (
  <Demo
    component={NumberField}
    min={number('Min', undefined)}
    max={number('Max', undefined)}
    step={number('Step', 1)}
    defaultValue={number('Default Value', 1)}
  />
)
