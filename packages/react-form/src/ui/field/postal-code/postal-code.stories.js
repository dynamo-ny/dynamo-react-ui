import React from 'react'
import { withKnobs, radios } from '@storybook/addon-knobs'

import { PostalCodeField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: PostalCodeField,
  title: 'Form/Fields/Postal Code',
  decorators: [withKnobs],
}

export const postal_code = () => (
  <Demo
    component={PostalCodeField}
    country={radios(
      'Country',
      { 'United States': 'US', Canada: 'CA', 'Great Britain': 'GB', Ukraine: 'UA' },
      'US',
    )}
  />
)
