import React, { useMemo } from 'react'
import PropTypes from 'prop-types'

import { TextField } from 'ui/field/text'
import { useFormValues } from 'ui/form/context'

const US = 'US'
const CA = 'CA'
const GB = 'GB'

const PostalCode = ({ country, autoCompletePrefix = 'billing', ...props }) => {
  const isUS = country && country.toUpperCase() === US
  const label = isUS ? 'ZIP' : 'Postal Code'
  const autoComplete = autoCompletePrefix ? `${autoCompletePrefix} postal-code` : 'postal-code'
  return (
    <TextField
      {...props}
      label={label}
      placeholder={`Enter ${label}`}
      autoComplete={autoComplete}
      maxLength={isUS ? 5 : null}
    />
  )
}

PostalCode.propTypes = {
  autoCompletePrefix: PropTypes.string,
  country: PropTypes.string,
}

PostalCode.defaultProps = {
  country: US,
}

const createValidation = (country) => (value) => {
  const reUS = /^[0-9]{5}(?:-[0-9]{4})?$/
  const reCA = /^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$/
  const reGB = /^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$/
  switch (country && country.toUpperCase()) {
    case US:
      return reUS.test(value) ? '' : 'Invalid ZIP code'
    case CA:
      return reCA.test(value) ? '' : 'not valid'
    case GB:
      return reGB.test(value) ? '' : 'not valid'
    default:
      return ''
  }
}

export const PostalCodeField = React.forwardRef(({ country, countryField, ...props }, ref) => {
  const { [countryField]: countryValue } = useFormValues()
  const validation = useMemo(() => createValidation(country ?? countryValue), [
    country,
    countryValue,
  ])
  return (
    <PostalCode {...props} ref={ref} country={country ?? countryValue} validation={validation} />
  )
})

PostalCodeField.displayName = 'PostalCodeField'
PostalCodeField.propTypes = {
  autoCompletePrefix: PropTypes.string,
  country: PropTypes.string,
  countryField: PropTypes.string,
}
