/*eslint-env jest*/
import React from 'react'
import { mount, shallow } from 'enzyme'

import { Form } from 'ui/form/form'
import { HiddenInput } from 'ui/form'

import { HiddenField } from '.'

describe('<HiddenField />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(shallow(<HiddenField name="test" />)).toMatchSnapshot()
    })
  })

  describe('methods', () => {
    it('instance has "value" getter & setter', () => {
      const submit = jest.fn()
      const ref = React.createRef()
      const props = {
        name: 'test',
        defaultValue: 'test default value',
      }
      const wrapper = mount(
        <Form onSubmit={submit}>
          <HiddenField {...props} ref={ref} />
        </Form>,
      )
      expect(ref.current.value).toEqual('test default value')
      ref.current.value = 'changed value'
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: 'changed value' })
      expect(wrapper.find(HiddenInput).instance().getValue()).toEqual('changed value')
    })
  })
  describe('on prop update', () => {
    it('defaultValue: value=defaultValue', () => {
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      const Tst = ({ defaultValue }) => (
        <Form onSubmit={jest.fn()}>
          <HiddenField name="test" defaultValue={defaultValue} />
        </Form>
      )
      const wrapper = mount(<Tst defaultValue="default value" />)
      wrapper.setProps({ defaultValue: 'updated default value' })
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: 'updated default value' })
    })
  })
})

describe('Inherits props from <Form />', () => {
  it('disabled', () => {
    const wrapper = mount(
      <Form onSubmit={jest.fn()} disabled>
        <HiddenField name="test" defaultValue="init" />
      </Form>,
    )
    expect(wrapper.find('#test').is('[disabled]')).toBe(true)
  })

  it('defaultValues', () => {
    const ref = React.createRef()
    mount(
      <Form onSubmit={jest.fn()} defaultValues={{ test: 'init' }}>
        <HiddenField name="test" ref={ref} />
      </Form>,
    )
    expect(ref.current.value).toEqual('init')
  })
})
