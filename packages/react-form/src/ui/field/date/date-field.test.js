/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { Form } from 'ui/form/form'

import { DateField, Field } from '.'

describe('<DateField />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(shallow(<Field name="test" label="Test" />)).toMatchSnapshot()
    })

    it('<input/>', () => {
      const wrapper = render(<Field name="test" label="Test" />)
      const input = wrapper.find('input')
      expect(input.is('[type=date]')).toBe(true)
      expect(input.is('[name=test]')).toBe(true)
      expect(input.is(':disabled')).toBe(false)
      expect(input.is('[readOnly]')).toBe(false)
    })
    it('is <label />', () => {
      const wrapper = render(<Field name="test" label="Test" />)
      expect(wrapper.is('label')).toEqual(true)
    })
    it('outputs label', () => {
      const wrapper = render(<Field name="test" label="Test" />)
      expect(wrapper.text()).toEqual(expect.stringContaining('Test'))
    })
    it('disabled -> <input/>', () => {
      const wrapper = render(<Field name="test" label="Test" disabled />)
      expect(wrapper.find('input').is(':disabled')).toBe(true)
    })
    it('readOnly -> <input/>', () => {
      const wrapper = render(<Field name="test" label="Test" readOnly />)
      expect(wrapper.find('input').is('[readOnly]')).toBe(true)
    })
    it('value -> <input/>', () => {
      const wrapper = render(<Field name="test" label="Test" defaultValue="2019-08-15" />)
      expect(wrapper.find('input').val()).toEqual('2019-08-15')
    })
  })
  describe('instance', () => {
    it('.value defined', () => {
      let field
      mount(
        <Form onSubmit={jest.fn()}>
          <DateField name="test" label="test" ref={(el) => (field = el)} />
        </Form>,
      )
      expect(field).toHaveProperty('value')
    })
    it('.getValue() defined, equals defaultValue', () => {
      let field
      mount(
        <Form onSubmit={jest.fn()}>
          <DateField
            name="test"
            label="test"
            defaultValue="2018-01-31"
            ref={(el) => (field = el)}
          />
        </Form>,
      )
      expect(field).toHaveProperty('getValue')
      expect(field.getValue()).toEqual('2018-01-31')
    })
    it('.getValue() equals .value equals defaultValue', () => {
      let field
      mount(
        <Form onSubmit={jest.fn()}>
          <DateField
            name="testDefaultValue"
            label="test"
            defaultValue="2018-01-31"
            ref={(el) => (field = el)}
          />
        </Form>,
      )
      expect(field.getValue()).toEqual('2018-01-31')
      expect(field.value).toEqual('2018-01-31')
    })
    it('.setValue() defined', () => {
      let field
      mount(
        <Form onSubmit={jest.fn()}>
          <DateField name="test" label="test" ref={(el) => (field = el)} />
        </Form>,
      )
      expect(field).toHaveProperty('setValue')
    })

    it('instance has "value" getter & setter', () => {
      const submit = jest.fn()
      const props = {
        name: 'test',
        defaultValue: '2019-08-15',
        label: 'Test',
      }
      let field, form
      mount(
        <Form onSubmit={submit} ref={(el) => (form = el)}>
          <DateField {...props} ref={(el) => (field = el)} />
        </Form>,
      )
      expect(field).toHaveProperty('setValue')
      expect(field).toHaveProperty('getValue')
      expect(field).toHaveProperty('value')

      expect(field.getValue()).toEqual('2019-08-15')
      field.value = '2019-08-16'
      expect(form.getValues()).toEqual({ test: '2019-08-16' })
      expect(field.value).toEqual('2019-08-16')
    })
  })

  describe('on prop update', () => {
    it('defaultValue: value=defaultValue', () => {
      let form
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      const Tst = ({ defaultValue }) => (
        <Form onSubmit={jest.fn()} ref={(el) => (form = el)}>
          <DateField name="test" label="Test" defaultValue={defaultValue} />
        </Form>
      )
      const wrapper = mount(<Tst defaultValue="2016-08-15" />)
      wrapper.find('input').simulate('change', { target: { value: '2019-08-15' } })
      wrapper.setProps({ defaultValue: '2019-08-16' })
      expect(form.getValues()).toEqual({ test: '2019-08-16' })
    })
  })
})
