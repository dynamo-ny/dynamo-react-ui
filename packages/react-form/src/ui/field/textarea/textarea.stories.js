import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'

import { Field, TextareaField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: Field,
  title: 'Form/Fields/Textarea',
  decorators: [withKnobs],
}

export const textarea = () => (
  <Demo component={TextareaField} defaultValue={text('defaultValue', 'some\ntext')} />
)
