import React from 'react'
import PropTypes from 'prop-types'

import { ImageInput } from 'ui/form/image'

import { BaseField, withFormDefaultValues } from '../base-field'

export class Field extends BaseField {
  static displayName = 'ImageField'

  static propTypes = {
    disabled: PropTypes.bool,
    getUrlFromUploadResult: PropTypes.func.isRequired,
    getValueFromUploadResult: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    onSrcChange: PropTypes.func.isRequired,
    onUpload: PropTypes.func.isRequired,
    validation: PropTypes.func,
  }
  static defaultProps = {
    getValueFromUploadResult: ({ uuid } = {}) => uuid,
    getUrlFromUploadResult: ({ imageUrl } = {}) => imageUrl,
  }

  clear() {
    this.element.current.clear() // TODO: what is it?
  }

  getWrapperClassName(className) {
    return 'image-input ' + super.getWrapperClassName(className)
  }

  renderField(props) {
    return <ImageInput {...props} />
  }
}

export const ImageField = withFormDefaultValues(Field)
