import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { withKnobs, text, radios } from '@storybook/addon-knobs'

import { Button } from 'ui/button'

import { Field, ImageField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: Field,
  title: 'Form/Fields/Image',
  decorators: [withKnobs],
}

export const image = () => (
  <Demo
    component={ImageStory}
    url={radios('url', { hasSourceURL: 'https://via.placeholder.com/350', hasNotSourceURL: '' })}
    uuid={text('Uuid', 'test')}
  />
)

function ImageStory({ uuid, url, label, name }) {
  const input = useRef(null)
  const defaultValue = uuid || null
  const [imageSrc, setSrc] = useState(url || null)
  const setImageSrc = (src) => {
    setSrc(src)
  }
  useEffect(() => {
    setSrc(url)
  }, [url])

  const handleDeleteProfileImage = () => {
    input.current.clear()
  }

  return (
    <>
      <ImageField
        ref={input}
        defaultValue={defaultValue}
        defaultSrc={url}
        onUpload={() => {
          return new Promise((resolve) => {
            setTimeout(() => {
              resolve({ imageUrl: 'https://via.placeholder.com/350', uuid: 'test' })
            }, 2000)
          })
        }}
        onSrcChange={setImageSrc}
        label={label}
        name={name}
        required
      >
        <div className="buttons-wrapper">
          <span className="button">Replace</span>
          <Button theme="alert" onClick={handleDeleteProfileImage}>
            Delete
          </Button>
        </div>
      </ImageField>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <span>Downloaded image: </span>
        <br />
        {imageSrc ? (
          <img className="image" src={imageSrc} width={150} height={150} alt="test_image" />
        ) : (
          false
        )}
      </div>
    </>
  )
}

ImageStory.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  url: PropTypes.string,
  uuid: PropTypes.string,
}
