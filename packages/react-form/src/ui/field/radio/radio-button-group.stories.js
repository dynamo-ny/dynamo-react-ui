import React from 'react'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'

import { Field, RadioButtonGroup } from '.'
import { Demo } from '../storybook-field-demo'

const options = {
  value1: 'First Option',
  value2: 'Second Option',
  value3: 'Third Option with some quite long text that is expected to wrap into a few lines',
  value4: 'Fourth Option',
}

export default {
  component: Field,
  title: 'Form/Fields/Radio Button Group',
  decorators: [withKnobs],
}

export const radio_button_group = () => (
  <Demo
    component={RadioButtonGroup}
    defaultValue={text('defaultValue', 'value1')}
    horizontal={boolean('horizontal', false)}
    options={options}
  />
)
