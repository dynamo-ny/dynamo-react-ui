import React, { useMemo } from 'react'
import PropTypes from 'prop-types'

import { Select } from 'ui/field/select'
import { TextField } from 'ui/field/text'
import { useFormValues } from 'ui/form/context'

import { State } from 'const/types'
import { cn } from 'utils'

const Region = ({
  states,
  country,
  required,
  personal,
  autoCompletePrefix = 'billing',
  label,
  className,
  ...props
}) => {
  const isUS = country?.toUpperCase() === 'US'
  const options = useMemo(() => Object.fromEntries(states.map(({ name, code }) => [code, name])), [
    states,
  ])

  const fullLabel = [label || '', isUS ? 'State' : 'State / Province'].join(' ')

  const ownProps = {
    className: cn('region', className),
    label: fullLabel,
    placeholder: `Enter ${fullLabel}`,
    autoComplete: autoCompletePrefix ? `${autoCompletePrefix} address-level1` : 'address-level1',
  }

  return isUS ? (
    <Select {...props} {...ownProps} required={required} options={options} />
  ) : (
    <TextField {...props} {...ownProps} required={required} />
  )
}

Region.propTypes = {
  autoCompletePrefix: PropTypes.string,
  country: PropTypes.string,
  disabled: PropTypes.bool,
  name: PropTypes.string.isRequired,
  personal: PropTypes.bool,
  required: PropTypes.bool,
  states: PropTypes.arrayOf(State).isRequired,
}

export const RegionField = React.forwardRef(({ country, countryField, ...props }, ref) => {
  const { [countryField]: countryValue } = useFormValues()
  return <Region {...props} ref={ref} country={country ?? countryValue} />
})

RegionField.displayName = 'RegionField'
RegionField.propTypes = {
  ...Region.propTypes,
  countryField: PropTypes.string,
}
