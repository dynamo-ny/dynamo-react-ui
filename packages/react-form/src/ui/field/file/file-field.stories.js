import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'

import { Field, FileField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: Field,
  title: 'Form/Fields/File',
  decorators: [withKnobs],
}

const uuid = 'b15ca2f9-1d1d-4a95-8c3b-724e19bb34b6_'

export const file = () => (
  <Demo
    component={FileField}
    upload={(x) => ({
      file: uuid + x.name,
    })}
  />
)
