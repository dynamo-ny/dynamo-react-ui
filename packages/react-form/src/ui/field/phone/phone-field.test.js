/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { Form } from 'ui/form/form'
import { BasePhoneInput } from 'ui/form/phone-input'

import { PhoneField } from '.'

describe('<PhoneField />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(shallow(<PhoneField name="test" label="Test" />)).toMatchSnapshot()
    })
  })

  it('<input/>', () => {
    const wrapper = render(<PhoneField name="test" label="Test" />)
    const input = wrapper.find('input')
    expect(input.is('[name=test]')).toBe(true)
    expect(input.is(':disabled')).toBe(false)
    expect(input.is('[readOnly]')).toBe(false)
  })
  it('outputs label', () => {
    const wrapper = render(<PhoneField name="test" label="Test" />)
    expect(wrapper.text()).toEqual(expect.stringContaining('Test'))
  })
  it('disabled -> <input/>', () => {
    const wrapper = render(<PhoneField name="test" label="Test" disabled />)
    expect(wrapper.find('input').is(':disabled')).toBe(true)
  })
  it('readOnly -> <input/>', () => {
    //unable to pass readonly to select
    const wrapper = render(<PhoneField name="test" label="Test" readOnly />)
    expect(wrapper.find('input').is('[readOnly]')).toBe(true)
  })
  it('value -> <input/>', () => {
    const wrapper = render(<PhoneField name="test" label="Test" defaultValue="380938382211" />)
    expect(wrapper.find('input').val().replace(/\D/g, '')).toEqual('380938382211')
  })
  describe('methods', () => {
    it('instance has "value" getter & setter', () => {
      //Has only getter
      const submit = jest.fn()
      const ref = React.createRef()
      const props = {
        name: 'test',
        defaultValue: '380931234568',
        label: 'Test',
      }
      const wrapper = mount(
        <Form onSubmit={submit}>
          <PhoneField {...props} ref={ref} />
        </Form>,
      )
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: '380931234568' })
      // expect(
      //   wrapper
      //     .find(BasePhoneInput)
      //     .instance()
      //     .getValue(),
      // ).toEqual('380931234567')
    })
  })
  describe('validation', () => {
    it('has willValidate', () => {
      const props = {
        name: 'testMaxLength',
        defaultValue: '123456',
        label: 'Test',
        maxLength: 1,
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <PhoneField {...props} />
        </Form>,
      )
      expect(wrapper.find(BasePhoneInput).instance().willValidate()).toEqual(true)
    })
    it('respects custom validation', () => {
      const props = {
        name: 'test',
        defaultValue: '321',
        label: 'Test',
        validation: (value) => (value && !value.includes('321') ? 'Must contain "321"' : null),
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <PhoneField {...props} />
        </Form>,
      )
      expect(wrapper.instance().getFieldsValidity()).toEqual({ test: true })
    })
    it('respects custom validation wrong value', () => {
      const props = {
        name: 'test',
        defaultValue: '123',
        label: 'Test',
        validation: (value) => (value && !value.includes('321') ? 'Must contain "321"' : null),
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <PhoneField {...props} />
        </Form>,
      )
      expect(wrapper.instance().getFieldsValidity()).toEqual({ test: false })
    })

    it('respects custom validation after changing value', () => {
      //It's works but have no acces to change value by ref
      const ref = React.createRef()
      const props = {
        name: 'test',
        defaultValue: '321',
        label: 'Test',
        validation: (value) => (value && !value.includes('321') ? 'Must contain "321"' : null),
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <PhoneField {...props} ref={ref} />
        </Form>,
      )
      wrapper.find('input').simulate('change', { target: { value: '123' } })
      expect(wrapper.find(Form).instance().getFieldsValidity()).toEqual({ test: false })
    })
  })
  describe('on prop update', () => {
    it('defaultValue: value=defaultValue', () => {
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      const Tst = ({ defaultValue }) => (
        <Form onSubmit={jest.fn()}>
          <PhoneField name="test" label="Test" defaultValue={defaultValue} />
        </Form>
      )
      const wrapper = mount(<Tst defaultValue="1234" />)
      wrapper.find('input').simulate('change', { target: { value: '4321' } })
      wrapper.setProps({ defaultValue: '10101' })
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: '10101' })
    })
  })

  describe('Inherits props from <Form />', () => {
    it('disabled', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()} disabled>
          <PhoneField name="test" label="Test" defaultValue="init" />
        </Form>,
      )
      expect(wrapper.find('input[name="test"]').is('[disabled]')).toBe(true)
    })
    it('readOnly', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()} readOnly>
          <PhoneField name="test" label="Test" defaultValue="init" />
        </Form>,
      )
      expect(wrapper.find('input').is('[readOnly]')).toBe(true)
    })

    it('defaultValues', () => {
      const ref = React.createRef()
      mount(
        <Form onSubmit={jest.fn()} defaultValues={{ test: '+1(111) 111-11-11' }}>
          <PhoneField name="test" label="Test" ref={ref} />
        </Form>,
      )
      expect(ref.current.value).toEqual('11111111111')
    })
  })
})
