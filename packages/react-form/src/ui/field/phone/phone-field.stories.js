import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'

import { Field, PhoneField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: Field,
  title: 'Form/Fields/Phone',
  decorators: [withKnobs],
}

export const phone = () => (
  <Demo component={PhoneField} defaultValue={text('defaultValue', '+12123851552')} />
)
