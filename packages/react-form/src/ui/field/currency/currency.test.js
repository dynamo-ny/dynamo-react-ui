/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { Form } from 'ui/form/form'

import { CurrencyField, Field } from '.'

describe('<CurrencyField />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(
        shallow(<Field name="currency" defaultValue={15} label="Currency label" />),
      ).toMatchSnapshot()
    })

    it('wrapped in label', () => {
      const wrapper = render(<Field name="test" label="Test" />)
      expect(wrapper.is('label')).toEqual(true)
    })

    it('input control respects name, is not disabled nor read-only', () => {
      const wrapper = render(
        <Field
          name="currency"
          defaultValue={15}
          label="Currency label value must be not equal to 22"
        />,
      )
      const input = wrapper.find('input')
      expect(input.is('[name=currency]')).toBe(true)
      expect(input.is(':disabled')).toBe(false)
      expect(input.is('[readOnly]')).toBe(false)
    })
  })

  describe('props', () => {
    it('label: outputs label text', () => {
      const wrapper = render(<Field name="test" label="Test" />)
      expect(wrapper.text()).toEqual(expect.stringContaining('Test'))
    })
    it('disabled: passed to input', () => {
      const wrapper = render(<Field name="test" label="Test" disabled />)
      expect(wrapper.find('input').is(':disabled')).toBe(true)
    })
    it('readOnly: passed to input', () => {
      const wrapper = render(<Field name="test" label="Test" readOnly />)
      expect(wrapper.find('input').is('[readOnly]')).toBe(true)
    })
  })

  describe('methods', () => {
    it('instance has "value" getter & setter', () => {
      const ref = React.createRef()
      mount(
        <Form onSubmit={jest.fn()}>
          <CurrencyField name="test" defaultValue={1} ref={ref} />
        </Form>,
      )
      expect(ref.current.value).toEqual(1)
      ref.current.value = 2
      expect(ref.current.value).toEqual(2)
    })
    it('value getter returns defaultValue', () => {
      const ref = React.createRef()
      mount(
        <Form onSubmit={jest.fn()}>
          <CurrencyField name="test" defaultValue={1} ref={ref} />
        </Form>,
      )
      expect(ref.current.value).toEqual(1)
    })
  })

  describe('parent <Form />', () => {
    it('receives value updates on user input', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <CurrencyField name="test" label="Test" defaultValue={15} />
        </Form>,
      )
      wrapper.find('input').simulate('change', { target: { value: '10' } })
      expect(wrapper.instance().getValues()).toEqual({ test: 10 })
    })
    it('receives validity updates on user input', () => {
      const ref = React.createRef()
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <CurrencyField ref={ref} name="test" label="Test" defaultValue={15} />
        </Form>,
      )
      wrapper.find('input').simulate('change', { target: { value: '10' } })
      expect(wrapper.instance().getValues()).toEqual({ test: 10 })
    })
    it('on reset, value=defaultValue', () => {
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <CurrencyField name="test" label="Test" defaultValue={10} />
        </Form>,
      )
      const form = wrapper.instance()
      wrapper.find('input').simulate('change', { target: { value: '15', rawValue: 15 } })
      expect(form.getValues()).toEqual({ test: 15 })
      form.reset()
      expect(form.getValues()).toEqual({ test: 10 })
    })
  })

  describe('on prop update', () => {
    it('defaultValue: value=defaultValue', () => {
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      const Tst = ({ defaultValue }) => (
        <Form onSubmit={jest.fn()}>
          <CurrencyField name="test" label="Test" defaultValue={defaultValue} />
        </Form>
      )
      const wrapper = mount(<Tst defaultValue={10} />)
      wrapper.find('input').simulate('change', { target: { value: '15' } })
      wrapper.setProps({ defaultValue: 13 })
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: 13 })
    })
  })
})
