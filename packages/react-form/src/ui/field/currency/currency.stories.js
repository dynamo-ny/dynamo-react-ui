import React from 'react'
import { action } from '@storybook/addon-actions'
import { number } from '@storybook/addon-knobs'

import { Form } from 'ui/form'

import { Field, CurrencyField } from '.'
import { Demo } from '../storybook-field-demo'

const formActions = {
  onSubmit: action('form.onSubmit'),
  onChange: action('form.onChange'),
}

const validation = (value) => (value === 22 ? 'Must be not equal to 22' : null)

export default {
  component: Field,
  title: 'Form/Fields/Currency',
}

export const currency = () => (
  <Demo
    component={CurrencyField}
    min={number('Min', undefined)}
    max={number('Max', undefined)}
    multipliesOf={number('Multiplies Of', undefined)}
    defaultValue={number('Default Value', 15000)}
  />
)

export const min = () => (
  <Form {...formActions}>
    <CurrencyField
      name="currency2"
      defaultValue={15}
      min={10}
      label="Currency label min value 10"
    />
  </Form>
)

export const max = () => (
  <Form {...formActions}>
    <CurrencyField
      name="currency3"
      defaultValue={15}
      max={20}
      label="Currency label max value 20"
    />
  </Form>
)

export const custom_validation = () => (
  <Form {...formActions}>
    <CurrencyField
      name="currency4"
      defaultValue={15}
      validation={validation}
      label="Currency label value must be not equal to 22"
    />
  </Form>
)

export const disabled = () => (
  <Form {...formActions}>
    <CurrencyField
      name="currencyDisabled"
      defaultValue={15}
      label="Currency label disabled"
      disabled
    />
  </Form>
)

export const read_only = () => (
  <Form {...formActions}>
    <CurrencyField
      name="currencyReadOnly"
      defaultValue={15}
      label="Currency label readOnly"
      readOnly
    />
  </Form>
)
