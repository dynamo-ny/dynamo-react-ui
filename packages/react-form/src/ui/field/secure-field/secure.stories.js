import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'

import { SecureField } from '.'
import { Demo } from '../storybook-field-demo'
import { SecureDOBField } from '../secure-date-of-birth'

export default {
  component: SecureField,
  title: 'Form/Fields/Secure',
  decorators: [withKnobs],
}

export const secured_data_of_birth = () => (
  <Demo component={SecureDOBField} defaultValue={text('defaultValue', '**/**/2020')} />
)
