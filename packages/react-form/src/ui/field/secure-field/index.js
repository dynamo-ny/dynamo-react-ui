import React from 'react'
import PropTypes from 'prop-types'

import { Button } from 'ui/button'

import { BaseField } from '../base-field'
import { Field as TextField } from '../text'
import { Label } from '../label'

export class SecureField extends BaseField {
  static propTypes = {
    component: PropTypes.elementType,
    defaultValue: PropTypes.any,
    disabled: PropTypes.bool,
    excludedProps: PropTypes.array,
    label: PropTypes.node,
    name: PropTypes.string.isRequired,
    readOnly: PropTypes.bool,
    required: PropTypes.bool,
    touched: PropTypes.bool,
    validation: PropTypes.func,
  }

  state = {
    isOpen: false,
  }

  toggleOpen = () => {
    this.setState((state) => ({ isOpen: !state.isOpen }))
  }

  /** @private */
  get className() {
    return this.getWrapperClassName(this.props.className)
  }

  wrapLabel(label) {
    const isEditable = !this.props.disabled && !this.props.readOnly
    return !isEditable ? (
      label
    ) : !this.props.defaultValue ? (
      label
    ) : this.state.isOpen ? (
      <>
        {label}&nbsp;
        <Button className="inline" onClick={this.toggleOpen}>
          Discard
        </Button>
      </>
    ) : (
      <>
        {label}&nbsp;
        <Button className="inline" onClick={this.toggleOpen}>
          Edit
        </Button>
      </>
    )
  }

  componentDidUpdate(prevProps) {
    super.componentDidUpdate && super.componentDidUpdate()
    //touched changed true => false when form was reset
    if (!this.props.touched && prevProps?.touched) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ isOpen: false })
    }
  }

  renderSecuredField({ defaultValue, ...props }) {
    const { isOpen } = this.state
    if (defaultValue && !isOpen) {
      return (
        <TextField
          {...props}
          defaultValue={defaultValue.replace(/\*/g, '∗')}
          disabled
          type="text"
        />
      )
    }

    return this.renderField({ ...props })
  }

  /** @protected */
  renderField() {
    throw 'override renderField(props)'
  }

  render() {
    return (
      <Label label={this.wrapLabel(this.label)} name={this.props.name} className={this.className}>
        {this.renderSecuredField(this.getInputProps())}
        {this.props.children}
        {this.shouldShowValidity() && this.renderValidationMessage()}
      </Label>
    )
  }
}
