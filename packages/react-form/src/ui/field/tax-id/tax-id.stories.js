import React from 'react'
import { withKnobs, radios } from '@storybook/addon-knobs'

import { Field, SecuredTaxIdField } from '.'
import { Demo } from '../storybook-field-demo'

export default {
  component: Field,
  title: 'Form/Fields/Tax ID',
  decorators: [withKnobs],
}

export const tax_id = () => (
  <Demo
    component={SecuredTaxIdField}
    country={radios('Country', { 'United States': 'US', Ukraine: 'UA' }, 'US')}
    ownerType={radios('Investor Type?', { Person: 'person', Company: 'company' }, 'person')}
  />
)
