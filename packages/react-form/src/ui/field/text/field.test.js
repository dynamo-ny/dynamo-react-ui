/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { Form } from 'ui/form/form'
import { TextInput } from 'ui/form/text-input'

import { TextField as Field } from '.'

describe('<Field />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(shallow(<Field name="test" label="Test" />)).toMatchSnapshot()
    })
  })

  it('<input/>', () => {
    const wrapper = render(<Field name="test" label="Test" />)
    const input = wrapper.find('input')
    expect(input.is('[name=test]')).toBe(true)
    expect(input.is(':disabled')).toBe(false)
    expect(input.is('[readOnly]')).toBe(false)
  })
  it('is <label />', () => {
    const wrapper = render(<Field name="test" label="Test" />)
    expect(wrapper.is('label')).toEqual(true)
  })
  it('outputs label', () => {
    const wrapper = render(<Field name="test" label="Test" />)
    expect(wrapper.text()).toEqual(expect.stringContaining('Test'))
  })
  it('disabled -> <input/>', () => {
    const wrapper = render(<Field name="test" label="Test" disabled />)
    expect(wrapper.find('input').is(':disabled')).toBe(true)
  })
  it('readOnly -> <input/>', () => {
    const wrapper = render(<Field name="test" label="Test" readOnly />)
    expect(wrapper.find('input').is('[readOnly]')).toBe(true)
  })
  it('value -> <input/>', () => {
    const wrapper = render(<Field name="test" label="Test" defaultValue="testing" />)
    expect(wrapper.find('input').val()).toEqual('testing')
  })
  describe('methods', () => {
    it('instance has "value" getter & setter', () => {
      const submit = jest.fn()
      const ref = React.createRef()
      const props = {
        name: 'test',
        defaultValue: 'test default value',
        label: 'Test',
      }
      const wrapper = mount(
        <Form onSubmit={submit}>
          <Field {...props} ref={ref} />
        </Form>,
      )
      expect(ref.current.value).toEqual('test default value')
      ref.current.value = 'changed value'
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: 'changed value' })
      expect(wrapper.find(TextInput).instance().getValue()).toEqual('changed value')
    })
  })
  describe('validation', () => {
    it('has willValidate', () => {
      const props = {
        name: 'testMaxLength',
        defaultValue: 'test default value',
        label: 'Test',
        maxLength: 1,
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Field {...props} />
        </Form>,
      )
      expect(wrapper.find(TextInput).instance().willValidate()).toEqual(true)
    })
    it('respects required', () => {
      const props = {
        name: 'testMaxLength',
        label: 'Test',
        required: true,
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Field {...props} />
        </Form>,
      )
      expect(wrapper.instance().getValues()).toEqual({ testMaxLength: null })
      expect(wrapper.instance().getFieldsValidity()).toEqual({ testMaxLength: false })
    })
    it('respects pattern', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Field name="testPatternInvalid" label="Test" pattern="\d+" defaultValue="abc" />
          <Field name="testPatternValid" label="Test" pattern="\d+" defaultValue="123" />
        </Form>,
      )
      expect(wrapper.instance().getValues()).toEqual({
        testPatternInvalid: 'abc',
        testPatternValid: '123',
      })
      expect(wrapper.instance().getFieldsValidity()).toEqual({
        testPatternInvalid: false,
        testPatternValid: true,
      })
    })
    it('respects minLength', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Field name="testMinLengthInvalid" label="Test" minLength={10} defaultValue="12345" />
          <Field name="testMinLengthValid" label="Test" minLength={5} defaultValue="12345" />
        </Form>,
      )
      expect(wrapper.instance().getValues()).toEqual({
        testMinLengthInvalid: '12345',
        testMinLengthValid: '12345',
      })
      expect(wrapper.instance().getFieldsValidity()).toEqual({
        testMinLengthInvalid: false,
        testMinLengthValid: true,
      })
    })

    it('respects custom validation', () => {
      const props = {
        name: 'test',
        defaultValue: 'test default value',
        label: 'Test',
        validation: (value) => (value && !value.includes('test') ? 'Must contain "test"' : null),
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Field {...props} />
        </Form>,
      )
      expect(wrapper.instance().getValues()).toEqual({ test: 'test default value' })
      expect(wrapper.instance().getFieldsValidity()).toEqual({ test: true })
    })

    it('respects validation after changing value', () => {
      const ref = React.createRef()
      const props = {
        name: 'test',
        defaultValue: 'test default value',
        label: 'Test',
        minLength: 10,
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Field {...props} ref={ref} />
        </Form>,
      )
      ref.current.value = 'changed'
      expect(wrapper.find(Form).instance().getFieldsValidity()).toEqual({ test: false })
    })

    it('respects custom validation after changing value', () => {
      const ref = React.createRef()
      const props = {
        name: 'test',
        defaultValue: 'test default value',
        label: 'Test',
        validation: (value) => (value && !value.includes('test') ? 'Must contain "test"' : null),
      }
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <Field {...props} ref={ref} />
        </Form>,
      )
      ref.current.value = 'changed value'
      expect(wrapper.find(Form).instance().getFieldsValidity()).toEqual({ test: false })
    })
  })
  describe('on prop update', () => {
    it('defaultValue: value=defaultValue', () => {
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      const Tst = ({ defaultValue }) => (
        <Form onSubmit={jest.fn()}>
          <Field name="test" label="Test" defaultValue={defaultValue} />
        </Form>
      )
      const wrapper = mount(<Tst defaultValue="default value" />)
      wrapper.find('input').simulate('change', { target: { value: 'testing' } })
      wrapper.setProps({ defaultValue: 'updated default value' })
      expect(wrapper.find(Form).instance().getValues()).toEqual({ test: 'updated default value' })
    })
  })
})

describe('Inherits props from <Form />', () => {
  it('disabled', () => {
    const wrapper = mount(
      <Form onSubmit={jest.fn()} disabled>
        <Field name="test" label="Test" defaultValue="init" />
      </Form>,
    )
    expect(wrapper.find('#test').is('[disabled]')).toBe(true)
  })
  it('readOnly', () => {
    const wrapper = mount(
      <Form onSubmit={jest.fn()} readOnly>
        <Field name="test" label="Test" defaultValue="init" />
      </Form>,
    )
    expect(wrapper.find('input').is('[readOnly]')).toBe(true)
  })

  it('defaultValues', () => {
    const ref = React.createRef()
    mount(
      <Form onSubmit={jest.fn()} defaultValues={{ test: 'init' }}>
        <Field name="test" label="Test" ref={ref} />
      </Form>,
    )
    expect(ref.current.value).toEqual('init')
  })
})
