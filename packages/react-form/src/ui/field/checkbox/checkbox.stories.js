import React from 'react'
import { action } from '@storybook/addon-actions'
import { boolean } from '@storybook/addon-knobs'

import { Form } from 'ui/form'

import { Field, Checkbox } from '.'
import { Demo } from '../storybook-field-demo'

const formActions = {
  onSubmit: action('form.onSubmit'),
  onChange: action('form.onChange'),
}
const actions = {
  onChange: action('onChange'),
}

export default {
  component: Field,
  title: 'Form/Fields/Checkbox',
}

export const checkbox = () => (
  <Demo component={Checkbox} defaultValue={boolean('defaultValue', true)} />
)

export const long_label = () => (
  <Form {...formActions}>
    <Checkbox
      {...actions}
      name="field"
      required
      label="A checkbox with a very long label label that takes a few lines... Just to check if everything fits fine. I hope it's ok"
    />
  </Form>
)

export const disabled = () => (
  <Form {...formActions}>
    <Checkbox {...actions} name="checkBoxLabelDisabled" disabled label="Disabled" />
  </Form>
)

export const checked_and_disabled = () => (
  <Form {...formActions}>
    <Checkbox
      {...actions}
      name="checkBoxLabelDisabled"
      disabled
      defaultValue
      label="Checked &amp; Disabled"
    />
  </Form>
)
