/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { Form } from 'ui/form/form'

import { Field, SecretField } from '.'

describe('<SecretField />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(shallow(<Field name="test" label="Test" />)).toMatchSnapshot()
    })

    it('wrapped in label', () => {
      const wrapper = render(<Field name="test" label="Test" />)
      expect(wrapper.is('label')).toEqual(true)
    })

    it('input control respects name, is not disabled nor read-only', () => {
      const wrapper = render(<Field name="secret" defaultValue="val" label="Secret Field Label" />)
      const input = wrapper.find('input')
      expect(input.is('[name=secret]')).toBe(true)
      expect(input.is(':disabled')).toBe(false)
      expect(input.is('[readOnly]')).toBe(false)
    })
    it('type=password by default', () => {
      const wrapper = render(<Field name="secret2" defaultValue="val" label="Secret Field Label" />)
      const input = wrapper.find('input')
      expect(input.is('[type=password]')).toBe(true)
    })

    it('has a button', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <SecretField name="secret2" defaultValue="val" label="Secret Field Label" />,
        </Form>,
      )
      expect(wrapper.find('button').is('button[type="button"]')).toBe(true)
    })
    it('button toggles visibility', () => {
      const wrapper = mount(
        <Form onSubmit={jest.fn()}>
          <SecretField name="secret2" defaultValue="val" label="Secret Field Label" />,
        </Form>,
      )
      wrapper.find('button').simulate('click')
      expect(wrapper.find('input').is('[type="text"]')).toBe(true)
      wrapper.find('button').simulate('click')
      expect(wrapper.find('input').is('[type="password"]')).toBe(true)
    })
  })

  describe('props', () => {
    it('label: outputs label text', () => {
      const wrapper = render(<Field name="test" label="Test" />)
      expect(wrapper.text()).toEqual(expect.stringContaining('Test'))
    })
    it('disabled: passed to input', () => {
      const wrapper = render(<Field name="test" label="Test" disabled />)
      expect(wrapper.find('input').is(':disabled')).toBe(true)
    })
    it('readOnly: passed to input', () => {
      const wrapper = render(<Field name="test" label="Test" readOnly />)
      expect(wrapper.find('input').is('[readOnly]')).toBe(true)
    })
  })

  describe('methods', () => {
    it('instance has "value" getter & setter', () => {
      const submit = jest.fn()
      const props = {
        defaultValue: 'init',
        name: 'test',
      }
      let instance
      const setInstance = (el) => {
        instance = el
      }
      mount(
        <Form onSubmit={submit}>
          <SecretField {...props} ref={setInstance} />
        </Form>,
      )
      // const instance = wrapper.find(SecretField).instance()
      expect(instance.value).toEqual('init')
      instance.value = 'changed'
      expect(instance.value).toEqual('changed')
    })
    it('value getter returns defaultValue', () => {
      const submit = jest.fn()
      const props = {
        defaultValue: 'init',
        name: 'test',
      }
      let instance
      const setInstance = (el) => {
        instance = el
      }
      mount(
        <Form onSubmit={submit}>
          <SecretField {...props} ref={setInstance} />
        </Form>,
      )
      expect(instance.value).toEqual('init')
    })
  })

  describe('parent <Form />', () => {
    it('receives value updates', () => {
      let field
      let form
      mount(
        <Form onSubmit={jest.fn()} ref={(el) => (form = el)}>
          <SecretField
            name="secret-field-test"
            label="Test"
            defaultValue="init"
            ref={(el) => (field = el)}
          />
        </Form>,
      )
      field.value = 'changed'
      expect(form.getValues()).toEqual({ 'secret-field-test': 'changed' })
    })
    it('receives validity updates', () => {
      let field
      let form
      mount(
        <Form onSubmit={jest.fn()} ref={(el) => (form = el)}>
          <SecretField
            name="test"
            label="Test"
            defaultValue="123"
            pattern="\d+"
            ref={(el) => (field = el)}
          />
        </Form>,
      )
      expect(form.getFieldsValidity()).toEqual({ test: true })
      field.value = 'changed'
      expect(form.getFieldsValidity()).toEqual({ test: false })
    })

    it('on reset, value=defaultValue', () => {
      let field, form
      mount(
        <Form onSubmit={jest.fn()} ref={(el) => (form = el)}>
          <SecretField name="test" label="Test" defaultValue="init" ref={(el) => (field = el)} />
        </Form>,
      )
      expect(form.getValues()).toEqual({ test: 'init' })
      field.value = 'changed'
      expect(form.getValues()).toEqual({ test: 'changed' })
      form.reset()
      expect(form.getValues()).toEqual({ test: 'init' })
    })
  })

  describe('on prop update', () => {
    it('defaultValue: value=defaultValue', () => {
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      let form
      const Tst = ({ defaultValue }) => (
        <Form onSubmit={jest.fn()} ref={(el) => (form = el)}>
          <SecretField name="test" label="Test" defaultValue={defaultValue} />
        </Form>
      )
      const wrapper = mount(<Tst defaultValue="default value" />)
      wrapper.find('input').simulate('change', { target: { value: 'testing' } })
      wrapper.setProps({ defaultValue: 'updated default value' })
      expect(form.getValues()).toEqual({ test: 'updated default value' })
    })
  })
})
