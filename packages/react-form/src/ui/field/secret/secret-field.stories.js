import React from 'react'
import { action } from '@storybook/addon-actions'
import { text } from '@storybook/addon-knobs'

import { Form } from 'ui/form'

import { Field, SecretField } from '.'
import { Demo } from '../storybook-field-demo'

const formActions = {
  onSubmit: action('Form.onSubmit'),
  onChange: action('Form.onChange'),
}
const fieldActions = {
  onChange: action('onChange'),
}

export default {
  component: Field,
  title: 'Form/Fields/Secret',
}

export const secret = () => (
  <Demo component={SecretField} defaultValue={text('defaultValue', 'default')} />
)

export const disabled = () => (
  <Form {...formActions}>
    <SecretField
      {...fieldActions}
      name="aSecretFieldDisabled"
      label="Disabled Secret Field"
      defaultValue="secret"
      disabled
    />
  </Form>
)
