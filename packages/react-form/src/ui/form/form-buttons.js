import React from 'react'
import PropTypes from 'prop-types'

import { Button } from 'ui/button'

import { useFormProps } from './context'

export const SubmitButton = React.forwardRef(({ disabled, disableUnmodified, ...props }, ref) => {
  const { pending, modified } = useFormProps()

  // TODO: Add some global Defaults/Config - to set whether a button should be disabled for invalid form
  return (
    <Button
      {...props}
      type="submit"
      ref={ref}
      disabled={disabled || pending || (disableUnmodified && !modified)}
    />
  )
})
SubmitButton.displayName = 'SubmitButton'

export const OptimisticSubmitButton = React.forwardRef(
  ({ disabled, disableUnmodified, ...props }, ref) => {
    const { pending, modified } = useFormProps()
    return (
      <Button
        {...props}
        type="submit"
        ref={ref}
        disabled={disabled || pending || (disableUnmodified && !modified)}
      />
    )
  },
)

OptimisticSubmitButton.displayName = 'OptimisticSubmitButton'

export const SaveButton = React.forwardRef(({ disabled, disableUnmodified, ...props }, ref) => {
  const { pending, modified } = useFormProps()
  // TODO: Add some global Defaults/Config - to set whether a button should be disabled for invalid form

  return <Button {...props} type="submit" disabled={!modified || disabled || pending} ref={ref} />
})
SaveButton.displayName = 'SaveButton'

export const ResetButton = ({ disabled, disableUnmodified, ...props }) => {
  const { pending, modified } = useFormProps()
  return (
    <Button
      {...props}
      type="reset"
      disabled={(disableUnmodified && !modified) || disabled || pending}
    />
  )
}

SubmitButton.propTypes = {
  disableUnmodified: PropTypes.bool,
  disabled: PropTypes.bool,
}

OptimisticSubmitButton.propTypes = {
  disableUnmodified: PropTypes.bool,
  disabled: PropTypes.bool,
}

SaveButton.propTypes = {
  disableUnmodified: PropTypes.bool,
  disabled: PropTypes.bool,
}

ResetButton.propTypes = {
  disableUnmodified: PropTypes.bool,
  disabled: PropTypes.bool,
}
