import './image.css'
import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

import { BaseHtmlValidationInput } from './base-html-validation-input'

export class ImageInput extends BaseHtmlValidationInput {
  static propTypes = {
    ...BaseHtmlValidationInput.propTypes,
    disabled: PropTypes.bool,
    getUrlFromUploadResult: PropTypes.func.isRequired,
    getValueFromUploadResult: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    onSrcChange: PropTypes.func.isRequired,
    onUpload: PropTypes.func.isRequired,
    validation: PropTypes.func,
  }
  static defaultProps = {
    getValueFromUploadResult: ({ uuid } = {}) => uuid,
    getUrlFromUploadResult: ({ imageUrl } = {}) => imageUrl,
  }

  setElement = (element) => {
    this.element = element
  }

  handleChange = async (event) => {
    const file = event.target.files[0]
    const { onUpload, getValueFromUploadResult, onSrcChange, getUrlFromUploadResult } = this.props

    this.previewFile(file)

    if (file && onUpload) {
      const uploadResult = await onUpload(file)
      this.setValue(getValueFromUploadResult(uploadResult))
      if (onSrcChange) {
        onSrcChange(getUrlFromUploadResult(uploadResult))
      }
    }
  }

  previewFile(file) {
    const { onSrcChange } = this.props
    if (!onSrcChange) return
    if (file) {
      const reader = new FileReader()
      reader.onloadend = () => onSrcChange(reader.result)
      reader.readAsDataURL(file)
    } else {
      onSrcChange(null)
    }
  }

  clear() {
    const { onSrcChange } = this.props
    this.setValue({ value: null })
    if (onSrcChange) {
      onSrcChange(null)
    }
  }

  reset() {
    const { onSrcChange, defaultSrc, defaultValue } = this.props
    this.broadcastUpdates({ value: defaultValue || null, touched: false })
    if (onSrcChange) {
      onSrcChange(defaultSrc || null)
    }
  }

  /**
   * @override
   */
  setValue(value) {
    this.broadcastUpdates({ value: value || null })
  }
  /**
   * @override
   */
  getValue() {
    if (!this.element) return undefined
  }

  render() {
    const {
      defaultSrc,
      validation,
      validationMessages,
      className,
      defaultValue,
      getValueFromUploadResult,
      getUrlFromUploadResult,
      onSrcChange,
      onUpload,
      children,
      ...props
    } = this.props

    return (
      <>
        <input
          {...props}
          className={cn('file-input', className)}
          type="file"
          ref={this.setElement}
          id={props.id || props.name}
          onChange={this.handleChange}
        />
        {children}
      </>
    )
  }
}
