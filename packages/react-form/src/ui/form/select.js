import React from 'react'
import T from 'prop-types'

import { cn } from 'utils'

import { BaseHtmlValidationInput } from './base-html-validation-input'

export class Select extends BaseHtmlValidationInput {
  static propTypes = {
    ...BaseHtmlValidationInput.propTypes,
    autoComplete: T.string,
    disabled: T.bool,
    disabledOptions: T.arrayOf(T.string),
    name: T.string.isRequired,
    onBlur: T.func,

    options: T.object,
  }
  static defaultProps = {
    placeholder: 'Please Select...',
    disabledOptions: [],
  }

  /** @private */
  onBlur = (e) => {
    this.validate()
    this.props.onBlur && this.props.onBlur(e)
  }

  /** @private */
  onChange = /** @type {impoSyntheticEvent} */ (event) => {
    const { value } = event.target
    // update the DOM element validity
    this.resetCustomValidity({ value })

    // let the parent form knows about changes
    this.broadcastUpdates({ value })
  }

  /** @override */
  setValue(value) {
    if (!this.element) return
    this.element.value = value
    this.broadcastUpdates({ value: value || null })
  }

  getValue() {
    return this.element.value || null
  }

  renderOption([value, label]) {
    const disabled = this.props.disabledOptions.includes(value)
    return (
      <option value={value} key={this.props.name + '-option-' + value} disabled={disabled}>
        {label}
      </option>
    )
  }

  render() {
    const {
      disabled,
      validation,
      validationMessage,
      options,
      placeholder,
      readOnly,
      disabledOptions,
      className,
      ...props
    } = this.props
    return (
      <select
        {...props}
        ref={this.setElement}
        id={props.id || props.name}
        onBlur={this.onBlur}
        onChange={this.onChange}
        className={cn(className, readOnly && 'read-only')}
        readOnly={readOnly}
        disabled={readOnly || disabled}
      >
        <option value="">{placeholder}</option>
        {options && Object.entries(options).map(this.renderOption, this)}
      </select>
    )
  }
}
