import './image.css'
import React from 'react'
import PropTypes from 'prop-types'

import { BaseInput } from './base-input'

export class FileInput extends BaseInput {
  static propTypes = {
    disabled: PropTypes.bool,
    name: PropTypes.string.isRequired,
    upload: PropTypes.func.isRequired,
    validation: PropTypes.func,
    valueToFilename: PropTypes.func.isRequired,
  }
  static defaultProps = {
    valueToFilename: (f) => f,
  }

  state = {
    value: this.props.defaultValue,
    filename: this.props.valueToFilename(this.props.defaultValue),
    uploading: false,
    validationMessage: null,
  }

  setElement = (element) => {
    this.element = element
  }

  handleChange = async (event) => {
    const WrongFormatError = 'Wrong file format, you should use on of ( * .pdf,* .docx,* .txt)'
    const file = event.target.files[0]
    if (file.type.includes('image')) {
      this.setState({
        validationMessage: WrongFormatError,
        uploading: false,
        touched: true,
      })
      await this.broadcastUpdates({
        value: null,
        validationMessage: WrongFormatError,
        valid: false,
        touched: true,
      })
      return
    }
    const { upload, valueToFilename } = this.props
    if (file && upload) {
      await this.broadcastUpdates({
        validationMessage: 'Uploading',
        touched: false, //workarond do hide the error message
        valid: false,
      })

      this.setState(
        {
          validationMessage: null,
          filename: file.name,
          uploading: true,
        },
        async () => {
          let uploadResult

          try {
            uploadResult = await upload(file)
            this.setState(
              {
                value: uploadResult,
                validationMessage: null,
                filename: valueToFilename(uploadResult),
                uploading: false,
              },
              () => {
                this.broadcastUpdates({
                  value: uploadResult,
                  validationMessage: null,
                  valid: true,
                  touched: true,
                })
              },
            )
          } catch ({ error }) {
            this.setState({
              validationMessage: error,
              uploading: false,
              touched: true,
            })
            await this.broadcastUpdates({
              value: null,
              validationMessage: error,
              valid: false,
              touched: true,
            })
          }
        },
      )
    }
  }

  handleBrowseFile = () => {
    this.element && this.element.click()
  }

  reset() {
    const { defaultValue, valueToFilename } = this.props
    const value = defaultValue || null
    this.setState({ filename: valueToFilename(value) }, () => {
      this.broadcastUpdates({ value, touched: false })
    })
  }

  /**
   * @override
   */
  setValue(value) {
    this.setState(
      {
        value,
        fileName: this.props.valueToFilename(value),
      },
      () => {
        this.broadcastUpdates({ value: value || null })
      },
    )
  }
  /**
   * @override
   */
  getValue() {
    return this.state.value
  }

  isValid() {
    if (this.props.required && !this.getValue()) return false
    return !this.state.uploading && !this.state.validationMessage
  }

  getValidationMessage() {
    const { uploading, validationMessage } = this.state
    if (uploading) return 'Uploading'
    if (validationMessage) return this.state.validationMessage
    if (this.props.required && !this.getValue()) return 'Required'
    return null
  }
  willValidate() {
    return this.element.willValidate
  }

  componentDidMount() {
    this.broadcastUpdates({ value: this.props.defaultValue })
  }

  render() {
    const { filename, uploading } = this.state
    const {
      validation,
      validationMessage,
      validationMessages,
      className,
      defaultValue,
      valueToFilename,
      upload,
      label,
      required,
      children,
      ...props
    } = this.props
    return (
      <>
        <input
          {...props}
          noValidate
          tabIndex="-1"
          className="input"
          type="file"
          accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf"
          ref={this.setElement}
          id={props.id || props.name}
          onChange={this.handleChange}
          name={props.name}
        />

        <button
          className="browse-button"
          type="button"
          onClick={this.handleBrowseFile}
          disabled={uploading || props.disabled || props.readOnly}
        >
          {filename && <span className="file-name">{filename}</span>}
          {uploading ? 'Uploading...' : 'Browse...'}
        </button>
      </>
    )
  }
}
