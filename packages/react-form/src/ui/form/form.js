/* eslint-disable react/no-unused-state */
import './form.css'
import React from 'react'
import PropTypes from 'prop-types'

import { ValidationMessage } from 'ui/form/validation-message'

import { cn } from 'utils'

import { FormFieldEmitter, FormPropsContext } from './context'

export class Form extends React.PureComponent {
  static propTypes = {
    defaultValues: PropTypes.object,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func.isRequired,
    readOnly: PropTypes.bool,
    redirect: PropTypes.func,
    validation: PropTypes.func,
  }

  static defaultProps = {
    defaultValues: {},
  }

  /**
   * @type {{ current: HTMLFormElement }}
   */
  element = React.createRef()

  /**
   * @type {{ current: HTMLDivElement }}
   */
  errorMessageRef = React.createRef()

  fieldRefs = {}

  /** @private */
  setFieldStatus = ({
    value = null,
    name,
    enable = true,
    defaultValue = null,
    validationMessage,
    touched,
    ref,
  } = {}) => {
    if (!name) throw 'field name is undefined'

    if (ref) {
      this.fieldRefs[name] = ref
    }

    return this.updateFieldState(name, {
      value,
      touched,
      validationMessage,
      defaultValue,
      enable,
    }).then(() => {
      this.props.onChange && this.props.onChange(this.getValues())
    })
  }

  state = {
    fields: {},
    pending: false,
    error: null,
    errors: {},
  }

  /**
   * @param {Event} event
   */
  handleSubmit = (event) => {
    event.preventDefault()
    event.stopPropagation()

    this.validate().then(
      () => this.doSubmit(),
      () => this.focusFirstInvalidField(),
    )
  }

  handleReset = () => {
    Object.values(this.fieldRefs).forEach((element) => {
      element?.reset?.()
    })
    this.resetValues()
  }

  async doSubmit() {
    const { onSubmit: submit, redirect } = this.props
    await this.setStateAsync({ pending: true })
    try {
      const result = await submit(this.getValues())
      await this.resetTouched()
      await this.setStateAsync({ pending: false })
      redirect && redirect(result)
    } catch ({ error, errors }) {
      await this.setStateAsync({ pending: false })
      await this.setErrors(error, errors)
      this.scrollToTop()
    }
  }

  /**
   * @param {String} name
   * @param {{
   *  value:*,
   *  touched:Boolean,
   *  validationMessage:String,
   *  defaultValue:*,
   *  enable:Boolean
   * }} next
   */
  updateFieldState(name, { value, touched, validationMessage, defaultValue, enable }) {
    // console.log(name, { value, touched, validationMessage, defaultValue, enable })

    if (this._unmounted) return Promise.resolve() // must be a Promise

    return new Promise((resolve) => {
      this.setState((state) => {
        const { [name]: prev, ...fields } = state.fields
        if (!enable && !prev) {
          // was disabled, now disabled - nothing has changed
          resolve()
          return null
        }
        if (
          prev &&
          prev.value === value &&
          prev.touched === touched &&
          prev.validationMessage === validationMessage &&
          prev.defaultValue === defaultValue &&
          enable
        ) {
          // when nothing has changed
          resolve()
          return null
        }
        if (!enable) {
          // was enabled, now disabled - remove a field
          return { fields }
        }

        const next = { ...prev, value, touched, validationMessage, defaultValue, enable }
        return {
          fields: {
            ...fields,
            [name]: next,
          },
          error: null,
        }
      }, resolve)
    })
  }

  resetTouched(value = false) {
    if (this._unmounted) return Promise.resolve() // must be a Promise
    return this.setStateAsync((state) => {
      const entries = Object.entries(state.fields).map(([name, fieldState]) => {
        return [name, { ...fieldState, touched: value }]
      })
      return { fields: Object.fromEntries(entries) }
    })
  }

  resetValues() {
    if (this._unmounted) return Promise.resolve() // must be a Promise
    return this.setStateAsync((state) => {
      const entries = Object.entries(state.fields).map(([name, fieldState]) => {
        return [name, { ...fieldState, value: fieldState.defaultValue, touched: false }]
      })
      return { fields: Object.fromEntries(entries) }
    })
  }

  scrollToTop() {
    this.errorMessageRef.current?.scrollIntoView({ behavior: 'smooth' })
  }

  setErrors(error, errors) {
    if (this._unmounted) return Promise.resolve() // must be a Promise
    if (!errors || !Object.keys(errors).length) {
      return this.setStateAsync(() => ({ error, pending: false }))
    }
    return this.setStateAsync((state) => {
      const nextFields = { ...state.fields }
      const unhandledMessages = []
      Object.entries(errors).forEach(([name, validationMessage]) => {
        if (nextFields.hasOwnProperty(name)) {
          nextFields[name] = {
            ...nextFields[name],
            validationMessage,
          }
        } else {
          unhandledMessages.push(`${validationMessage} (${name})`)
        }
      })

      const nextState = { fields: nextFields, pending: false }

      if (unhandledMessages) {
        nextState.error = [error, ...unhandledMessages].filter(Boolean).join(', ')
      } else {
        nextState.error = error
      }

      return nextState
    })
  }

  reset() {
    this.element.current && this.element.current.reset()
    this.handleReset()
  }

  getCustomValidationError(values = this.getValues()) {
    const { validation } = this.props
    return validation ? validation(values) : null
  }

  /**
   * @returns {boolean}
   */
  isValid(values = this.getValues()) {
    if (this.hasInvalidField()) return false
    return !this.getCustomValidationError(values)
  }

  /**
   * @returns {boolean}
   */
  async validate() {
    await this.resetTouched(true)
    if (this.hasInvalidField()) {
      throw new Error('fail')
    }
    const error = await this.getCustomValidationError()
    await this.setStateAsync(() => ({ error }))
    if (error) {
      throw new Error('fail')
    }
  }

  /** @public */
  setValues(values = {}) {
    return Promise.all(
      Object.entries(values).map(([name, value]) => this.setFieldStatus({ value, name })),
    )
  }

  /** @public */
  getValues(fields = this.state.fields) {
    const entries = Object.entries(fields).map(([name, fieldState]) => [name, fieldState.value])
    return Object.fromEntries(entries)
  }
  hasInvalidField(fields = this.state.fields) {
    return Object.values(fields).some((fieldState) => !!fieldState.validationMessage)
  }

  getFirstInvalidFieldName(fields = this.state.fields) {
    for (const [name, state] of Object.entries(fields)) {
      if (state.validationMessage) return name
    }
  }

  hasModifiedField(fields = this.state.fields) {
    return Object.values(fields).some(({ value, defaultValue }) => value !== defaultValue)
  }

  getFieldsValidity(fields = this.state.fields) {
    const entries = Object.entries(fields).map(([name, fieldState]) => [
      name,
      !fieldState.validationMessage,
    ])
    return Object.fromEntries(entries)
  }

  focusFirstInvalidField() {
    const name = this.getFirstInvalidFieldName()
    const ref = this.fieldRefs[name]
    if (!ref) return
    if (ref.focus) {
      return ref.focus()
    }
    if (ref.scrollIntoView) {
      return ref.scrollIntoView({ behavior: 'smooth' })
    }
  }

  setStateAsync(state) {
    return Promise.race([
      new Promise((resolve) => {
        this.setState(state, resolve)
      }),
      // not sure whether the React's setState
      // executes the callback when state has no changes
      new Promise((resolve) => setTimeout(resolve, 1)),
    ])
  }

  componentWillUnmount() {
    this._unmounted = true
  }

  render() {
    const {
      onSubmit,
      children,
      onChange,
      redirect,
      className,
      validation,
      readOnly,
      defaultValues,
      disabled,
      ...props
    } = this.props

    const { error, pending, fields, wasSubmitted } = this.state

    const modified = this.hasModifiedField()
    const values = this.getValues()
    const valid = this.isValid()

    // console.log('modified', modified)
    // console.log('values', values)
    // console.log('valid', valid)

    return (
      <form
        {...props}
        onSubmit={this.handleSubmit}
        ref={this.element}
        onReset={this.handleReset}
        className={cn(className, 'form')}
        noValidate
      >
        <ValidationMessage error ref={this.errorMessageRef}>
          {(!this.hasInvalidField() && error) || null}
        </ValidationMessage>

        <FormFieldEmitter.Provider value={this.setFieldStatus}>
          <FormPropsContext.Provider
            // eslint-disable-next-line react/jsx-no-constructed-context-values
            value={{
              readOnly,
              defaultValues,
              disabled,
              pending,
              modified,
              valid,
              values,
              fields,
              wasSubmitted,
            }}
          >
            {children}
          </FormPropsContext.Provider>
        </FormFieldEmitter.Provider>
      </form>
    )
  }
}
