/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { ValidationMessage } from '.'

describe('<ValidationMessage />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(shallow(<ValidationMessage success>test</ValidationMessage>)).toMatchSnapshot()
    })
  })
  describe('props', () => {
    it('children: outputs text equal test', () => {
      const wrapper = render(<ValidationMessage success>test</ValidationMessage>)
      expect(wrapper.text()).toEqual(expect.stringContaining('test'))
    })
    it('Contains class success', () => {
      const wrapper = mount(<ValidationMessage success>test</ValidationMessage>)
      expect(wrapper.find('.success')).toHaveLength(1)
      expect(wrapper.find('.error')).toHaveLength(0)
      expect(wrapper.find('.validation-message')).toHaveLength(1)
    })
    it('Contains class error', () => {
      const wrapper = mount(<ValidationMessage error>test</ValidationMessage>)
      expect(wrapper.find('.error')).toHaveLength(1)
      expect(wrapper.find('.success')).toHaveLength(0)
      expect(wrapper.find('.validation-message')).toHaveLength(1)
    })
    it("Contains doesn't contain classes error and success but contains validation-message", () => {
      const wrapper = mount(<ValidationMessage>test</ValidationMessage>)
      expect(wrapper.find('.error')).toHaveLength(0)
      expect(wrapper.find('.success')).toHaveLength(0)
      expect(wrapper.find('.validation-message')).toHaveLength(1)
    })
  })
})
