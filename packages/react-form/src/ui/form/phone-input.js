import React from 'react'
import PropTypes from 'prop-types'
import PhoneInput from 'react-phone-input-2'

import { cn } from 'utils'

import { BaseInput } from './base-input'

export class BasePhoneInput extends BaseInput {
  static propTypes = {
    ...BaseInput.propTypes,
    autoComplete: PropTypes.string,
    disabled: PropTypes.bool,
    name: PropTypes.string.isRequired,
    onBlur: PropTypes.func,
    validation: PropTypes.func,
  }
  static defaultProps = {
    type: 'text',
    autoComplete: 'off',
  }

  state = {
    ...super.state,
    value: this.props.defaultValue || null,
    country: 'us',
    dialCode: '',
    reset: false,
  }

  /** @type {HTMLInputElement} */
  element = null
  defaultCountry = 'us'
  dialCode = ''

  setElement = (element) => {
    this.element = element
    const value = this.props.defaultValue || null
    // let the parent form knows about this control
    this.broadcastUpdates({ value })
  }

  onBlur = (e) => {
    this.validate()
    this.props.onBlur && this.props.onBlur(e)
  }

  onChange = (value, data) => {
    this.setState({ value })
    this.dialCode = data.dialCode

    this.setValue(value)
  }

  setValue(value = null) {
    const normalValue = this.getNormalizedValue(value)
    const validationMessage = this.getValidationMessage(normalValue)
    this.broadcastUpdates({ value: normalValue, validationMessage, valid: !validationMessage })
  }

  /**
   * @override
   */
  getValue() {
    return this.getNormalizedValue(this.state.value)
  }

  getNormalizedValue(value) {
    if (value === undefined || value === null) {
      return ''
    }
    if (typeof value === 'string' && String(value).trim() === '') {
      return ''
    }
    return value.replace(/\D/g, '')
  }

  getDefaultValue(defaultValue = this.props.defaultValue) {
    return this.getNormalizedValue(defaultValue)
  }

  getValidationMessage(value = this.getValue()) {
    if (!this.willValidate()) return null

    const { required, validation } = this.props
    if ((!value || (this.dialCode && !value.slice(this.dialCode.length))) && required) {
      return 'The field is required'
    }
    return (validation && validation(value)) || null
  }

  broadcastUpdates({
    value = this.getValue(),
    defaultValue = this.getDefaultValue(),
    ...props
  } = {}) {
    return super.broadcastUpdates({
      ...props,
      value: this.getNormalizedValue(value),
      defaultValue: this.getNormalizedValue(defaultValue),
    })
  }

  validate() {
    this.broadcastUpdates({ touched: true })
  }

  componentDidMount() {
    if (this.props.defaultValue) {
      this.defaultCountry = this.element.state.selectedCountry.iso2
      //is needed for validation
      this.dialCode = this.element.state.selectedCountry.dialCode
    }
  }

  reset() {
    const reset = this.state.reset
    //give know for a library that it needs to reset
    const resetState = () => this.setState({ reset: !reset })
    //create promise from setState
    const updateReset = new Promise((resolve) => setTimeout(resolve, resetState()))
    //update value to initial after reset, is needed for the formatting default value
    updateReset.then(() => this.setState({ value: this.props.defaultValue }))
  }

  focus() {
    this.element?.numberInputRef?.focus?.()
  }

  render() {
    const {
      validation,
      validationMessage,
      defaultValue,
      value,
      required,
      readOnly,
      disabled,
      enableSearch,
      placeholder,
      country,
      className,
      ...props
    } = this.props

    const touched = className?.includes('touched')
    const invalid = className?.includes('invalid')

    return (
      <PhoneInputWithReset
        {...props}
        ref={this.setElement}
        id={props.id || props.name}
        onBlur={this.onBlur}
        onChange={this.onChange}
        value={this.state.value || ''}
        defaultCountry={this.defaultCountry}
        country={this.props.defaultValue ? '' : country || 'us'}
        disableDropdown={disabled || readOnly}
        enableSearch={enableSearch}
        buttonClass={cn(
          disabled && 'disabled',
          readOnly && 'read-only',
          touched && 'touched',
          invalid && 'invalid',
        )}
        inputClass={className}
        inputProps={{
          id: props.id || props.name,
          name: props.id || props.name,
          required,
          readOnly,
          disabled,
        }}
        placeholder={placeholder || ''}
        reset={this.state.reset}
      />
    )
  }
}

export class PhoneInputWithReset extends PhoneInput {
  //added reset for library
  componentDidUpdate(prevProps, prevState) {
    super.componentDidUpdate && super.componentDidUpdate(prevProps, prevState)
    if (prevProps.reset !== this.props.reset) {
      const defaultSelectedCountry = this.state.onlyCountries?.find(
        (o) => o.iso2 === this.props.defaultCountry,
      )
      super.setState({
        country: this.props.defaultCountry,
        selectedCountry: defaultSelectedCountry,
      })
    }
  }
}
