import React from 'react'

import { IsInvalid, Form } from 'ui/form'
import { TextField } from 'ui/field'

import notes from './is-invalid.md'

export default {
  component: IsInvalid,
  title: 'Form|IsInvalid',
  parameters: { notes },
}

export const is_invalid = () => (
  <Form>
    <TextField label="Required Field" name="field" required />
    <p>Try to fill the input</p>
    <IsInvalid>
      <h3>content is visible</h3>
    </IsInvalid>
  </Form>
)
