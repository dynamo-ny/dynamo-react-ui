import React from 'react'
import { withKnobs, boolean } from '@storybook/addon-knobs'

import { IsFieldModified, Form } from 'ui/form'
import { TextField } from 'ui/field'

import { ResetButton } from '../form-buttons'

export default {
  component: IsFieldModified,
  title: 'Form|IsFieldModified',
  decorators: [withKnobs],
}

export const is_field_modified = () => (
  <Form>
    <p>Modify the field to see it in action</p>
    <TextField label="Demo Field" name="field" defaultValue="win" />
    <IsFieldModified name="field" not={boolean('not', false)}>
      <h3>content is visible</h3>
    </IsFieldModified>
    <footer>
      <ResetButton>Reset</ResetButton>
    </footer>
  </Form>
)
