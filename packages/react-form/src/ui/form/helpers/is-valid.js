import { useFormProps } from '../context'

export const IsValid = ({ children }) => {
  const { valid } = useFormProps()
  return valid ? children : false
}
