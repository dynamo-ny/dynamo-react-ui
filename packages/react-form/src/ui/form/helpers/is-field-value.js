import PropTypes from 'prop-types'

import { useField } from '../context'

export const IsFieldValue = ({ name, blank, equals, not, oneOf, children }) => {
  const field = useField(name)
  let result = false
  if (blank) {
    result = !!field.value
  }
  if (equals !== undefined) {
    result = result || field.value === equals
  }
  if (Array.isArray(oneOf)) {
    result = result || oneOf.includes(field.value)
  }
  if (not) {
    result = !result
  }
  return result ? children : false
}

IsFieldValue.propTypes = {
  blank: PropTypes.bool,
  equals: PropTypes.any,
  // field name
  name: PropTypes.string.isRequired,
  not: PropTypes.bool,
  oneOf: PropTypes.arrayOf(PropTypes.any),
}
IsFieldValue.defaultProps = {
  blank: false,
  not: false,
}
