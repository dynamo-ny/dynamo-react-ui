import React from 'react'

import { IsValid, Form } from 'ui/form'
import { TextField } from 'ui/field'

import notes from './is-valid.md'

export default {
  component: IsValid,
  title: 'Form|IsValid',
  parameters: { notes },
}

export const is_valid = () => (
  <Form>
    <TextField label="Required Field" name="field" required defaultValue="x" />
    <p>Try to clear the input</p>
    <IsValid>
      <h3>content is visible</h3>
    </IsValid>
  </Form>
)
