import { Children, cloneElement } from 'react'
import PropTypes from 'prop-types'

import { useField } from '../context'

const createMatchValidatior = (matchValue, name) => (value) =>
  value === matchValue ? null : `Must match ${name}`

export const AddMatchFieldValidation = ({ name, label, children }) => {
  const { value } = useField(name)

  return cloneElement(Children.only(children), {
    validation: createMatchValidatior(value, label),
  })
}

AddMatchFieldValidation.propTypes = {
  children: PropTypes.node.isRequired,
  label: PropTypes.node.isRequired,
  name: PropTypes.string.isRequired,
}
