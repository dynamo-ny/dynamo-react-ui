import { useFormProps } from '../context'

export const IsInvalid = ({ children }) => {
  const { valid } = useFormProps()
  return valid ? false : children
}
