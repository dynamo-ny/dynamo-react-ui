import React from 'react'
import { mount } from 'enzyme'

import { Checkbox, TextField, Select, CurrencyField, SecretField, HiddenField } from 'ui/field'
import { NumberField } from 'ui/field/number'

import { Form } from './form'

describe('Form', () => {
  it('must render without exceptions', () => {
    expect(() => {
      mount(<Form onSubmit={jest.fn()} />)
    }).not.toThrow()
  })

  it('aware of name & defaultValue', () => {
    const instance = mount(
      <Form onSubmit={jest.fn()}>
        <TextField name="aTextField" defaultValue="textDefaultValue" label="test" />
        <NumberField name="aNumberField" defaultValue={15} label="test" />
        <CurrencyField name="currency2" defaultValue={15} label="test" />
        <Select name="select2" defaultValue="a" options={{ a: 1, b: 2, c: 3 }} label="test" />
        <Checkbox name="checkbox2" defaultValue label="test" />
        <SecretField name="secret2" defaultValue="secret-value" label="test" />
        <HiddenField name="hidden2" defaultValue="hidden-value" />
      </Form>,
    ).instance()
    expect(instance.getValues()).toEqual({
      aTextField: 'textDefaultValue',
      aNumberField: 15,
      currency2: 15,
      select2: 'a',
      checkbox2: true,
      secret2: 'secret-value',
      hidden2: 'hidden-value',
    })
    expect(instance.isValid()).toBe(true)
  })

  it('ignores disabled fields', () => {
    const instance = mount(
      <Form onSubmit={jest.fn()}>
        <TextField disabled name="aTextField" defaultValue="textDefaultValue" label="test" />
        <NumberField disabled name="aNumberField" defaultValue={15} label="test" />
        <CurrencyField disabled name="currency3" defaultValue={15} label="test" />
        <Select
          name="select3"
          disabled
          defaultValue="a"
          options={{ a: 1, b: 2, c: 3 }}
          label="test"
        />
        <Checkbox name="checkbox3" disabled label="test" />
        <SecretField name="secret3" disabled defaultValue="secret-value" label="test" />
        <HiddenField name="hidden3" disabled defaultValue="hidden-value" />
      </Form>,
    ).instance()
    const values = instance.getValues()
    expect(values).not.toHaveProperty('select3')
    expect(values).not.toHaveProperty('aNumberField')
    expect(values).not.toHaveProperty('currency3')
    expect(values).not.toHaveProperty('checkbox3')
    expect(values).not.toHaveProperty('secret3')
    expect(values).not.toHaveProperty('hidden3')
    expect(values).toEqual({})
    expect(instance.isValid()).toBe(true)
  })

  it('aware of invalid fields', () => {
    const instance = mount(
      <Form onSubmit={jest.fn()}>
        <TextField name="aTextField" required label="test" />
        <NumberField name="aNumberField" defaultValue={15} max="10" label="test" />
        <CurrencyField name="currency4" defaultValue={15} label="test" min={20} />
        <Select name="select4" required options={{ a: 1, b: 2, c: 3 }} label="test2" />
        <Checkbox name="checkbox4" label="test" required />
        <SecretField name="secret4" label="test" required />
        <HiddenField name="hidden4" />
      </Form>,
    ).instance()
    expect(instance.getValues()).toEqual({
      aTextField: null,
      currency4: 15,
      aNumberField: 15,
      select4: null,
      checkbox4: false,
      secret4: null,
      hidden4: null,
    })
    expect(instance.isValid()).toBe(false)

    expect(instance.getFieldsValidity()).toEqual({
      select4: false,
      currency4: false,
      aNumberField: false,
      aTextField: false,
      checkbox4: false,
      secret4: false,
      hidden4: true,
    })
  })

  it('hasModifiedField() returns false on initialization', () => {
    const handleSubmit = jest.fn()
    const options = { a: 1, b: 2, c: 3 }
    const instance = mount(
      <Form onSubmit={handleSubmit}>
        <TextField name="text6" label="test" />
        <NumberField name="number6" label="test" />
        <CurrencyField name="currency6" label="test" />
        <Select name="select6" options={options} label="test2" />
        <Checkbox name="checkbox6" label="test" />
        <SecretField name="secret6" label="test" />
        <HiddenField name="hidden6" />
      </Form>,
    ).instance()
    expect(instance.hasModifiedField()).toBe(false)
  })

  it('Custom validation', () => {
    const handleSubmit = jest.fn()
    const options = { a: 1, b: 2, c: 3 }
    const fail = () => 'fail'
    const instance = mount(
      <Form onSubmit={handleSubmit}>
        <TextField validation={fail} name="text7" label="test" />
        <NumberField validation={fail} name="number7" label="test" />
        <CurrencyField validation={fail} name="currency7" label="test" defaultValue={10} />
        <Select validation={fail} name="select7" options={options} label="test2" />
        <Checkbox validation={fail} name="checkbox7" label="test" />
        <SecretField validation={fail} name="secret7" defaultValue="secret-value" label="test" />
        <HiddenField validation={fail} name="hidden7" defaultValue="hidden-value" />
      </Form>,
    ).instance()

    expect(instance.getFieldsValidity()).toEqual({
      text7: false,
      number7: false,
      currency7: false,
      select7: false,
      checkbox7: false,
      secret7: false,
      hidden7: true,
    })
  })

  // it.skip('Native form reset calls handleReset()', async () => {
  //   const wrapper = mount(<Form onSubmit={() => {}} />)
  //   const form = wrapper.instance()
  //   form.handleReset = jest.fn().mockName('handleReset')
  //   wrapper.find('form').simulate('reset')
  //   expect(form.handleReset).toHaveBeenCalledTimes(1)
  // })
})
