//eslint-env node
const path = require('path')

module.exports = {
  settings: {
    'import/resolver': {
      node: { paths: [path.resolve(__dirname, 'src')] },
      typescript: {
        project: '.',
      },
    },
  },
}
