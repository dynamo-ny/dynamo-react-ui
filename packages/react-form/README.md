# @dynamo-ny/react-form

> react components library

[![NPM](https://img.shields.io/npm/v/@dynamo-ny/react-form.svg)](https://www.npmjs.com/package/@dynamo-ny/form)

## Install

```bash
npm install @dynamo-ny/react-form
```

or

```bash
yarn add @dynamo-ny/react-form
```

## Usage

```tsx
import '@dynamo-ny/react-form/dist/index.css'
import React from 'react'

import {
  Form,
  TextField,
  NumberField,
  DateField,
  Checkbox,
  TextareaField,
  Select,
  RadioButtonGroup,
  SubmitButton,
  ResetButton,
} from '@dynamo-ny/react-form'

const submit = (data) => {
  // do smth, return a promise
}

export const TestForm = () => (
  <Form onSubmit={submit}>
    <TextField name="name" label="Name" placeholder="John Doe" required />
    <NumberField name="price" label="Price" required />
    <DateField name="dob" label="Date of Birth" required />
    <TextareaField name="TextareaField" label="TextareaField" placeholder="placeholder" required />
    <Select name="gender" label="Gender" options={['male', 'female']} />
    <RadioButtonGroup
      name="radios"
      label="Buttons example"
      required
      options={['alpha', 'beta', 'gamma']}
    />
    <Checkbox name="agree" label="Agree?" required />
    <footer>
      <ResetButton>reset</ResetButton>
      <SubmitButton>submit</SubmitButton>
    </footer>
  </Form>
)
```

## License

MIT
