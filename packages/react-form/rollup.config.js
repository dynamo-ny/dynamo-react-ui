import babel from '@rollup/plugin-babel'
import external from 'rollup-plugin-peer-deps-external'
import postcss from 'rollup-plugin-postcss'
import svgr from '@svgr/rollup'
import typescript from 'rollup-plugin-typescript2'
import commonjs from '@rollup/plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve'

import pkg from './package.json'

export default {
  input: pkg.source,
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: true,
    },
    {
      file: pkg.module,
      format: 'esm',
      exports: 'named',
      sourcemap: true,
    },
  ],
  plugins: [
    external(),

    postcss({
      extract: true,
    }),
    resolve(),
    babel({
      babelHelpers: 'bundled',
      exclude: ['node_modules/**', 'src/icons/**'],
    }),
    commonjs({
      requireReturnsDefault: 'namespace',
    }), // must be before babel
    svgr({ babel: true, include: ['src/icons/**'] }),
    typescript({
      useTsconfigDeclarationDir: true,
      rollupCommonJSResolveHack: true,
      clean: true,
    }),
  ],
  external: Object.keys(pkg.peerDependencies || {}),
}
