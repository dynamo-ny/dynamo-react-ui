/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  BrowserRouter,
  useHistory,
  useLocation,
  useParams,
  generatePath,
  Link,
  NavLink,
} from 'react-router-dom'

import { RoutePath } from 'const/types'

export { useParams, useHistory, useLocation, BrowserRouter as Router }

export const useRedirect = () => {
  const history = useHistory()
  return ({ to, query, next }) => {
    history.push({
      ...(next && { state: { redirect: next } }),
      ...convertProps({ to, query }),
    })
  }
}

export const useRedirectNext = () => {
  const location = useLocation()
  const redirect = useRedirect()
  return (destination) => redirect(location?.state?.redirect || destination)
}

export const Redirect = (props) => {
  const redirect = useRedirect()
  useEffect(() => {
    redirect(props)
  }, [])
  return false
}

export const RouterLink = (props) => <Link {...convertProps(props)} />
RouterLink.propTypes = {
  query: PropTypes.object,
  to: RoutePath.isRequired,
}

export const RouterNavLink = (props) => <NavLink {...convertProps(props)} />
RouterNavLink.propTypes = {
  activeClassName: PropTypes.string,
  query: PropTypes.object,
  to: RoutePath.isRequired,
}

const convertProps = ({ to, query, ...props }) => {
  if (!to) return props
  if (!query) return { to, ...props }

  if (typeof to === 'string')
    return {
      ...props,
      to: {
        pathname: generatePath(to, query),
      },
    }
  return {
    ...props,
    to: {
      ...to,
      pathname: generatePath(to.pathname, query),
    },
  }
}
