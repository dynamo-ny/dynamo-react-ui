import React from 'react'
import PropTypes from 'prop-types'

import { Link } from 'ui/link'

import { cn } from 'utils'

import { useBreadcrumbs } from './breadcrumbs-context'

export const Breadcrumbs = ({ className, divider }) => {
  const breadcrumbs = useBreadcrumbs()
  return breadcrumbs?.length > 0 ? (
    <div className={cn('breadcrumbs', className)}>
      {breadcrumbs?.map(({ label, ...props }, index) => {
        return (
          <>
            <Link {...props} />
            {index !== breadcrumbs.length - 1 && divider}
          </>
        )
      })}
    </div>
  ) : null
}

Breadcrumbs.propTypes = {
  divider: PropTypes.node,
}

Breadcrumbs.defaultProps = {
  divider: <span>&nbsp;&nbsp;/&nbsp;&nbsp;</span>,
}
