import React, { useContext } from 'react'
import PropTypes from 'prop-types'

import { RoutePath } from 'const/types'

const Context = React.createContext()

export const useBreadcrumbs = () => {
  return useContext(Context)
}

export const BreadcrumbsContext = ({ label, children, ...props }) => {
  const breadcrumbs = useBreadcrumbs() || []
  return (
    // eslint-disable-next-line react/jsx-no-constructed-context-values
    <Context.Provider value={[...breadcrumbs, { children: label, ...props }]}>
      {children}
    </Context.Provider>
  )
}

BreadcrumbsContext.propTypes = {
  label: PropTypes.node,
  next: RoutePath,
  query: PropTypes.object,
  to: RoutePath.isRequired,
}
