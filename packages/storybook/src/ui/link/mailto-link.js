import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'
export const MailToLink = ({ className, email, children, ...props }) => (
  <a
    href={`mailto:${email}`}
    {...props}
    className={cn('link', className)}
    target="_blank"
    rel="noopener noreferrer"
  >
    {children || email}
  </a>
)
MailToLink.propTypes = {
  email: PropTypes.string.isRequired,
}
