import React from 'react'

import { Router } from 'router'

import { Link, NavLink, MailToLink } from '.'

export default {
  component: Link,
  title: 'Navigation|Link',
  // decorators: [withKnobs],
}

export const link = () => (
  <Router>
    <Link to="/">Just a link</Link>
  </Router>
)

export const nav_link = () => (
  <Router>
    <NavLink to="/">nav-link</NavLink>
  </Router>
)

nav_link.story = {
  component: NavLink,
}

export const mailto_link = () => (
  <MailToLink email="someone@neighborhood.me">Mailto link</MailToLink>
)
