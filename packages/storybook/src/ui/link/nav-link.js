import React from 'react'
import PropTypes from 'prop-types'

import { RouterNavLink } from 'router'
import { RoutePath } from 'const/types'
import { cn } from 'utils'

const blur = (event) => event.target.blur()

/**
 * It adds a class to element when url matches current location
 */
export const NavLink = ({ className, disable, to, query, ...props }) =>
  disable ? (
    <span {...props} className={cn('link nav-link disable', className)} />
  ) : (
    <RouterNavLink
      {...props}
      to={to}
      query={query}
      className={cn('link nav-link', className)}
      onClick={blur}
    />
  )

NavLink.propTypes = {
  activeClassName: PropTypes.string,
  disable: PropTypes.bool,
  query: PropTypes.object,
  to: RoutePath.isRequired,
}
