/* eslint-disable jsx-a11y/anchor-has-content */
import './link.css'

import { Link } from './link'
import { NavLink } from './nav-link'
import { MailToLink } from './mailto-link'
import { ExternalLink } from './external-link'

export { Link, NavLink, MailToLink, ExternalLink }
