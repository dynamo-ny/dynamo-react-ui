import React from 'react'
import PropTypes from 'prop-types'

import { RoutePath } from 'const/types'
import { RouterLink } from 'router'
import { cn } from 'utils'

/*
  instead of 
  
  <Link to={generatePath(path, { uuid })}/>

  =>

  <Link to={path} query={{ uuid:uuid }} />

*/

const blur = (event) => event.target.blur()

export const Link = ({ theme, to, next, query, className, children, ...props }) => (
  <RouterLink
    {...props}
    to={to}
    next={next}
    query={query}
    className={cn('link', theme, className)}
    onClick={blur}
  >
    {children}
  </RouterLink>
)

Link.propTypes = {
  className: PropTypes.string,
  next: RoutePath,
  query: PropTypes.object,
  theme: PropTypes.oneOf([
    'round-button',
    'button',
    'contrast',
    'primary button',
    'secondary button',
    'flat button',
  ]),
  to: RoutePath.isRequired,
}
