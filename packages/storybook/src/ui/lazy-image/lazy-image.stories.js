import React from 'react'
import { storiesOf } from '@storybook/react'

import { LazyImage } from '.'

// src/components/Task.stories.js

const wrapperStyle = {
  display: 'grid',
  gridGap: '1rem',
  width: '50%',
  margin: 'auto',
  justifyItems: 'center',
}

const manyImgs = () => {
  const ImgSizes = ['150', '200', '250', '300', '350', '400', '450', '500', '550', '600']
  const images = new Array(20)
  images.fill(1)
  return images.map((size, i) => {
    const randSize = ImgSizes[Math.floor(Math.random() * ImgSizes.length)]
    return (
      <LazyImage
        src={`https://via.placeholder.com/${randSize}`}
        height={randSize}
        width={randSize}
        // eslint-disable-next-line react/no-array-index-key
        key={i}
      />
    )
  })
}

storiesOf('Lazy|Image', module).add('Image', () => <div style={wrapperStyle}>{manyImgs()}</div>)
