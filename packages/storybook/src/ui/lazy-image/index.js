import './lazy-image.css'
import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

const useObserver = (ref, cb, { root = null, rootMargin = '0px', threshold = 0.01 } = {}) => {
  const observer = useRef(
    new window.IntersectionObserver((entries) => entries[0].intersectionRatio > 0 && cb(), {
      root,
      rootMargin,
      threshold,
    }),
  )
  const currentObserver = observer.current

  useEffect(() => {
    currentObserver.observe(ref.current)
    return () => currentObserver.disconnect()
  }, [currentObserver, ref])
}

export const LazyImage = ({ src, className, ...props }) => {
  const [tempSrc, setSrc] = useState(null)
  const img = useRef(null)
  useObserver(img, () => setSrc(src))

  return <img {...props} className={cn(className, 'lazy-image')} ref={img} src={tempSrc} alt="" />
}

LazyImage.propTypes = {
  src: PropTypes.string.isRequired,
}
