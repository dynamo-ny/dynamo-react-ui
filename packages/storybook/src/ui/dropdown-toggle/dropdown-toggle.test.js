import React from 'react'
import { mount, shallow } from 'enzyme'

import { DropdownToggle } from '.'

describe('Dropdown Toggle', () => {
  it('to match snapshot', () => {
    const tree = shallow(<DropdownToggle label="Dropdown Toggle" hideIcon={false} />)
    expect(tree).toMatchSnapshot()
  })
  it('must render without exceptions', () => {
    expect(() => {
      mount(<DropdownToggle label="Dropdown Toggle" hideIcon={false} />)
    }).not.toThrow()
  })
  it('dropdown open after click', () => {
    const wrapper = mount(<DropdownToggle label="Dropdown Toggle" hideIcon={false} />)
    wrapper.find('button').simulate('click')
    expect(wrapper.instance().state.open).toBe(true)
  })
  it('dropdown open after press down key', () => {
    const wrapper = mount(<DropdownToggle label="Dropdown Toggle" hideIcon={false} />)
    wrapper.find('button').simulate('keyUp', { key: 'Down' })
    expect(wrapper.instance().state.open).toBe(true)
  })
  it('dropdown open after press down key and close after press key esc', () => {
    const wrapper = mount(<DropdownToggle label="Dropdown Toggle" hideIcon={false} />)
    wrapper.find('button').simulate('keyUp', { key: 'Down' })
    expect(wrapper.instance().state.open).toBe(true)
    wrapper.find('button').simulate('keyUp', { key: 'Escape' })
    expect(wrapper.instance().state.open).toBe(false)
  })
  it('dropdown close after double click', () => {
    const wrapper = mount(<DropdownToggle label="Dropdown Toggle" hideIcon={false} />)
    wrapper.find('button').simulate('click')
    wrapper.find('button').simulate('click')
    expect(wrapper.instance().state.open).toBe(false)
  })
  it("dropdown doesn't open after click with prop disabled", () => {
    const wrapper = mount(<DropdownToggle label="Dropdown Toggle" hideIcon={false} disabled />)
    wrapper.find('button').simulate('click')
    expect(wrapper.instance().state.open).toBe(false)
  })
})
