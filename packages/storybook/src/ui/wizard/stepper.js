import React from 'react'

import { Stepper } from 'ui/stepper'

import { useWizardState } from './wizard'

export const WizardStepper = (props) => {
  const { steps, index } = useWizardState()
  return <Stepper {...props} items={steps} current={index} />
}
