/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useCallback, useContext } from 'react'
import PropTypes from 'prop-types'

const WizardContext = React.createContext({})

export const WizardContextProvider = WizardContext.Provider

export const useWizardContext = () => useContext(WizardContext)
export const useWizardState = () => useWizardContext().state

export const Wizard = ({ current, children, urlFactory, onComplete, onStepChange }) => {
  const [steps, setSteps] = useState([])

  const state = {
    steps,
    index: current,
  }

  const dispatch = useCallback(
    ({ type, payload }) => {
      const next = getNextStepIndex(steps, current)
      const prev = getPrevStepIndex(steps, current)
      switch (type) {
        case 'init':
          setSteps((steps) => {
            return [
              ...steps,
              {
                fade: payload.fade ?? false,
                label: payload.label || `Step\u00A0${state.steps.length}`,
                path: urlFactory(steps.length),
                index: steps.length,
              },
            ]
          })
          break

        case 'disable':
          setSteps((steps) =>
            steps.map((step) =>
              step.index === payload.index && step.fade !== payload.fade
                ? { ...step, fade: payload.fade }
                : step,
            ),
          )
          break

        case 'next':
          if (next === steps.length) {
            onComplete()
          } else {
            onStepChange(steps[next])
          }
          break
        case 'prev':
          if (prev !== -1) {
            onStepChange(steps[prev])
          }
          break
      }
    },
    [steps, current],
  )

  return <WizardContextProvider value={{ state, dispatch }}>{children}</WizardContextProvider>
}

Wizard.propTypes = {
  current: PropTypes.number.isRequired,
  onComplete: PropTypes.func.isRequired,
  onStepChange: PropTypes.func.isRequired,
  urlFactory: PropTypes.func.isRequired,
}
Wizard.defaultProps = {
  current: 0,
}

function getNextStepIndex(steps, current) {
  return current + 1
}
function getPrevStepIndex(steps, current) {
  const prev = current - 1
  if (prev < 0) return -1
  return prev
}
