import React from 'react'

import { WizardStep } from './step'
import { WizardStepper } from './stepper'
import { RouteWizard } from './router-wizard'

export { WizardStep, WizardStepper }

export const Wizard = React.memo(RouteWizard)
