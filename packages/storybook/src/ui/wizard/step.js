/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

import { useWizardContext } from './wizard'

export const WizardStep = ({ label, component: Component, disable, ...props }) => {
  const { state, dispatch } = useWizardContext()

  const index = state?.steps?.findIndex((step) => step.label === label)
  const prev = index === 0 ? undefined : () => dispatch({ type: 'prev', payload: { index } })
  const next = () => dispatch({ type: 'next', payload: { index } })

  useEffect(() => {
    if (index === -1) {
      dispatch({ type: 'init', payload: { label, fade: disable } })
      return
    }
    if (state.steps[index].fade !== disable) {
      dispatch({ type: 'disable', payload: { index, fade: disable } })
      return
    }
    if (disable && state.steps[index].fade === disable && state.index === index) {
      prev()
    }
  }, [state, index, disable, dispatch])

  if (!Component || index === -1 || state.index !== index || disable) return false

  return <Component next={next} prev={prev} {...props} />
}

WizardStep.propTypes = {
  component: PropTypes.func,
  disable: PropTypes.bool,
  label: PropTypes.string.isRequired,
}
