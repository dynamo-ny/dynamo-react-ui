/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback } from 'react'
import PropTypes from 'prop-types'

import { useHistory, useParams } from 'router'
import { RoutePath } from 'const/types'

import { Wizard } from './wizard'

export const RouteWizard = ({ redirectTo, ...props }) => {
  const { step } = useParams()
  const history = useHistory()

  const handleStepChange = useCallback(
    ({ path }) => {
      history.push(path)
    },
    [history],
  )

  const handleComplete = useCallback(() => {
    redirectTo && history.push(redirectTo)
  }, [redirectTo, history])

  if (isNaN(Number(step ?? 0))) {
    return false
  }

  return (
    <Wizard
      {...props}
      current={Number(step ?? 0)}
      onStepChange={handleStepChange}
      onComplete={handleComplete}
    />
  )
}

RouteWizard.propTypes = {
  redirectTo: RoutePath,
  urlFactory: PropTypes.func.isRequired,
}
