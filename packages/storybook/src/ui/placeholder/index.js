import './styles.css'
import React from 'react'
import PropTypes from 'prop-types'

export const PlaceHolder = ({
  width,
  height = '50vh',
  margin = '1rem 0',
  padding,
  backgroundColor,
  img,
  display = 'flex',
  alignItems = 'center',
  justifyContent = 'center',
  children,
  style,
}) => {
  const imageWidth = width
    ? width.slice(-3) === 'rem'
      ? width.slice(0, -3) * 16
      : width.slice(0, -2)
    : 150
  const imageHeight = height
    ? height.slice(-3) === 'rem'
      ? height.slice(0, -3) * 16
      : height.slice(0, -2)
    : 150
  let backgroundImage =
    img && `https://via.placeholder.com/${imageWidth}x${height ? imageHeight : imageWidth}`

  return (
    <div
      style={{
        ...style,
        display,
        alignItems,
        justifyContent,
        maxWidth: width,
        height,
        margin: margin,
        padding,
        backgroundColor,
        backgroundImage: `${img ? 'url(' + backgroundImage + ')' : 'none'}`,
      }}
      className="placeholder"
    >
      {children}
    </div>
  )
}

PlaceHolder.propTypes = {
  alignItems: PropTypes.string,
  backgroundColor: PropTypes.string,
  display: PropTypes.string,
  height: PropTypes.string,
  img: PropTypes.bool,
  justifyContent: PropTypes.string,
  margin: PropTypes.string,
  padding: PropTypes.string,
  style: PropTypes.shape({}),
  width: PropTypes.string,
}
