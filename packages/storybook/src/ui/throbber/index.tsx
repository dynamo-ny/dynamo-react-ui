import './throbber.css'
import * as React from 'react'

import { cn } from 'utils'

export const Throbber: React.FC<React.HTMLAttributes<HTMLDivElement>> = function ({
  className,
  ...props
}) {
  return (
    <div {...props} className={cn('throbber', className)}>
      <div />
      <div />
      <div />
    </div>
  )
}
