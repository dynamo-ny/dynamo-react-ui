import React from 'react'
import { storiesOf } from '@storybook/react'

import { Throbber } from '.'

const wrapperStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
  width: '100%',
  height: '100%',
}

storiesOf('Load Data|Throbber', module).add('default', () => (
  <div style={wrapperStyle}>
    <Throbber />
  </div>
))
