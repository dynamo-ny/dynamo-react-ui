import React from 'react'

import { BreadcrumbsContext } from 'ui/breadcrumbs'

import { Router } from '../../router'
import { Header } from '.'

export default {
  component: Header,
  title: 'Page Layout|Header',
}

export const header = () => (
  <Header>
    <h1>Title</h1>
  </Header>
)

export const headerWithBreadcrumbs = () => (
  <Router>
    <BreadcrumbsContext to="/" label="Breadcrumbs">
      <BreadcrumbsContext to="/" label="Breadcrumbs-level-2">
        <Header title="Title" />
      </BreadcrumbsContext>
    </BreadcrumbsContext>
  </Router>
)
