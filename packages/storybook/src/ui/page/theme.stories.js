import 'theme.css'
import './theme-container.css'
import React from 'react'
import { Button } from '@dynamo-ny/react-form'

import { Section } from '.'

export default {
  title: 'Page Layout|Vertical Rhythm',
}

export const sample = () => (
  <div className="theme-container">
    <Section>
      <h1>
        Page Title H1
        <br />
        Two Lines
      </h1>
      <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Animi maxime dolor quae voluptatum
        adipisci consequatur aliquid minima voluptates cupiditate eveniet dolorem minus unde
        pariatur, similique laborum quia perspiciatis voluptas non?
      </p>
      <h1>Single Line</h1>
      <p>
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Animi maxime dolor quae voluptatum
        adipisci consequatur aliquid minima voluptates cupiditate eveniet dolorem minus unde
        pariatur, similique laborum quia perspiciatis voluptas non?
      </p>
      <div>
        <Button theme="primary">primary button</Button>
      </div>
      <h2>Heading level 2</h2>
      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
      <p className="secondary">
        Animi maxime dolor quae voluptatum adipisci consequatur aliquid minima voluptates cupiditate
        eveniet dolorem minus unde pariatur, similique laborum quia perspiciatis voluptas non?
      </p>
      <h3>Heading level 3</h3>
      <ul>
        <li>list item #1</li>
        <li>list item number two</li>
      </ul>
    </Section>
  </div>
)
