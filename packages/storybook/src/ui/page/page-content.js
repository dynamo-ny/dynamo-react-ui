import './page-content.css'
import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

export const Container = ({
  className,
  as: Component,
  cols,
  center,
  hero,
  theme,
  as,
  ...props
}) => (
  <Component
    {...props}
    className={cn(
      'page-content',
      center && 'center',
      hero && 'hero',
      theme,
      'cols-' + cols,
      className,
    )}
  />
)
Container.propTypes = {
  as: PropTypes.elementType,
  center: PropTypes.bool,
  cols: PropTypes.number,
  hero: PropTypes.bool,
  theme: PropTypes.oneOf(['elevated', 'gray']),
}
Container.defaultProps = {
  cols: 1,
  hero: false,
  center: false,
  as: 'section',
}
