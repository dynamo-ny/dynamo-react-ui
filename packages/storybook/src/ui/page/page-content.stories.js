import React from 'react'

import { PlaceHolder } from 'ui/placeholder'

import { Container } from '.'

export default {
  component: Container,
  title: 'Page Layout|Content',
}

export const default_content = () => (
  <Container>
    <PlaceHolder>Content</PlaceHolder>
  </Container>
)

export const content_2_cols = () => (
  <Container cols={2}>
    <PlaceHolder>col 1</PlaceHolder>
    <PlaceHolder>col 2</PlaceHolder>
  </Container>
)
export const content_3_cols = () => (
  <Container cols={3}>
    <PlaceHolder>col 1</PlaceHolder>
    <PlaceHolder>col 2</PlaceHolder>
    <PlaceHolder>col 3</PlaceHolder>
  </Container>
)
