import './page-section.css'
import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

export const Section = ({ className, as: Component, cols, theme, center, ...props }) => (
  <Component
    {...props}
    className={cn('page-section', 'cols-' + cols, center && 'center', theme, className)}
  />
)

Section.propTypes = {
  as: PropTypes.elementType,
  center: PropTypes.bool,
  cols: PropTypes.number,
  theme: PropTypes.oneOf(['elevated']),
}

Section.defaultProps = {
  as: 'section',
  cols: 1,
  theme: 'elevated',
}

export const SectionHTML = ({ html, ...props }) => (
  <Section {...props} dangerouslySetInnerHTML={{ __html: html }} />
)

SectionHTML.propTypes = {
  as: PropTypes.elementType,
  html: PropTypes.string.isRequired,
  theme: PropTypes.oneOf(['elevated']),
}
