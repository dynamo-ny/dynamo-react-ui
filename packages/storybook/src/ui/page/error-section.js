import './error-section.css'
import React from 'react'
import PropTypes from 'prop-types'

import { Section } from 'ui/page/page-section'

import { cn } from 'utils'

export const ErrorSection = ({ className, error, children, ...props }) => (
  <Section {...props} className={cn('error-section', className)}>
    {error || children}
  </Section>
)
ErrorSection.propTypes = {
  error: PropTypes.string,
}
