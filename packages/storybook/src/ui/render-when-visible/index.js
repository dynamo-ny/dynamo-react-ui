import React, { useState, useRef, useEffect } from 'react'

export const RenderWhenVisible = ({ children }) => {
  const [visible, setVisible] = useState(window.IntersectionObserver ? false : true)
  const element = useRef()

  useEffect(() => {
    let observer = new window.IntersectionObserver(
      (entries) => {
        if (entries.some((entry) => entry.isIntersecting)) {
          setVisible(true)
          observer.disconnect()
          observer = null
        }
      },
      {
        threshold: 1,
      },
    )

    element.current && observer.observe(element.current)

    return () => {
      observer && observer.disconnect()
    }
  }, [])

  return visible ? children : <div ref={element} />
}
