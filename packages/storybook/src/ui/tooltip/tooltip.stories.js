import React from 'react'
import { storiesOf } from '@storybook/react'

import { Tooltip } from '.'

const wrapperStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  margin: 'auto',
}

storiesOf('Tooltip', module).add('Tooltip', () => (
  <div style={wrapperStyle}>
    Hover it:
    <Tooltip>
      <span>Tooltip</span>
    </Tooltip>
  </div>
))
