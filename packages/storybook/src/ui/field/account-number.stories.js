import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { AccountNumberField } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

export default {
  component: AccountNumberField,
  title: 'Form/Fields/Account Number',
  decorators: [withKnobs],
}

export const account_number = () => (
  <Demo component={AccountNumberField} defaultValue={text('defaultValue', '123234345')} />
)
