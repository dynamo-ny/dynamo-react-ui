import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { PasswordField } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

export default {
  component: PasswordField,
  title: 'Form/Fields/Password',
  decorators: [withKnobs],
}

export const password = () => (
  <Demo component={PasswordField} defaultValue={text('defaultValue', '+12123851552')} />
)
