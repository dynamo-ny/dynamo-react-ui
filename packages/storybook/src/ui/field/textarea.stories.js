import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { TextareaField } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

export default {
  component: TextareaField,
  title: 'Form/Fields/Textarea',
  decorators: [withKnobs],
}

export const textarea = () => (
  <Demo component={TextareaField} defaultValue={text('defaultValue', 'some\ntext')} />
)
