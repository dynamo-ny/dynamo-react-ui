import React from 'react'
import { action } from '@storybook/addon-actions'
import { text } from '@storybook/addon-knobs'
import { Form, SecretField } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

const formActions = {
  onSubmit: action('Form.onSubmit'),
  onChange: action('Form.onChange'),
}
const fieldActions = {
  onChange: action('onChange'),
}

export default {
  component: SecretField,
  title: 'Form/Fields/Secret',
}

export const secret = () => (
  <Demo component={SecretField} defaultValue={text('defaultValue', 'default')} />
)

export const disabled = () => (
  <Form {...formActions}>
    <SecretField
      {...fieldActions}
      name="aSecretFieldDisabled"
      label="Disabled Secret Field"
      defaultValue="secret"
      disabled
    />
  </Form>
)
