import React from 'react'
import { withKnobs, text, number, radios } from '@storybook/addon-knobs'
import { TextField } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

export default {
  component: TextField,
  title: 'Form/Fields/Text',
  decorators: [withKnobs],
}

export const text_ = () => (
  <Demo
    component={TextField}
    minLength={number('Min Length', undefined)}
    maxLength={number('Max Length', undefined)}
    pattern={text('Pattern')}
    type={radios('Type', { Text: 'text', URL: 'url' }, 'text')}
  />
)
