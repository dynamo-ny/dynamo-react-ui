import React, { useRef } from 'react'
import PropTypes from 'prop-types'
import { action } from '@storybook/addon-actions'
import { text, boolean } from '@storybook/addon-knobs'
import {
  Form,
  FieldValue,
  IsFieldModified,
  IsModified,
  ResetButton,
  SubmitButton,
  IsValid,
  IsInvalid,
 Button } from '@dynamo-ny/react-form'


const formActions = {
  onSubmit: action('Form.onSubmit'),
  onChange: action('Form.onChange'),
}
const fieldActions = {
  onChange: action('onChange'),
  onReset: action('onReset'),
}

export const Demo = ({ component: Component, children, ...props }) => {
  /* eslint-disable-next-line react-hooks/rules-of-hooks */
  const ref = useRef()

  return (
    <Form {...formActions} style={{ width: '25rem' }}>
      <Component
        ref={ref}
        name="field"
        {...fieldActions}
        label={text('label', 'A field`s label')}
        required={boolean('required', true)}
        disabled={boolean('disabled', false)}
        readOnly={boolean('read-only', false)}
        placeholder={text('placeholder', 'placeholder')}
        {...props}
      />
      {children}

      <hr />

      <ul className="x" style={{ padding: 0 }}>
        <li>
          Current Value:{' '}
          <b>
            <FieldValue name="field" />
          </b>
        </li>

        <li>
          field is{' '}
          <IsFieldModified name="field">
            <b>modified</b>
          </IsFieldModified>
          <IsFieldModified not name="field">
            not modified
          </IsFieldModified>
        </li>
        <li>
          form is{' '}
          <IsModified>
            <b>modified</b>
          </IsModified>
          <IsModified not>not modified</IsModified>
          {' and is '}
          <IsValid>valid</IsValid>
          <IsInvalid>
            <b>invalid</b>
          </IsInvalid>
        </li>
      </ul>

      <div>
        <Button
          theme="secondary"
          onClick={() => {
            ref.current.focus()
          }}
        >
          Focus
        </Button>
        <ResetButton>Reset</ResetButton>
        <SubmitButton>Submit</SubmitButton>
      </div>
    </Form>
  )
}

Demo.propTypes = {
  component: PropTypes.any.isRequired,
}
