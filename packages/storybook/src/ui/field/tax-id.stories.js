import React from 'react'
import { withKnobs, radios } from '@storybook/addon-knobs'
import { TaxIdField } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

export default {
  component: TaxIdField,
  title: 'Form/Fields/Tax ID',
  decorators: [withKnobs],
}

export const tax_id = () => (
  <Demo
    component={TaxIdField}
    country={radios('Country', { 'United States': 'US', Ukraine: 'UA' }, 'US')}
    ownerType={radios('Investor Type?', { Person: 'person', Company: 'company' }, 'person')}
  />
)
