import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { DOBField } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

export default {
  component: DOBField,
  title: 'Form/Fields/Secured Date of Birth',
  decorators: [withKnobs],
}

export const secured_data_of_birth = () => (
  <Demo component={DOBField} defaultValue={text('defaultValue', '1985-**-**')} />
)
