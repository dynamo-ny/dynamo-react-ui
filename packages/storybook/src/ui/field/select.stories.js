import React from 'react'
import { action } from '@storybook/addon-actions'
import { radios } from '@storybook/addon-knobs'
import { Form, Select } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

const formActions = {
  onSubmit: action('Form.onSubmit'),
  onChange: action('Form.onChange'),
}
const selectActions = {
  onChange: action('onChange'),
}

export default {
  component: Select,
  title: 'Form/Fields/Select',
}

export const select_field = () => (
  <Demo
    component={Select}
    defaultValue={radios('Default Value', { 1: '1', 2: '2', 3: '3' })}
    options={{ 1: 'one', 2: 'two', 3: 'three' }}
  />
)
export const placeholder = () => (
  <Form {...formActions}>
    <Select
      name="select2"
      options={{ a: 1, b: 2, c: 3 }}
      label="with placeholder"
      placeholder="please choose..."
      {...selectActions}
    />
  </Form>
)
export const disabled = () => (
  <Form {...formActions}>
    <Select
      name="select3"
      defaultValue="a"
      options={{ a: 1, b: 2, c: 3 }}
      label="disabled"
      disabled
      {...selectActions}
    />
  </Form>
)
