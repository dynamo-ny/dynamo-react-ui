import React from 'react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { PhoneField } from '@dynamo-ny/react-form'

import { Demo } from './storybook-field-demo'

export default {
  component: PhoneField,
  title: 'Form/Fields/Phone',
  decorators: [withKnobs],
}

export const phone = () => (
  <Demo component={PhoneField} defaultValue={text('defaultValue', '+12123851552')} />
)
