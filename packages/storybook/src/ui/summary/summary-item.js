import './summary-item.css'
import React from 'react'
import PropTypes from 'prop-types'

import { Money } from 'ui/money'

import { cn } from 'utils'

export const SummaryItem = ({ className, children, label, value, isCurrency }) => (
  <div className={cn('summary-item', className)}>
    {isCurrency ? (
      <Money className="value" amount={value} />
    ) : (
      <span className="value">{value}</span>
    )}
    <span className="label">{label || children}</span>
  </div>
)

SummaryItem.propTypes = {
  isCurrency: PropTypes.bool,
  label: PropTypes.string,
  value: PropTypes.number,
}
