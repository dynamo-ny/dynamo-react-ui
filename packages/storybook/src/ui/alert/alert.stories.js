import React from 'react'
import { Button } from '@dynamo-ny/react-form'

import { AlertProvider, useAlert } from '.'
import { AlertMessage } from './alert-message'

export default {
  component: AlertProvider,
  title: 'Messaging|Alerts',
}

const Buttons = () => {
  const alerts = useAlert()
  return (
    <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr 1fr', gap: '1rem' }}>
      <div style={{ gridColumn: '1 / 4' }}>default (persist: false, timeout: 7 sec)</div>

      <Button primary onClick={() => alerts.success('Success Message')}>
        Success Message
      </Button>
      <Button primary onClick={() => alerts.warning('Warning Message')}>
        Warning Message
      </Button>
      <Button primary onClick={() => alerts.error('Error Message')}>
        Error Message
      </Button>

      <div style={{ gridColumn: '1 / 4' }}>persist</div>

      <Button
        primary
        onClick={() => alerts.success({ persist: true, message: 'Success Message (persist)' })}
      >
        Success (persist)
      </Button>
      <Button
        primary
        onClick={() => alerts.warning({ persist: true, message: 'Warning Message (persist)' })}
      >
        Warning (persist)
      </Button>
      <Button
        primary
        onClick={() => alerts.error({ persist: true, message: 'Error Message (persist)' })}
      >
        Error (persist)
      </Button>

      <div style={{ gridColumn: '1 / 4' }}>custom timeout</div>

      <Button primary onClick={() => alerts.success({ timeout: 500, message: 'Success 0.5s' })}>
        Success 0.5s
      </Button>
      <Button primary onClick={() => alerts.warning({ timeout: 2000, message: 'Warning 2s' })}>
        Warning 2s
      </Button>
      <Button primary onClick={() => alerts.error({ timeout: 10000, message: 'Error 10s' })}>
        Error 10s
      </Button>

      <div style={{ gridColumn: '1 / 4' }}>show &amp; hide</div>

      <Button
        primary
        onClick={() => alerts.success({ persist: true, key: 'TOGGLE', message: 'Success' })}
      >
        Show Success
      </Button>
      <Button primary onClick={() => alerts.close('TOGGLE')}>
        Hide Success
      </Button>
      <Button theme="alert" onClick={() => alerts.clear()}>
        Clear All
      </Button>
    </div>
  )
}

export const playground = () => (
  <div style={{ width: '100%', height: '100%' }}>
    <AlertProvider>
      <Buttons />
    </AlertProvider>
  </div>
)

export const success = () => (
  <AlertMessage messageKey="X" message="Success Message" theme="success" />
)
export const success_persistent = () => (
  <AlertMessage messageKey="X" message="Success Message" theme="success" persist />
)
export const warning = () => (
  <AlertMessage messageKey="X" message="Warning Message" theme="warning" />
)
export const warning_persistent = () => (
  <AlertMessage messageKey="X" message="Warning Message" theme="warning" persist />
)
export const error = () => <AlertMessage messageKey="X" message="Error Message" theme="error" />
export const error_persistent = () => (
  <AlertMessage messageKey="X" message="Error Message" theme="error" persist />
)
