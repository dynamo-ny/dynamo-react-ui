import React, { useContext, useReducer } from 'react'

import { AlertsContainer } from './alert-container'

const ADD = 'add'
const REMOVE = 'remove'
const CLEAR_ALL = 'clear-errors'
// const CLEAR_ERRORS = 'clear-errors'

const INITIAL_STATE = []
/**
 * @param {Message[]} state
 * @param {AlertAction} action
 * @returns {Message[]}
 */
const reducer = (state = INITIAL_STATE, { type, ...props }) => {
  switch (type) {
    case ADD:
      return [...state, props]
    case REMOVE:
      return state.filter((item) => item.key !== props.key)
    case CLEAR_ALL:
      return INITIAL_STATE
    default:
      return state
  }
}

/**
 * @param {function(AlertAction)} dispatch
 * @returns {Alerts}
 */
const createAlertsDispatcher = (dispatch) => {
  const showMessage = ({
    timeout = 7000,
    key = Date.now().toString(),
    persist = false,
    ...props
  }) => {
    if (!props.message) return
    dispatch({ ...props, persist, key, type: ADD })
    const removeMessage = () => dispatch({ ...props, key, type: REMOVE })
    timeout && !persist && setTimeout(removeMessage, timeout)
    return removeMessage
  }

  const parseMessageArg = (message) => (typeof message === 'string' ? { message } : message || {})

  return {
    error: (message) => showMessage({ ...parseMessageArg(message), theme: 'error' }),
    warning: (message) => showMessage({ ...parseMessageArg(message), theme: 'warning' }),
    success: (message) => showMessage({ ...parseMessageArg(message), theme: 'success' }),
    close: (key) => dispatch({ key: key || key.key, type: REMOVE }),
    clear() {
      dispatch({ type: CLEAR_ALL })
    },
  }
}

const AlertApiContext = React.createContext()

/**
 * @returns {Alerts}
 */
export const useAlert = () => {
  return useContext(AlertApiContext)
}

export const AlertProvider = ({ children }) => {
  const [messages, dispatch] = useReducer(reducer, INITIAL_STATE)
  const alertsApi = createAlertsDispatcher(dispatch)

  return (
    <>
      <AlertsContainer messages={messages} alertsApi={alertsApi} />
      <AlertApiContext.Provider value={alertsApi}>{children}</AlertApiContext.Provider>
    </>
  )
}

/**
 * @typedef {Object} Message
 * @prop {('error'|'success'|'warning')} theme
 * @prop {string|(import('react').Component)} [message]
 * @prop {boolean} [persist]
 */
/**
 * @typedef {Object} AlertAction
 * @prop {('add-error'|'add-success'|'add-warning'|'remove'|'clear-errors')} type
 * @prop {string|(import('react').Component)} [message]
 * @prop {boolean} [persist]
 */
/**
 * @typedef {Object} Alerts
 * @property {function(String message,Boolean persist,Number timeout):void} error
 * @property {function(String message,Boolean persist,Number timeout):void} success
 * @property {function(String message,Boolean persist,Number timeout):void} warning
 * @property {function(String message):void} close
 * @property {function():void} clear
 */
