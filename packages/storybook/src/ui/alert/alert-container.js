import './alert-container.css'
import React from 'react'
import PropTypes from 'prop-types'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import { AlertMessage } from './alert-message'

export const AlertsContainer = ({ messages, alertsApi }) => (
  <div className="alert-container">
    <TransitionGroup className="messages">
      {messages?.map(({ message, key, ...props }) => (
        <CSSTransition timeout={500} classNames="message" key={key}>
          <AlertMessage {...props} messageKey={key} message={message} alertsApi={alertsApi} />
        </CSSTransition>
      ))}
    </TransitionGroup>
  </div>
)

AlertsContainer.propTypes = {
  alertsApi: PropTypes.shape({
    close: PropTypes.func.isRequired,
  }).isRequired,
  messages: PropTypes.arrayOf(PropTypes.shape(AlertMessage.propTypes)),
}
