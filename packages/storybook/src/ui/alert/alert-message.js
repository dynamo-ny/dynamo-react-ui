import './alert-message.css'
import React from 'react'
import PropTypes from 'prop-types'
import { IconButton } from '@dynamo-ny/react-form'

import { Link } from 'ui/link'

import { cn } from 'utils'
import IconClose from 'icons/close.svg'
import { RoutePath } from 'const/types'

export const AlertMessage = ({
  className,
  messageKey,
  theme,
  message,
  alertsApi,
  persist,
  link,
}) => {
  const close = () => {
    alertsApi.close(messageKey)
  }
  return (
    <div className={cn('alert-message', !persist && 'closable', theme, className)}>
      <span>
        {message}{' '}
        {link && (
          <Link to={link.path} onClick={link.closeAlert ? close : undefined}>
            {link.text}
          </Link>
        )}
      </span>
      {!persist && (
        <IconButton onClick={close}>
          <IconClose />
        </IconButton>
      )}
    </div>
  )
}

AlertMessage.propTypes = {
  alertsApi: PropTypes.shape({
    close: PropTypes.func.isRequired,
  }),
  link: PropTypes.shape({
    path: RoutePath.isRequired,
    text: PropTypes.string.isRequired,
    closeAlert: PropTypes.bool,
  }),
  message: PropTypes.node.isRequired,
  messageKey: PropTypes.string,
  persist: PropTypes.bool,
  theme: PropTypes.oneOf(['error', 'success', 'warning']).isRequired,
}
