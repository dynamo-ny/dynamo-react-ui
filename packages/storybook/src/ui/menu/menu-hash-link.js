/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

import { navigateKeyboard } from '.'
export const toAnchor = (title) => (title || '').replace(/\s+/g, '-').toLowerCase()
export const toHashLink = (title) => '#' + toAnchor(title)

export const MenuHashLink = ({ className, children, to, ...props }) => {
  return (
    <li className={cn('menu-item', className)} onKeyUp={navigateKeyboard}>
      <a {...props} className="menu-link nav-link" href={toHashLink(to)}>
        {children}
      </a>
    </li>
  )
}

MenuHashLink.propTypes = {
  to: PropTypes.string.isRequired,
}
