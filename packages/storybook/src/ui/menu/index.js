/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import './menu.css'

import React, { useCallback, useRef, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import debounce from 'debounce'
import { Button, ToggleButton } from '@dynamo-ny/react-form'

import { NavLink } from 'ui/link'

import { cn } from 'utils'
import { RoutePath } from 'const/types'
import Pimp from 'icons/pimp.svg'

/**
 * @param {HTMLElement} element
 * @param {string} query
 */
const focusNextSibling = (element, query) => {
  if (!element.nextElementSibling) return
  const next = query ? element.nextElementSibling.querySelector(query) : element.nextElementSibling
  next && next.focus()
}
/**
 * @param {HTMLElement} element
 * @param {string} query
 */
const focusPreviousSibling = (element, query) => {
  if (!element.previousElementSibling) return
  const previous = query
    ? element.previousElementSibling.querySelector(query)
    : element.previousElementSibling
  previous && previous.focus()
}

/**
 * @param {KeyboardEvent} event
 */
export const navigateKeyboard = (event) => {
  switch (event.key) {
    case 'Down': // IE/Edge specific value
    case 'ArrowDown':
      focusNextSibling(event.currentTarget, 'a,button')
      event.preventDefault()
      return
    case 'Up': // IE/Edge specific value
    case 'ArrowUp':
      focusPreviousSibling(event.currentTarget, 'a,button')
      event.preventDefault()
      return
  }
}

export const Menu = ({
  direction,
  dropdown = false,
  toLeft = false,
  className,
  children,
  ...props
}) => {
  return (
    <nav
      {...props}
      className={cn('menu', direction, dropdown && 'dropdown', toLeft && 'to-left', className)}
    >
      <ul className="menu-list">{children}</ul>
    </nav>
  )
}
Menu.propTypes = {
  direction: PropTypes.oneOf(['horizontal', 'vertical']),
  dropdown: PropTypes.bool,
  toLeft: PropTypes.bool,
}
Menu.defaultProps = {
  direction: 'horizontal',
}

export const MenuGroup = ({ open = false, className, label, children }) => {
  const [isOpen, setOpen] = useState(open)
  const element = useRef()
  const button = useRef()

  useEffect(() => {
    open !== isOpen && setOpen(open)
  }, [open])

  // const toggleOpen = () => {
  //   if (isOpen) return
  //   button.current && button.current.focus()
  //   setOpen(true)
  // }

  const toggleClose = () => {
    if (!isOpen) return
    if (isSameOrChildElement(element.current, document.activeElement)) {
      document.activeElement.blur()
    }
    setOpen(false)
  }

  const toggle = () => {
    button.current && button.current.blur()
    setOpen(!isOpen)
  }

  const blur = () => {
    if (isSameOrChildElement(element.current, document.activeElement)) {
      document.activeElement.blur()
    }
  }

  const validateFocus = useCallback(
    debounce(() => {
      if (!isSameOrChildElement(element.current, document.activeElement)) {
        toggleClose()
      }
    }, 50),
    [isOpen],
  )

  return (
    <li
      className={cn('menu-group', className, isOpen && 'open')}
      onBlur={validateFocus}
      onMouseOut={validateFocus}
      ref={element}
    >
      <Button onClick={toggle} className="menu-group-toggle desktop" ref={button}>
        {label}
        <Pimp className="menu-group-toggle-pimp" />
      </Button>
      <ToggleButton className="menu-group-toggle tablet">
        {label}
        <Pimp className="menu-group-toggle-pimp" />
      </ToggleButton>
      <ul className="submenu" onClick={blur}>
        {children}
      </ul>
    </li>
  )
}
MenuGroup.propTypes = {
  label: PropTypes.node,
  open: PropTypes.bool,
}

export const MenuLink = ({ className, children, ...props }) => {
  return (
    <li className={cn('menu-item', className)} onKeyUp={navigateKeyboard}>
      <NavLink {...props} className="menu-link nav-link">
        {children}
      </NavLink>
    </li>
  )
}
MenuLink.propTypes = {
  to: RoutePath,
}

export const MenuButton = ({ className, ...props }) => {
  return (
    <li className={cn('menu-item', className)} onKeyUp={navigateKeyboard}>
      <Button type="button" {...props} className="menu-link menu-button" />
    </li>
  )
}

export const MenuTitle = ({ className, ...props }) => {
  return <li {...props} className={cn('menu-item menu-title', className)} />
}

export function isSameOrChildElement(parent, child) {
  return parent && child && (parent === child || parent.contains(child))
}
