import React from 'react'
import { action } from '@storybook/addon-actions'

import { Router } from 'router'

import { Menu, MenuGroup, MenuLink, MenuButton, MenuTitle } from '.'

export default {
  component: Menu,
  title: 'Navigation/Dropdown Menu',
}

export const example = () => (
  <div
    style={{
      width: '100vw',
      height: '100vh',
      display: 'flex',
      alignItems: 'stretch',
      flexDirection: 'column',
    }}
  >
    <Router>
      <Menu dropdown style={{ backgroundColor: 'slategrey', color: 'white' }}>
        <MenuGroup label="Category">
          <MenuLink to="#">Link to some page</MenuLink>
          <MenuLink to="#">Another link</MenuLink>
          <MenuTitle>subtitle</MenuTitle>
          <MenuButton onClick={action('click')}>A Button</MenuButton>
        </MenuGroup>
        <MenuGroup label="A Copy">
          <MenuLink to="#">Link to some page</MenuLink>
          <MenuLink to="#">Another link</MenuLink>
          <MenuTitle>subtitle</MenuTitle>
          <MenuButton onClick={action('click')}>A Button</MenuButton>
        </MenuGroup>
      </Menu>
    </Router>
  </div>
)
