import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

export const Money = ({ className, currency, integer, amount, locales, ...props }) => {
  const formatter = new Intl.NumberFormat(locales, {
    currency,
    style: 'currency',
    ...(integer && {
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }),
  })

  return (
    <span {...props} className={cn('money', className)}>
      {formatter.format(amount || 0)}
    </span>
  )
}

Money.propTypes = {
  amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  currency: PropTypes.string.isRequired,
  integer: PropTypes.bool,
  locales: PropTypes.string.isRequired,
}

Money.defaultProps = {
  currency: 'USD',
  amount: 0,
  locales: 'en-US', // window.navigator.languages ???
}
