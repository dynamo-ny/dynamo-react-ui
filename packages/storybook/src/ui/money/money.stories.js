import React from 'react'
import { withKnobs, text, number } from '@storybook/addon-knobs'

import { Money } from '.'

export default {
  component: Money,
  title: 'Buttons|Money',
  decorators: [withKnobs],
}

export const money = () => (
  <Money
    amount={number('amount', 100)}
    currency={text('currency', 'USD')}
    locales={text('locales', 'en-US')}
  />
)
