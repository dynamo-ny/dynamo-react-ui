import React from 'react'
import { action } from '@storybook/addon-actions'
import { Button, IconButton } from '@dynamo-ny/react-form'

import IconShow from 'icons/visibility-show.svg'


// src/components/Task.stories.js

export default {
  component: Button,
  title: 'Buttons|Button',
  parameters: {
    docs: {
      //
    },
  },
}

const actions = {
  onClick: action('click'),
}

export const primary = () => (
  <Button primary {...actions}>
    Primary
  </Button>
)

export const disabled = () => (
  <Button primary type="submit" disabled {...actions}>
    Button
  </Button>
)

export const theme_success = () => (
  <Button type="submit" theme="success" {...actions}>
    Button
  </Button>
)
export const theme_success_disabled = () => (
  <Button type="submit" theme="success" disabled {...actions}>
    Button
  </Button>
)
export const theme_alert = () => (
  <Button type="reset" theme="alert" {...actions}>
    Button
  </Button>
)

export const reset = () => (
  <Button type="reset" {...actions}>
    Button
  </Button>
)

export const icon = () => (
  <IconButton {...actions}>
    <IconShow />
  </IconButton>
)
