import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

export const LocalDate = ({ value, year, month, day, className }) => {
  const date = new Date(value)
  const formattedDate = date.toLocaleDateString('en-US', { year, month, day })
  return <span className={cn('local-date', className)}>{formattedDate}</span>
}

LocalDate.propTypes = {
  day: PropTypes.string,
  month: PropTypes.string,
  value: PropTypes.string,
  year: PropTypes.string,
}
LocalDate.defaultProps = {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
}
