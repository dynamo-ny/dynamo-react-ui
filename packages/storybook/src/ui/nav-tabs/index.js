import './styles.css'
import React from 'react'
import PropTypes from 'prop-types'

import { NavLink } from 'ui/link'

export const NavTabs = ({ items }) => (
  <>
    <nav className="nav nav-tabs">
      {items.map(({ to, path, label }) => (
        <NavLink
          key={path}
          to={typeof to === 'function' ? to() : typeof to === 'string' ? to : path}
          className="nav-item"
        >
          {label}
        </NavLink>
      ))}
    </nav>
    {/* <Switch>
      {items.map(({ path, component }) => (
        <Route key={path} path={path} component={component} />
      ))}
      <Redirect to={items[0].path} />
    </Switch> */}
  </>
)
NavTabs.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.exact({
      path: PropTypes.string.isRequired,
      to: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
      label: PropTypes.string.isRequired,
      component: PropTypes.elementType.isRequired,
    }),
  ).isRequired,
}
