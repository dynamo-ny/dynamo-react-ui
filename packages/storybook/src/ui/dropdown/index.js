import React from 'react'
import T from 'prop-types'
import debounce from 'debounce'
import { BaseInput } from '@dynamo-ny/react-form'

import { FocusGroup } from 'ui/focus-group'

import IconDown from 'icons/down.svg'
import { cn } from 'utils'

import styles from './dropdown.css'

const OPENED_CLASS_NAME = styles.open
const SEARCH_RESET_TIMEOUT = 500

export default class Dropdown extends BaseInput {
  static propTypes = {
    ...BaseInput.propTypes,
    allowSearch: T.bool,
    defaultValue: T.any,
    // name: T.string.isRequired,
    // label: T.string,
    label: T.string,
    options: T.arrayOf(T.shape({ value: T.any.isRequired, label: T.string })),
    // required: T.bool,
    placeholder: T.string,
  }

  static defaultProps = {
    placeholder: 'Please select',
  }

  constructor(...args) {
    super(...args)
    this.state = {
      ...this.state,
      value: this.props.defaultValue,
      open: false,
      filter: null,
      search: false,
    }
  }

  button = React.createRef()
  optionsElement = React.createRef()
  searchElement = React.createRef()

  onButtonClick = (event) => {
    if (this.props.allowSearch) {
      this.setState({ search: true }, () => {
        this.searchElement.current && this.searchElement.current.focus()
        this.open()
      })
    } else {
      this.toggle(event)
    }
  }
  open = () => {
    this.setState({ open: true }, () => {
      this.focusOption(this.getOptionByValue(this.state.value))
    })
  }
  close = () => {
    this.setState({ open: false })
  }
  toggle = () => {
    this.state.open ? this.close() : this.open()
  }

  focusOption(option) {
    /** @type {HTMLFieldSetElement} */
    const el = this.optionsElement.current
    if (!el) return
    const buttonElement = el.querySelector(option ? `button[value="${option.value}"]` : 'button')
    buttonElement && buttonElement.focus()
  }

  getFocusedOptionElement() {
    /** @type {HTMLFieldSetElement} */
    const el = this.optionsElement.current
    return el ? el.querySelector('button:focus') : null
  }
  getFirstOptionElement() {
    /** @type {HTMLFieldSetElement} */
    const el = this.optionsElement.current
    return el ? el.querySelector('button:first-child') : null
  }
  getLastOptionElement() {
    /** @type {HTMLFieldSetElement} */
    const el = this.optionsElement.current
    return el ? el.querySelector('button:last-child') : null
  }
  focusFirstOption() {
    const el = this.getFirstOptionElement()
    el && el.focus()
  }
  focusPreviousOption() {
    const current = this.getFocusedOptionElement()
    const prev = current && current.previousElementSibling
    if (prev) {
      prev.focus()
    } else {
      this.focusLastOption()
    }
  }
  focusNextOption() {
    const current = this.getFocusedOptionElement()
    const next = current && current.nextElementSibling
    if (next) {
      next.focus()
    } else {
      this.focusFirstOption()
    }
  }

  /**
   * @param {KeyboardEvent} event
   */
  onButtonKeyUp = (event) => {
    switch (event.key) {
      case 'Esc':
      case 'Escape':
        this.close()
        break

      case 'Home':
        this.focusFirstOption()
        break

      case 'End':
        this.focusLastOption()
        break

      case 'Backspace':
        this.resetSearchQuery()
        break

      case 'Down': // IE/Edge specific value
      case 'ArrowDown':
        this.focusNextOption()
        break

      case 'Up': // IE/Edge specific value
      case 'ArrowUp':
        this.focusPreviousOption()
        break

      default:
        if ((event.key + '').length === 1) {
          // just a char, not a special value
          this.setSearchQuery(event.key)
          this.setClearSearchTimeout()
        }
        return
    }
    // event.preventDefault()
    // event.stopPropagation()
  }

  searchQuery = ''
  resetSearchQuery = () => {
    this.searchQuery = ''
  }
  setClearSearchTimeout = debounce(this.resetSearchQuery, SEARCH_RESET_TIMEOUT)

  setSearchQuery(query = '') {
    this.searchQuery += query
    const option = this.props.options.find(getOptionsFilter(this.searchQuery))
    if (option) {
      this.focusOption(option)
    }
  }

  /**
   * @param {Event} event
   */
  onChange = (event) => {
    const { value } = event.target
    if (value === this.state.value) {
      this.close()
    } else {
      this.setState(
        {
          touched: true,
          value: value,
        },
        () => {
          this.close()
          this.broadcastUpdates()
        },
      )
    }
  }

  /**
   * @returns {boolean}
   */
  getValue() {
    return this.state.value
  }

  /**
   * @returns {boolean}
   */
  isValid() {
    return this.props.required ? !!this.getValue() : true
  }

  getOptionByValue(value) {
    return this.props.options.find((option) => option.value === value)
  }

  getLabelFor(value) {
    const option = this.getOptionByValue(value)
    return option ? option.label : undefined
  }

  close() {
    const element = this.button.current
    if (!element) {
      return
    }
    element.classList.remove(OPENED_CLASS_NAME)
  }

  renderButton() {
    const label = this.getLabelFor(this.state.value)
    return (
      <button
        type="button"
        className="dropdown-button"
        onClick={this.onButtonClick}
        ref={this.button}
      >
        {label === undefined ? (
          <span className="placeholder">{this.props.placeholder}</span>
        ) : (
          <span className="dropdown-value">{label}</span>
        )}
        <IconDown className="dropdown-icon" />
      </button>
    )
  }

  componentDidMount() {
    this.broadcastUpdates()
  }

  render() {
    const { options, label } = this.props

    return (
      <FocusGroup
        onKeyUp={this.onButtonKeyUp}
        className={cn('dropdown', this.state.open && 'open')}
        onBlur={this.close}
      >
        {label && <div className={cn('label-text', this.isValid() && 'valid')}>{label}</div>}
        {this.renderButton()}

        <div className="options" ref={this.optionsElement}>
          {options.map(({ label, value }) => (
            <button
              className="option"
              role="switch"
              key={value}
              type="button"
              value={value}
              onClick={this.onChange}
              aria-checked={value === this.state.value}
            >
              {label}
            </button>
          ))}
        </div>
      </FocusGroup>
    )
  }
}

function getOptionsFilter(value) {
  if (!value) {
    return () => true
  }
  const regExp = new RegExp('^' + (value + '').toLowerCase(), 'i')
  return ({ label }) => label.match(regExp)
}
