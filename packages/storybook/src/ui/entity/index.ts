import { Entity as EntityToProps } from './entity'
import { EntityContext } from './entity-context'
export { useEntityContext } from './use-entity-context'

export const Entity = {
  toProps: EntityToProps,
  toContext: EntityContext,
}
