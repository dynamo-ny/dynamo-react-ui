import React, { ElementType, Children, FC } from 'react'

import { LoaderError } from 'ui/loader/use-loader-state'
import { GetEntityEndpoint, useEntityLoader } from 'ui/loader/use-entity-loader'
import { Throbber } from 'ui/throbber'
import { LoaderErrorHandler } from 'ui/loader/LoaderErrorHandler'

interface EntityProps {
  errorComponent?: ElementType<{
    error: LoaderError
  }>
  get: GetEntityEndpoint<any>
  pendingComponent?: ElementType
  uuid: string
}

export const Entity: FC<EntityProps> = ({
  children,
  uuid,
  get,
  pendingComponent: Pending,
  errorComponent: Error,
}) => {
  const { pending, error, entity, setEntity } = useEntityLoader(get, uuid)
  if (pending) return <Pending />
  if (error) return <Error error={error} />

  if (!entity) return null

  const props = {
    setEntity,
    entity,
  }

  return (
    <span>
      {Children.map(children, (child) =>
        React.isValidElement(child) ? React.cloneElement(child, props) : null,
      )}
    </span>
  )
}

Entity.defaultProps = {
  pendingComponent: Throbber,
  errorComponent: LoaderErrorHandler,
}
