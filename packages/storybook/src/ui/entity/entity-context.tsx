import React from 'react'

import { Throbber } from 'ui/throbber'
import { LoaderErrorHandler } from 'ui/loader/LoaderErrorHandler'
import { LoaderError } from 'ui/loader/use-loader-state'
import { useEntityLoader, GetEntityEndpoint } from 'ui/loader/use-entity-loader'

import { EntityContextValue, EntityContextProvider } from './use-entity-context'

type EntityContextProps = {
  contextProviderComponent?: React.ElementType<{
    value: EntityContextValue<any>
  }>
  errorComponent?: React.ElementType<{
    error: LoaderError
  }>
  get: GetEntityEndpoint<any>
  pendingComponent?: React.ElementType
  uuid: string
}

export const EntityContext: React.FC<EntityContextProps> = ({
  children,
  uuid,
  get,
  contextProviderComponent: Provider,
  pendingComponent: Pending,
  errorComponent: Error,
}) => {
  const { pending, error, entity, setEntity } = useEntityLoader(get, uuid)
  if (pending) return <Pending />
  if (error) return <Error error={error} />

  return entity ? <Provider value={[entity, setEntity]}>{children}</Provider> : null
}

EntityContext.defaultProps = {
  contextProviderComponent: EntityContextProvider,
  pendingComponent: Throbber,
  errorComponent: LoaderErrorHandler,
}
