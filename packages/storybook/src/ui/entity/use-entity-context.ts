import { createContext, useContext } from 'react'

export type EntityContextValue<T> = [T | null, (T) => void]

const DefaultEntityContext = createContext<EntityContextValue<any>>([
  null,
  (): void => {
    throw new Error('EntityContext not found')
  },
])

export const EntityContextProvider = DefaultEntityContext.Provider

export function useEntityContext<T>(): EntityContextValue<T> {
  return useContext(DefaultEntityContext)
}
