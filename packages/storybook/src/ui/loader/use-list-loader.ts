/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react'

import { LoaderState, useLoaderState } from './use-loader-state'

type ListQuery<T> = {
  page?: number
  limit?: number
  order?: Record<keyof T, 'ASC' | 'DESC'>
} & {
  [F in keyof T]: T[F]
}

type ListEndpointResult<T> = { items: T[]; totalItems: number }

type ListEndpoint<T> = (query: ListQuery<T>) => Promise<ListEndpointResult<T>>

type ListLoaderState<T> = LoaderState<ListEndpointResult<T>>

type ListLoader<T> = ListLoaderState<T> & {
  page: number
  limit: number
  refresh: () => void
}

export type ListRefreshAction<T> = () => Promise<ListEndpointResult<T> | void>

export function useListLoader<T>(
  endpoint: ListEndpoint<T>,
  { page = 1, order, limit = 15, ...filter }: ListQuery<T>,
): ListLoader<T> {
  const [{ pending, error, result }, load, abort] = useLoaderState<ListEndpointResult<T>>()

  const refresh: ListRefreshAction<T> = () => {
    return load(endpoint, {
      page,
      ...(order && { order }),
      limit: Number(limit),
      ...filter,
    })
  }

  useEffect(() => {
    refresh()
    return abort
  }, [
    endpoint,
    Number(page),
    Number(limit),
    ...((order && Object.entries(order).flat()) || []),
    ...((filter && Object.entries(filter).flat()) || []),
  ]) // eslint-disable-line react-hooks/exhaustive-deps

  return { pending, error, result, page: Number(page), limit, refresh }
}
