import * as React from 'react'

import { ErrorSection } from 'ui/page'

import { LoaderError } from './use-loader-state'

export const LoaderErrorHandler: React.FC<{
  error: LoaderError
}> = ({ error }) => (
  <ErrorSection className="loader-error" error={error}>
    {error}
  </ErrorSection>
)
