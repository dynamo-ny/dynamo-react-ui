import { useLoader } from 'ui/loader/use-loader'
import { LoaderState } from 'ui/loader/use-loader-state'

import { storage } from 'utils/storage'

export function usePersistentLoader<T>(
  key: string,
  fn: (...any) => Promise<T>,
  ...args: Array<any>
): LoaderState<T> {
  const cachedFn = function (...args): Promise<T> {
    const cachedData = storage.read(key)
    if (cachedData) {
      return Promise.resolve(cachedData)
    }
    return fn(...args).then((result) => {
      storage.write(key, result)
      return result
    })
  }

  return useLoader<T>(cachedFn, ...args)
}
