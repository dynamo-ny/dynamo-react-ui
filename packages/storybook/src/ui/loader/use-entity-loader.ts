/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from 'react'

import { useLoaderState, LoaderError } from './use-loader-state'

export type GetEntityEndpoint<T> = (uuid: string) => Promise<T>

type EntityLoaderResult<T> = {
  pending: boolean
  error?: LoaderError
  entity?: T | null
  setEntity: (T) => void
}

export function useEntityLoader<T>(get: GetEntityEndpoint<T>, uuid: string): EntityLoaderResult<T> {
  const [{ pending, error }, load] = useLoaderState({
    pending: !!uuid, // we need it only when the id is available
  })

  const [entity, setEntity] = useState(null)

  useEffect(() => {
    if (uuid) {
      load(get, uuid).then((result) => {
        setEntity(result)
      })
    } else {
      setEntity(null)
    }
  }, [uuid])

  return {
    pending,
    error,
    entity,
    setEntity,
  }
}
