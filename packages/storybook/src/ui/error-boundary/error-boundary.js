/* eslint-disable no-console */
import React from 'react'
import PropTypes from 'prop-types'

export class ErrorBoundary extends React.PureComponent {
  static getDerivedStateFromError(error) {
    if (error instanceof Error) {
      console.error(error)
      return { error: error.message }
    }
    if (typeof error === 'string') {
      return { error }
    }
    if (typeof error?.error !== 'string') {
      // Unknown error
      console.error(error)
      return { error: 'Something went wrong' }
    }
    return error
  }

  static propTypes = {
    component: PropTypes.func,
    onError: PropTypes.func,
  }

  state = {
    error: null,
  }

  componentDidUpdate(prevProps, prevState) {
    const { error } = this.state
    const { onError } = this.props
    if (onError && error && (!prevState || prevState.error !== error)) {
      onError(error)
    }
  }

  // componentDidCatch(error, errorInfo) {
  //   // console.error(errorInfo)
  //   // console.error(error)
  // }

  render() {
    const { children, component: Component } = this.props

    if (this.state.error) {
      return <Component error={this.state.error} />
    }
    return children
  }
}
