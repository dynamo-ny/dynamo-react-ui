import React from 'react'

import { ErrorSection } from 'ui/page/error-section'
import { PureContainer } from 'ui/page/page-content'

import { ErrorBoundary } from './error-boundary'

const Container = ({ children, ...props }) => (
  <PureContainer {...props}>
    <ErrorBoundarySection>{children}</ErrorBoundarySection>
  </PureContainer>
)

const ErrorPage = (props) => (
  <Container>
    <ErrorSection {...props} />
  </Container>
)

export const ErrorBoundarySection = (props) => <ErrorBoundary {...props} component={ErrorSection} />

export const ErrorBoundaryPage = (props) => <ErrorBoundary {...props} component={ErrorPage} />
