import { ListItems } from './unordered-list/unordered-list'
import { ListEmpty } from './empty/list-empty'
import { ListLoaderProps } from './list-load-to-props'
import { ListPagination } from './pagination/list-pagination'
import { ListLoadMore } from './load-more/list-load-more'

export const List = {
  Items: ListItems,
  Resolve: ListLoaderProps,
  Empty: ListEmpty,
  Pagination: ListPagination,
  LoadMore: ListLoadMore,
}
