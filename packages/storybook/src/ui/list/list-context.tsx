import * as React from 'react'

import { ListRefreshAction } from 'ui/loader/use-list-loader'

type ListContext<T> = {
  setPage: (number) => void
  refresh: ListRefreshAction<T>
  setOrder: (name: keyof T, order: 'ASC' | 'DESC') => void
  setLimit: (number) => void
}

const defaultContextValue: ListContext<any> = {
  setPage() {
    throw 'Not a list context'
  },
  refresh() {
    throw 'Not a list context'
  },
  setOrder() {
    throw 'Not a list context'
  },
  setLimit() {
    throw 'Not a list context'
  },
}

const Context = React.createContext(defaultContextValue)
const ContextProvider = Context.Provider

export const useListController = function <T>(): ListContext<T> {
  return React.useContext(Context)
}

type ListContextProps = {
  value: ListContext<any>
}

export const ListContextProvider: React.FC<ListContextProps> = ({ value, children }) => (
  <ContextProvider value={value}>{children}</ContextProvider>
)
