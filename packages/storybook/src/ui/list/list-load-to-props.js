/* eslint-disable react-hooks/exhaustive-deps */
import React, { Children, cloneElement, useState, useEffect, useCallback } from 'react'
import PropTypes from 'prop-types'

import { LoaderErrorHandler, useListLoader } from 'ui/loader'
import { Throbber } from 'ui/throbber'

import { ORDER } from 'const/types'

import { ListContextProvider } from './list-context'

/**
 * Calls the <endpoint> with a single argument -
 * object that contains the rest of props (excluding children):
 * endpoint({ limit, page })

 * For every child, it passes the following props:
 * items
 * totalItems,
 * pending,
 * error,
 * page,
 * limit,
 */
export const ListLoaderProps = ({
  endpoint,
  children,
  pendingComponent: Pending,
  errorComponent: Error,
  ...props
}) => {
  const [page, setPage] = useState(Number(props.page) ?? 1)
  const [order, setOrder] = useState(props.order ?? null)
  const [limit, setLimit] = useState(Number(props.limit) ?? 15)

  const setPageNumber = useCallback(
    (value) => {
      if (Number(value) !== page) {
        setPage(Number(value))
      }
    },

    [page],
  )
  const setLimitNumber = useCallback(
    (value) => {
      if (Number(value) !== limit) {
        setLimit(Number(value))
      }
    },
    [limit],
  )

  useEffect(() => {
    setPageNumber(props.page)
  }, [props.page])

  useEffect(() => {
    setLimitNumber(props.limit)
  }, [props.limit])

  const { pending, error, result, refresh } = useListLoader(endpoint, {
    ...props,
    page,
    order,
    limit,
  })

  if (pending && !result?.items?.length) return <Pending />
  if (error) return <Error error={error?.error || error} />

  const value = {
    pending,
    error,
    items: result?.items ?? null,
    totalItems: result?.totalItems ?? null,
    page,
    limit: Number(limit),
    order,
  }

  const contextValue = {
    setPage: setPageNumber,
    setOrder,
    refresh,
    setLimit,
  }

  return (
    <ListContextProvider value={contextValue}>
      {Children.map(children, (child) => child && cloneElement(child, value))}
    </ListContextProvider>
  )
}

ListLoaderProps.propTypes = {
  endpoint: PropTypes.func.isRequired,
  errorComponent: PropTypes.elementType,
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  order: PropTypes.objectOf(PropTypes.oneOf(Object.values(ORDER))),
  page: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  pendingComponent: PropTypes.elementType,
}

ListLoaderProps.defaultProps = {
  page: 1,
  limit: 15,
  pendingComponent: Throbber,
  errorComponent: LoaderErrorHandler,
}
