import React from 'react'

import { useLocation } from 'router'
import { getParamFromUrl } from 'utils/url'

/**
 * For every prop (<name>,<value>) of UseSearchParams,
 * it passes a property <name> to every child component.
 * It’s value equals the corresponding search parameter of current location.
 *
 * When there are no corresponding item in location, it passes the <value> given in properties.
 *
 * @example
 * <UseSearchParams page> // pass the page search param to child component
 *
 * @example
 * <UseSearchParams page={1}> // pass the page search param or pass 1 when there is no page in the current location
 */
export const WithSearchParams = ({ children, ...props }) => {
  const { search } = useLocation()
  const result = Object.fromEntries(
    Object.entries(props).map(([name, value]) => [
      name,
      getParamFromUrl(search, name, value === true ? undefined : value),
    ]),
  )

  return React.Children.map(children, (el) => React.cloneElement(el, result))
}
