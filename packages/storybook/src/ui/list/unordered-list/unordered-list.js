import './unordered-list.css'
import React from 'react'
import PropTypes from 'prop-types'

import { cn } from 'utils'

export const ListItems = ({
  as: ListComponent,
  center,
  className,
  cols,
  error,
  items,
  keyValue,
  limit,
  page,
  pending,
  render: Component,
  totalItems,
  ...props
}) =>
  error || !items ? (
    false
  ) : (
    <ListComponent
      {...props}
      className={cn(
        'unordered-list',
        cols && cols > 1 && `cols-${cols}`,
        pending && 'pending',
        center && 'center',
        className,
      )}
    >
      {items.map((item, i) => (
        <Component key={item[keyValue]} style={{ '--index': i }} item={item} />
      ))}
    </ListComponent>
  )

ListItems.propTypes = {
  as: PropTypes.elementType,
  center: PropTypes.bool,
  cols: PropTypes.number,
  error: PropTypes.any,
  items: PropTypes.PropTypes.arrayOf(PropTypes.object),
  keyValue: PropTypes.string,
  limit: PropTypes.number,
  page: PropTypes.number,
  pending: PropTypes.bool,
  render: PropTypes.elementType.isRequired,
  totalItems: PropTypes.number,
}

ListItems.defaultProps = {
  cols: 1,
  keyValue: 'uuid',
  as: 'ul',
  center: false,
}
