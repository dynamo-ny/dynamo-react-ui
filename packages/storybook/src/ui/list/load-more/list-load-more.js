import './list-load-more.css'
import React from 'react'
import PropTypes from 'prop-types'
import { Button } from '@dynamo-ny/react-form'

import { useListController } from '../list-context'

export const ListLoadMore = ({
  children,
  className,
  items,
  limit,
  extraItemsAmt,
  totalItems,
  pending,
  disabled,
}) => {
  const { setLimit } = useListController()
  const handleClick = () => {
    setLimit(limit + Number(extraItemsAmt))
  }
  return (
    <div className="load-more-container">
      {items.length < totalItems && (
        <Button className={className} wide onClick={handleClick} disabled={pending || disabled}>
          {children}
        </Button>
      )}
    </div>
  )
}

ListLoadMore.propTypes = {
  children: PropTypes.node,
  disabled: PropTypes.bool,
  extraItemsAmt: PropTypes.number,
  items: PropTypes.array,
  limit: PropTypes.number,
  // page: PropTypes.number,
  pending: PropTypes.bool,
  totalItems: PropTypes.number,
  // updateLocation: PropTypes.bool,
}
ListLoadMore.defaultProps = {
  extraItemsAmt: 15,
  children: 'Show More',
}
