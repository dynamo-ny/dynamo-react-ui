/* eslint-disable react-hooks/exhaustive-deps */
import './list-pagination.css'

import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { Button } from '@dynamo-ny/react-form'

import { fulfillUrlWithParam } from 'utils/url'
import IconPrev from 'icons/arrow-left.svg'
import IconNext from 'icons/arrow-right.svg'
import { cn } from 'utils'
import { useLocation, useHistory } from 'router'

import { useListController } from '../list-context'

const COLLAPSE = '…'
const Collapse = () => (
  <Button disabled theme="secondary" className="pagination-item collapse">
    {COLLAPSE}
  </Button>
)

// Number of page buttons a round (including collapse)
const PAGES_AROUND = 3

// [1, 2, 3, *4*] => false
// [1, 2, 3, 4, *5*] => false
// [1, _, 4, 5, *6*] => true
const hasCollapseBefore = (current) => current - PAGES_AROUND > 2

// [1, 2, *3*, 4, 5, 6] => false
// [1, 2, *3*, 4, 5, 6, 7] => false
// [1, 2, *3*, 4, 5, _, 8] => true
const hasCollapseAfter = (current, total) => current + PAGES_AROUND < total - 1

// [1, 2, 3, *4*, 5, 6, 7] => [2, 3, *4*, 5, 6]
// [1, 2, 3, 4, *5*, 6, 7, 8, 9] => [2, 3, 4, *5*, 6, 7, 8]
// [1, _, 4, 5, *6*, 7, 8, 9, 10] => [4, 5, *6*, 7, 8, 9]
// [1, _, 4, 5, *6*, 7, 8, _, 11] => [4, 5, *6*, 7, 8]
const getPageNumbers = (current, total) => {
  const result = []
  const start = Math.max(2, current - PAGES_AROUND + (hasCollapseBefore(current) ? 1 : 0))
  const end = Math.min(
    total - 1,
    current + PAGES_AROUND + (hasCollapseAfter(current, total) ? -1 : 0),
  )
  for (let page = start; page <= end; page++) {
    result.push(page)
  }
  return result
}

const PaginationItem = ({ isCurrent, page, handler }) => {
  const handlePageClick = (event) => handler(+event.target.value)
  return (
    <Button
      value={page}
      onClick={handlePageClick}
      disabled={isCurrent}
      className={cn('pagination-item page', isCurrent && 'current')}
      theme="secondary"
    >
      {page}
    </Button>
  )
}
PaginationItem.propTypes = {
  handler: PropTypes.func.isRequired,
  isCurrent: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
}

export const ListPagination = ({
  as: Component,
  className,
  totalItems,
  items,
  limit,
  page,
  updateLocation,
}) => {
  const location = useLocation()
  const history = useHistory()
  const { setPage } = useListController()
  const lastPage = Math.ceil(totalItems / limit)

  useEffect(() => {
    if (page > lastPage) {
      goTo(lastPage)
    }
  }, [lastPage, page])

  if (!totalItems || !items || !limit || items.length === totalItems) return false

  const goTo = (nextPage) => {
    updateLocation
      ? history.replace({
          ...location,
          search: fulfillUrlWithParam(location.search, 'page', nextPage),
        })
      : setPage(nextPage)
  }

  const next = () => goTo(page + 1)
  const prev = () => goTo(page - 1)

  const isFirst = page <= 1
  const isLast = page >= lastPage

  return (
    <Component className={cn('list-pagination', className)}>
      <Button
        theme="secondary"
        className="pagination-item prev"
        disabled={isFirst}
        onClick={prev}
        leftIcon={IconPrev}
      >
        Prev
      </Button>
      <PaginationItem page={1} handler={goTo} isCurrent={isFirst} />

      {hasCollapseBefore(page) && <Collapse />}

      {getPageNumbers(page, lastPage).map((number) => (
        <PaginationItem isCurrent={page === number} handler={goTo} page={number} key={number} />
      ))}

      {hasCollapseAfter(page, lastPage) && <Collapse />}

      {lastPage > 1 && <PaginationItem page={lastPage} handler={goTo} isCurrent={isLast} />}
      <Button
        theme="secondary"
        className="pagination-item next"
        disabled={isLast}
        onClick={next}
        rightIcon={IconNext}
      >
        Next
      </Button>
    </Component>
  )
}

ListPagination.propTypes = {
  as: PropTypes.elementType,
  items: PropTypes.array,
  limit: PropTypes.number,
  page: PropTypes.number,
  totalItems: PropTypes.number,
  updateLocation: PropTypes.bool,
}

ListPagination.defaultProps = {
  as: 'nav',
}
