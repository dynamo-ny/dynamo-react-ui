import React from 'react'
import PropTypes from 'prop-types'

import { PlaceHolder } from 'ui/placeholder'

export const ListEmpty = ({
  as: Component,
  children,
  pending,
  error,
  totalItems,
  items,
  ...props
}) => {
  if (pending || error || totalItems > 0) {
    return false
  }
  return <Component {...props}>{children}</Component>
}

ListEmpty.propTypes = {
  as: PropTypes.elementType,
  error: PropTypes.any,
  items: PropTypes.array,
  pending: PropTypes.bool,
  totalItems: PropTypes.number,
}

ListEmpty.defaultProps = {
  as: PlaceHolder,
}
