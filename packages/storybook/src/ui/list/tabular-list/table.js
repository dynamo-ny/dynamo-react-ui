/* eslint-disable react-hooks/exhaustive-deps */
import './table.css'
import PropTypes from 'prop-types'
import React from 'react'

import { useListController } from 'ui/list/list-context'

import { cn } from 'utils'
import { ORDER } from 'const/types'

import { useTableDefinition, TableConfigContext } from './table-config'

export const Table = ({ children, items, keyPropName, className, order, pending, theme }) => {
  const { cells, cols, addColumn } = useTableDefinition({ keyPropName })
  const { setOrder } = useListController()

  return (
    <>
      <TableConfigContext.Provider value={addColumn}>{children}</TableConfigContext.Provider>

      {Array.isArray(items) && items.length > 0 && (
        <table
          className={cn('table', pending && 'pending', theme === 'section' && 'section', className)}
        >
          <thead>
            <tr>{cols.map((renderColumnHeading) => renderColumnHeading({ order, setOrder }))}</tr>
          </thead>
          <tbody>
            {items.map((item, i) => (
              <tr key={item[keyPropName]} style={{ '--index': i }}>
                {cells.map((renderCell) => renderCell(item))}
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </>
  )
}
Table.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
  keyPropName: PropTypes.string.isRequired,
  order: PropTypes.objectOf(PropTypes.oneOf(Object.values(ORDER))),
  pending: PropTypes.bool,
  theme: PropTypes.oneOf(['section', 'default']),
}
Table.defaultProps = {
  keyPropName: 'uuid',
  theme: 'default',
}
