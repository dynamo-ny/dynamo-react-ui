import React from 'react'
import { StaticRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import { ExternalLink } from 'ui/link'
import { List } from 'ui/list'

import Delete from 'icons/close.svg'
import { ORDER } from 'const/types'

import { ExpandableTable, Column, Table } from '.'

const renderer = ({ expandContent }) => <div>{expandContent}</div>
renderer.propTypes = {
  expandContent: PropTypes.string,
}
const isExpandable = ({ expandContent }) => !!expandContent

const tableActions = {
  onDelete: action('Table.onDelete'),
}

const endpoint = ({ order }) => {
  const [orderKey, orderValue] = order ? Object.entries(order)[0] : []
  const compare = (a, b) => {
    if (new Date(a).toString() !== 'Invalid Date') {
      return new Date(a).getTime() - new Date(b).getTime()
    }
    if (typeof a === 'string') {
      return a.localeCompare(b)
    }
    return a - b
  }
  const items = [
    {
      uuid: 1,
      logo: 'https://via.placeholder.com/50',
      name: 'First Name',
      nick: 'First Nick',
      status: 'test1',
      amount: 100,
      url: 'https://www.google.com',
      expandContent: 'First Name Expandable Content',
      createdAt: 'Mon May 21 2020 18:43:43',
    },
    {
      uuid: 2,
      logo: 'https://via.placeholder.com/133',
      name: 'Second Name',
      nick: 'Second Nick',
      status: 'test2',
      amount: 1020,
      url: 'https://www.google.com',
      expandContent: 'Second Name Expandable Content',
      createdAt: 'Mon May 25 2020 18:43:43',
    },
    {
      uuid: 3,
      logo: 'https://via.placeholder.com/10',
      name: 'Third Name',
      nick: 'Third Nick',
      status: 'test3',
      amount: 11,
      url: 'https://www.google.com',
      expandContent: 'Third Name Expandable Content',
      createdAt: 'Mon May 27 2020 18:43:43',
    },
    {
      uuid: 4,
      logo: 'https://via.placeholder.com/70',
      name: 'Fourth Name',
      nick: 'Fourth Nick',
      status: 'test1',
      amount: 121,
      url: 'https://www.google.com',
      expandContent: 'Fourth Name Expandable Content',
      createdAt: 'Mon May 28 2020 18:43:43',
    },
    {
      uuid: 5,
      logo: 'https://via.placeholder.com/99',
      name: 'Fifth Name',
      nick: 'Fifth Nick',
      status: 'test2',
      amount: 12221,
      url: 'https://www.google.com',
      expandContent: 'Fifth Name Expandable Content',
      createdAt: 'Mon May 30 2020 18:43:43',
    },
  ]
  return new Promise((resolve) =>
    setTimeout(resolve, 100, {
      items: orderKey
        ? items.sort((item1, item2) => {
            return orderValue === ORDER.ASC
              ? compare(item1[orderKey], item2[orderKey])
              : compare(item2[orderKey], item1[orderKey])
          })
        : items,
      totalItems: 2,
    }),
  )
}

const testOptions = {
  test1: 'test 1',
  test2: 'test 2',
  test3: 'test 3',
}

storiesOf('Table', module).add('Expandable Table', () => {
  return (
    <StaticRouter>
      <List.Resolve endpoint={endpoint} order={{ name: ORDER.DEFAULT }}>
        <ExpandableTable expandedContent={renderer} isExpandable={isExpandable}>
          <Column name="logo" type="img" width="50" height="50" />
          <Column name="name" label="name" sortable />
          <Column name="nick" label="nick(center)" type="center" sortable />
          <Column
            name="status"
            type="option"
            options={testOptions}
            className="small"
            label="Status(option)"
            sortable
          />
          <Column name="amount" type="currency" label="Amount(currency)" sortable />
          <Column name="createdAt" type="date" label="Created(date,short)" month="short" sortable />
          <Column name="action-continue" type="action">
            {({ url }) => <ExternalLink href={url}>Go to Page</ExternalLink>}
          </Column>
          <Column name="remove" type="remove-icon" onClick={tableActions.onDelete} icon={Delete} />
        </ExpandableTable>
      </List.Resolve>
    </StaticRouter>
  )
})

storiesOf('Table', module).add('Table', () => (
  <StaticRouter>
    <List.Resolve endpoint={endpoint} order={{ name: ORDER.DEFAULT }}>
      <Table>
        <Column name="logo" type="img" width="50" height="50" />
        <Column name="name" label="name" sortable />
        <Column name="nick" label="nick(center)" type="center" sortable />
        <Column
          name="status"
          type="option"
          options={testOptions}
          className="small"
          label="Status(option)"
          sortable
        />
        <Column name="amount" type="currency" label="Amount(currency)" sortable />
        <Column name="createdAt" type="date" label="Created(date,short)" month="short" sortable />
        <Column name="action-continue" type="action">
          {({ url }) => <ExternalLink href={url}>Go to Page</ExternalLink>}
        </Column>
        <Column name="remove" type="remove-icon" onClick={tableActions.onDelete} icon={Delete} />
      </Table>
    </List.Resolve>
  </StaticRouter>
))
