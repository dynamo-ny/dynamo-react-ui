/* eslint-disable react-hooks/exhaustive-deps */
import './table.css'
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { IconButton } from '@dynamo-ny/react-form'

import { useListController } from 'ui/list/list-context'

import { cn } from 'utils'

import { useTableDefinition, TableConfigContext } from './table-config'

export const ExpandableTable = ({
  children,
  className,
  expandedContent,
  isExpandable,
  items,
  keyPropName,
  order,
  pending,
}) => {
  const { cells, cols, addColumn } = useTableDefinition({ keyPropName })
  const [expandedRows, setIsOpen] = useState({})
  const { setOrder } = useListController()

  const toggle = (event) => {
    const { value } = event.currentTarget
    setIsOpen({ ...expandedRows, [value]: !expandedRows[value] })
  }

  return (
    <>
      <TableConfigContext.Provider value={addColumn}>{children}</TableConfigContext.Provider>
      {Array.isArray(items) && items.length > 0 && (
        <table className={cn('table expandable', pending && 'pending', className)}>
          <thead>
            <tr>
              <th className="toggle" />
              {cols.map((renderColumnHeading) => renderColumnHeading({ setOrder, order }))}
            </tr>
          </thead>
          <tbody>
            {items.map((item) => (
              <React.Fragment key={item[keyPropName]}>
                <tr>
                  <td className="toggle">
                    {isExpandable(item) && (
                      <IconButton value={item[keyPropName]} onClick={toggle}>
                        {expandedRows[item[keyPropName]] ? '-' : '+'}
                      </IconButton>
                    )}
                  </td>
                  {cells.map((renderCell) => renderCell(item))}
                </tr>
                {expandedContent && (
                  <tr className={cn('expandable-row', expandedRows[item[keyPropName]] && 'open')}>
                    <td colSpan={cols.length + 1}>{expandedContent(item)}</td>
                  </tr>
                )}
              </React.Fragment>
            ))}
          </tbody>
        </table>
      )}
    </>
  )
}
ExpandableTable.propTypes = {
  expandedContent: PropTypes.func.isRequired,
  isExpandable: PropTypes.func.isRequired,
  items: PropTypes.array,
  keyPropName: PropTypes.string.isRequired,
  order: PropTypes.object,
  pending: PropTypes.bool,
}
ExpandableTable.defaultProps = {
  keyPropName: 'uuid',
  isExpandable: () => true,
  expandedContent: () => false,
}
