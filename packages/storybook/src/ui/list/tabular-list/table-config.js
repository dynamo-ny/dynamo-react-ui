/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useCallback, useContext, useLayoutEffect } from 'react'
import { Button } from '@dynamo-ny/react-form'

import { cn } from 'utils'
import IconOrderAsc from 'icons/table-order-asc.svg'
import IconOrderDesc from 'icons/table-order-desc.svg'
import { ORDER } from 'const/types'

const IconOrder = {
  [ORDER.ASC]: IconOrderAsc,
  [ORDER.DESC]: IconOrderDesc,
}

export const TableConfigContext = React.createContext()
export const useTableConfigContext = () => useContext(TableConfigContext)

export const useColumnConfig = (column) => {
  const addColumn = useTableConfigContext()
  useLayoutEffect(() => {
    addColumn(column)
  }, []) // eslint-disable-next-line react-hooks/exhaustive-deps
}

const createHeading = ({ label, name, className, type, sortable }) => {
  const key = ['col', name, className].filter(Boolean).join('-')

  // eslint-disable-next-line react/prop-types
  return function Heading({ setOrder, order }) {
    // example of order: { createdAt: "ASC" }

    const isCurrentOrder = name && order?.hasOwnProperty(name)
    const currentDirection = name && isCurrentOrder && order?.[name]
    const Icon = name && order && IconOrder[order[name]]

    const handleClick = () => {
      setOrder({
        [name]: isCurrentOrder
          ? currentDirection === ORDER.ASC
            ? ORDER.DESC
            : ORDER.ASC
          : ORDER.DEFAULT,
      })
    }

    return (
      <th key={key} className={cn(className, name, type, isCurrentOrder && 'current-order')}>
        {sortable ? (
          <Button theme="inline" className="sortable-button" onClick={handleClick}>
            {label}
            {isCurrentOrder && <Icon />}
          </Button>
        ) : (
          <span>{label}</span>
        )}
      </th>
    )
  }
}

const createCell = ({ renderer: Renderer, className, label, children, ...props }) => {
  const key = ['col', props.name, className].filter(Boolean).join('-')
  const cellProps = { key, ['data-col']: label, className: cn(className, props.name, props.type) }
  return function renderCell(item) {
    return (
      <td {...cellProps}>
        <Renderer {...item} />
      </td>
    )
  }
}

export const useTableDefinition = ({ keyPropName, initialCells = [], initialCols = [] }) => {
  const [cells, setCells] = useState(initialCells)
  const [cols, setCols] = useState(initialCols)

  const addColumn = useCallback((props) => {
    const allProps = { keyPropName, ...props }
    setCols((cols) => [...cols, createHeading(allProps)])
    setCells((cells) => [...cells, createCell(allProps)])
  }, [])

  return { cells, cols, addColumn }
}
