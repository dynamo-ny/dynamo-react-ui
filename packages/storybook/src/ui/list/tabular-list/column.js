/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { IconButton } from '@dynamo-ny/react-form'

import { LocalDate } from 'ui/local-date'
import { Money } from 'ui/money'
import { useListController } from 'ui/list/list-context'

import { getObjectDeepProperty } from 'utils/object'

import { useColumnConfig } from './table-config'

const createDefaultCell = ({ name }) => (item) => getObjectDeepProperty(item, name) ?? false

const createImgCell = ({ name, ...props }) => (item) => {
  const src = getObjectDeepProperty(item, name)
  return src ? <img {...props} src={src} /> : false
}

const createOptionCell = ({ name, options }) => (item) =>
  options[getObjectDeepProperty(item, name)] ?? false

const createDateCell = ({ name, ...props }) => (item) => {
  const value = getObjectDeepProperty(item, name)
  return (value && <LocalDate value={value} {...props} />) || false
}

const createCurrencyCell = ({ name, sortable, ...props }) => (item) => {
  const value = getObjectDeepProperty(item, name)
  return <Money amount={value || 0} {...props} />
}

const createRemoveIconCell = ({ name, ...props }) => (item) => {
  let { icon: Icon, onClick, disabled, ...restProps } = props
  const [pending, setPending] = useState(false)
  const { refresh } = useListController()

  const clickHandler = async () => {
    setPending(true)
    try {
      await onClick(item)
      await refresh()
    } catch (e) {
      setPending(false)
    }
  }
  return (
    <IconButton {...restProps} disabled={pending || disabled} onClick={clickHandler}>
      <Icon />
    </IconButton>
  )
}

const renderers = {
  img: createImgCell,
  option: createOptionCell,
  date: createDateCell,
  currency: createCurrencyCell,
  'remove-icon': createRemoveIconCell,
}

/**
 * By given props of <Column />, it returns a function to render a table cell.
 * The function will be called with the props of a list item (row)
 */
export const Column = ({ children, label, ...props }) => {
  const renderer =
    children || (renderers[props.type] && renderers[props.type](props)) || createDefaultCell(props)
  useColumnConfig({ ...props, label, renderer })
  return false
}

Column.propTypes = {
  icon: PropTypes.elementType,
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  options: PropTypes.object,
  sortable: PropTypes.bool,
  type: PropTypes.oneOf([
    'currency',
    'text',
    'date',
    'option',
    'img',
    'action',
    'center',
    'remove-icon',
  ]),
}
