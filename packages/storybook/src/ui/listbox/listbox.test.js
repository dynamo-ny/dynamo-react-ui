/*eslint-env jest*/
import React from 'react'
import { mount, render, shallow } from 'enzyme'

import { ListBox } from '.'

describe('<ListBox />', () => {
  describe('renders', () => {
    it('to match snapshot', () => {
      expect(
        shallow(
          <ListBox
            name="listbox"
            label="List box"
            options={{ a: 'Option', b: 'Option', c: 'Option' }}
          />,
        ),
      ).toMatchSnapshot()
    })
    it('wrapped in div', () => {
      const wrapper = render(
        <ListBox
          name="listbox"
          label="List box"
          options={{ a: 'Option', b: 'Option', c: 'Option' }}
        />,
      )
      expect(wrapper.is('div')).toEqual(true)
    })
    it('input control respects name, is not disabled nor read-only', () => {
      const wrapper = render(
        <ListBox
          name="listbox"
          label="List box"
          options={{ a: 'Option', b: 'Option', c: 'Option' }}
        />,
      )
      const input = wrapper.find('input')
      //TODO find name field
      expect(input.is(':disabled')).toBe(false)
      expect(input.is('[readOnly]')).toBe(false)
    })
  })
  describe('props', () => {
    it('label: outputs label text', () => {
      const wrapper = render(
        <ListBox name="listbox" label="Test" options={{ a: 'Option', b: 'Option', c: 'Option' }} />,
      )
      expect(wrapper.text()).toEqual(expect.stringContaining('Test'))
    })
    it('disabled: passed to input', () => {
      const wrapper = render(
        <ListBox
          name="listbox"
          label="Test"
          disabled
          options={{ a: 'Option', b: 'Option', c: 'Option' }}
        />,
      )
      expect(wrapper.find('input').is(':disabled')).toBe(true)
    })
    it('readOnly: passed to input', () => {
      const wrapper = render(
        <ListBox
          name="listbox"
          label="Test"
          readOnly
          options={{ a: 'Option', b: 'Option', c: 'Option' }}
        />,
      )
      expect(wrapper.find('input').is('[readOnly]')).toBe(true)
    })
    describe('methods', () => {
      it('instance has "value" getter & setter', () => {
        //TODO take a look on this part
        const props = {
          defaultValue: 'a',
          name: 'test',
          label: 'Test',
          options: { a: 'Option', b: 'Option', c: 'Option' },
        }
        const wrapper = mount(<ListBox {...props} />)
        expect(wrapper.instance().getValue()).toEqual('a')
        wrapper
          .find('input')
          .at(1)
          .simulate('change', { target: { value: 'b', checked: true } })
        expect(wrapper.instance().getValue()).toEqual('b')
      })
    })
  })
  describe('on prop update', () => {
    it('defaultValue: value=defaultValue', () => {
      // eslint-disable-next-line react/display-name
      // eslint-disable-next-line react/prop-types
      const Tst = ({ defaultValue }) => (
        <ListBox
          name="listbox"
          label="List box"
          options={{ a: 'Option', b: 'Option', c: 'Option' }}
          defaultValue={defaultValue}
        />
      )
      const wrapper = mount(<Tst defaultValue="a" />)
      wrapper
        .find('input')
        .at(1)
        .simulate('change', { target: { value: 'b', checked: true } })
      wrapper.setProps({ defaultValue: 'c' })
      expect(wrapper.find('ListBox').instance().getValue()).toEqual('c')
    })
  })
})
