/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import './listbox.css'
import React from 'react'
import T from 'prop-types'

// import { LabelText } from 'ui/field/label'

import {
  firstElementSibling,
  lastElementSibling,
  nextElementSibling,
  previousElementSibling,
  focusElement,
} from 'utils/dom'

const KEYS = {
  FIRST: ['Home'],
  LAST: ['End'],
  NEXT: ['Down', 'ArrowDown'],
  PREVIOUS: ['Up', 'ArrowUp'],
  SELECT: [' ', 'Spacebar', 'Enter'],
}
const ANY_KEY = [].concat(...Object.values(KEYS))

export class ListBox extends React.PureComponent {
  static propTypes = {
    defaultValue: T.string,
    disabled: T.bool,
    label: T.node,
    name: T.string.isRequired,
    onChange: T.func,
    onKeyDown: T.func,
    options: T.object,
    readOnly: T.bool,
    required: T.bool,
    tabIndex: T.number,
  }

  static defaultProps = {
    required: false,
    readOnly: false,
    disabled: false,
    tabIndex: 0,
  }

  state = {
    value: this.props.defaultValue === undefined ? null : this.props.defaultValue,
  }

  optionsElement = React.createRef()

  handleOptionKeyDown = (event) => {
    if (!ANY_KEY.includes(event.key)) {
      this.props.onKeyDown && this.props.onKeyDown(event)
      return
    }
    if (KEYS.SELECT.includes(event.key)) {
      this.setValue(event.target.dataset.value)
    } else if (KEYS.NEXT.includes(event.key)) {
      focusElement(nextElementSibling(event.target))
    } else if (KEYS.PREVIOUS.includes(event.key)) {
      focusElement(previousElementSibling(event.target))
    } else if (KEYS.FIRST.includes(event.key)) {
      focusElement(firstElementSibling(event.target))
    } else if (KEYS.LAST.includes(event.key)) {
      focusElement(lastElementSibling(event.target))
    }
    event.preventDefault()
    event.stopPropagation()
  }

  onChange = (event) => {
    const { value, checked } = event.target
    checked &&
      value !== this.state.value &&
      this.setState({ value }, () => {
        this.props.onChange && this.props.onChange({ target: { value, name: this.props.name } })
      })
  }

  /** @public */
  focus() {
    /** @type {HTMLElement} */
    const element = this.optionsElement.current
    if (!element) return
    if (this.state.value === undefined || this.state.value === null) {
      focusElement(element.firstElementChild)
    } else {
      const query = `input[value="${this.state.value || ''}"]`
      const target = element.querySelector(query)
      focusElement(target && target.parentNode)
    }
  }

  /** @public */
  setValue(value) {
    this.setState({ value })
  }

  /** @public */
  getValue() {
    return this.state.value
  }

  /** @public */
  getLabel(value = this.getValue()) {
    return (this.props.options && this.props.options[value]) || undefined
  }

  /** @public */
  isValid(value = this.getValue()) {
    return !this.props.readOnly && !this.props.disabled && this.props.required ? !!value : true
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps &&
      prevProps.defaultValue !== this.props.defaultValue &&
      this.props.defaultValue !== this.state.value
    ) {
      this.setValue(this.props.defaultValue)
    }
  }

  render() {
    const { options, required, readOnly, disabled, tabIndex, className, label } = this.props
    const labelId = label ? [name, 'label-text'].join('-') : undefined

    return (
      <div className={className}>
        {(label && <label id={labelId}>{label}</label>) || false}
        <div
          className="listbox"
          ref={this.optionsElement}
          role="listbox"
          aria-required={!disabled && !readOnly && required}
          aria-readonly={disabled || readOnly}
          aria-labelledby={labelId}
        >
          {(options &&
            Object.entries(options).map(([value, label]) => {
              const checked = value === this.state.value
              return (
                <label
                  aria-selected={checked}
                  className="option"
                  data-value={value || ''}
                  key={value}
                  onKeyDown={this.handleOptionKeyDown}
                  tabIndex={checked ? tabIndex : -1}
                >
                  <input
                    type="radio"
                    name={name}
                    value={value}
                    onChange={this.onChange}
                    checked={checked}
                    tabIndex="-1"
                    readOnly={readOnly}
                    disabled={disabled}
                  />
                  <span className="option-text">{label}</span>
                </label>
              )
            })) ||
            false}
        </div>
      </div>
    )
  }
}
