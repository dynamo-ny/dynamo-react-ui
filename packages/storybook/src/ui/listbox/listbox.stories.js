import React from 'react'
import { storiesOf } from '@storybook/react'

import { ListBox } from '.'

const wrapperStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  margin: 'auto',
}

storiesOf('Listbox', module)
  .add('Listbox', () => (
    <div style={wrapperStyle}>
      <ListBox
        name="listbox"
        label="List box"
        options={{ a: 'Option', b: 'Option', c: 'Option' }}
      />
    </div>
  ))
  .add('Listbox default value - option 1', () => (
    <div style={wrapperStyle}>
      <ListBox
        name="listbox"
        label="List box"
        options={{ a: 'Option', b: 'Option', c: 'Option' }}
        defaultValue="a"
      />
    </div>
  ))
  .add('Listbox readonly', () => (
    <div style={wrapperStyle}>
      <ListBox
        name="listbox readonly"
        label="List box readonly"
        options={{ a: 'Option', b: 'Option', c: 'Option' }}
        readOnly
      />
    </div>
  ))
  .add('Listbox disabled', () => (
    <div style={wrapperStyle}>
      <ListBox
        name="listbox disabled"
        label="List box readonly"
        options={{ a: 'Option', b: 'Option', c: 'Option' }}
        disabled
      />
    </div>
  ))
