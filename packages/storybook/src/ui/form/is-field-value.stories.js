import React from 'react'
import { withKnobs, text, boolean, array } from '@storybook/addon-knobs'
import { TextField, IsFieldValue, Form } from '@dynamo-ny/react-form'

import notes from './is-field-value.md'

export default {
  component: IsFieldValue,
  title: 'Form|IsFieldValue',
  decorators: [withKnobs],
  parameters: { notes },
}

export const is_field_value = () => (
  <Form>
    <TextField label="Demo Field" name="field" defaultValue="win" />
    <IsFieldValue
      name="field"
      not={boolean('not', false)}
      blank={boolean('blank', false)}
      equals={text('equals', 'win')}
      oneOf={array('oneOf', ['win', 'lose'])}
    >
      <h3>content is visible</h3>
    </IsFieldValue>
  </Form>
)
