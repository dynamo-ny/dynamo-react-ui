import React from 'react'
import { action } from '@storybook/addon-actions'
import { SubmitButton, TextField, AddMatchFieldValidation, Form } from '@dynamo-ny/react-form'

export default {
  component: AddMatchFieldValidation,
  title: 'Form|Confirmation',
}

export const is_field_modified = () => (
  <Form onSubmit={action('submit')}>
    <p>The bottom field has a validation error when values are not equal</p>
    <TextField label="Demo Field" name="field" defaultValue="win" />
    <AddMatchFieldValidation name="field" label="Demo Field">
      <TextField label="Confirm Demo Field" name="_fieldConfirm" />
    </AddMatchFieldValidation>
    <footer>
      <SubmitButton>Submit</SubmitButton>
    </footer>
  </Form>
)
