import React from 'react'
import { IsModified, Form, ResetButton, TextField } from '@dynamo-ny/react-form'

export default {
  component: IsModified,
  title: 'Form|IsModified',
  // parameters: { notes },
}

export const is_modified = () => (
  <Form>
    <p>modify the input value</p>
    <TextField label="Field" name="field" />
    <IsModified>
      <h3>form is modified - content is visible</h3>
    </IsModified>
    <footer>
      <ResetButton>reset</ResetButton>
    </footer>
  </Form>
)
