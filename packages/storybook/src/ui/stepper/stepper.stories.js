import React from 'react'

import { Router } from 'router'

import { Stepper } from '.'

export default {
  component: Stepper,
  title: 'Navigation|Stepper',
  // decorators: [withKnobs],
}

export const array_of_paths = () => (
  <div style={{ minWidth: '100%' }}>
    <Router>
      <Stepper
        items={[
          'link/to/step/1',
          'link/to/step/2',
          'link/to/step/3',
          'link/to/step/4',
          'link/to/step/5',
        ]}
      />
    </Router>
  </div>
)

export const object_of_strings = () => (
  <div style={{ minWidth: '100%' }}>
    <Router>
      <Stepper
        items={{
          'link/to/step/1': 'one',
          'link/to/step/2': 'two',
          'link/to/step/3': 'three',
          'link/to/step/4': 'four',
          'link/to/step/5': 'five',
        }}
      />
    </Router>
  </div>
)

export const array_of_objects = () => (
  <div style={{ minWidth: '100%' }}>
    <Router>
      <Stepper
        items={[
          { path: 'link/to/step/1', label: 'one' },
          { path: 'link/to/step/2', label: 'two' },
          { path: 'link/to/step/3', label: 'three' },
          { path: 'link/to/step/4', label: 'four', fade: true },
          { path: 'link/to/step/5', label: 'five', fade: true },
        ]}
      />
    </Router>
  </div>
)
