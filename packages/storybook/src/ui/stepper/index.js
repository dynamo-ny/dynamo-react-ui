import './stepper.css'
import React from 'react'
import PropTypes from 'prop-types'

import { NavLink } from 'ui/link'

import { cn } from 'utils'
import Check from 'icons/check.svg'
import { RoutePath } from 'const/types'

const Step = ({ index, complete, current, disable, path, label }) => (
  <li className={cn('step', current && 'current', complete && 'complete', disable && 'disable')}>
    <NavLink to={path} disable={disable}>
      <span className="bullet">
        {complete && <Check className="icon" />}
        <span className="index">{index + 1}</span>
      </span>
      <span className="label">{label}</span>
    </NavLink>
  </li>
)
Step.propTypes = {
  complete: PropTypes.bool,
  current: PropTypes.bool,
  disable: PropTypes.bool,
  index: PropTypes.number,
  label: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
}

export const Stepper = ({ current, items, className }) => {
  if (!items) return false

  const itemsArray = Array.isArray(items)
    ? items.map((item, index) =>
        typeof item === 'string' ? { path: item, label: `Step\u00A0${index + 1}` } : item,
      )
    : Object.entries(items).map(([path, label]) => ({ path, label }))
  return (
    <nav className={cn('stepper', className)}>
      <div className="line" />
      <ol>
        {itemsArray.map(({ path, label, fade }, index) => (
          <Step
            index={index}
            label={label}
            current={index === current}
            complete={current > index}
            disable={!!fade}
            key={path + label}
            path={path}
          />
        ))}
      </ol>
    </nav>
  )
}

{
  /* <Check />
<span className="number">{index + 1}</span> */
}

Stepper.propTypes = {
  current: PropTypes.number.isRequired,
  items: PropTypes.oneOfType([
    PropTypes.arrayOf(RoutePath),
    PropTypes.objectOf(PropTypes.node),
    PropTypes.arrayOf(
      PropTypes.exact({
        label: PropTypes.node,
        path: RoutePath,
        fade: PropTypes.bool,
        index: PropTypes.number,
      }),
    ),
  ]),
}
