/**
 * @param {Object} object
 * @param {String} path
 */
export const getObjectDeepProperty = (object, path) => {
  const [name, restPath] = path.split('.', 2)
  if (!restPath) return object[name]
  return getObjectDeepProperty(object[name], restPath)
}
