import {
  getParamFromUrl,
  fulfillUrlWithParam,
  getAllParamsFromUrl,
  fulfillUrlWithParameters,
} from './url'

describe('Utils from utils/url.js', () => {
  describe('getParamFromUrl()', () => {
    it("'', 'name' -> null", () => {
      expect(getParamFromUrl('', 'name')).toBeNull()
    })
    it("'?foo=4&bar=5', 'name'-> null", () => {
      expect(getParamFromUrl('?foo=4&bar=5', 'name')).toBeNull()
    })
    it("'?name=1', 'name' -> '1'", () => {
      expect(getParamFromUrl('?name=1', 'name')).toEqual('1')
    })
    it("'?foo=foo&name=&bar=bar', 'name' -> '1'", () => {
      expect(getParamFromUrl('?foo=foo&name=name&bar=bar', 'name')).toEqual('name')
    })
  })
  describe('getAllParamsFromUrl()', () => {
    it("'', -> {}", () => {
      expect(getAllParamsFromUrl('')).toEqual({})
    })
    it("'?foo=4&bar=5', -> { foo: 4, bar: 5 }", () => {
      expect(getAllParamsFromUrl('?foo=4&bar=5')).toEqual({ foo: '4', bar: '5' })
    })
  })

  describe('fulfillUrlWithParam()', () => {
    it("'from blank url', 'name', null -> '?'", () => {
      expect(fulfillUrlWithParam('', 'name', null)).toEqual('?')
    })
    it('from blank url', () => {
      expect(fulfillUrlWithParam('', 'name', 'test')).toEqual('?name=test')
    })
    it('from url with other param', () => {
      expect(fulfillUrlWithParam('?foo=bar', 'name', 'test')).toEqual('?foo=bar&name=test')
    })
    it('from url with the same param', () => {
      expect(fulfillUrlWithParam('?name=bar', 'name', 'test')).toEqual('?name=test')
    })
    it('from url with the same param', () => {
      const args = ['?foo=1&name=bar&bar=foo', 'name', 'test']
      expect(fulfillUrlWithParam(...args)).toEqual(expect.stringContaining('name=test'))
      expect(fulfillUrlWithParam(...args)).toEqual(expect.stringContaining('bar=foo'))
      expect(fulfillUrlWithParam(...args)).toEqual(expect.stringContaining('foo=1'))
    })
  })

  describe('fulfillUrlWithParameters', () => {
    it("'from blank url', 'params = {name: null}' -> '?'", () => {
      expect(fulfillUrlWithParameters('', { name: null })).toEqual('?')
    })
    it("'from blank url', 'params = {name: test}' -> '?name=test'", () => {
      expect(fulfillUrlWithParameters('', { name: 'test' })).toEqual('?name=test')
    })
    it("'from url with other param', 'params = {name: test}' -> '?foo=bar&name=test'", () => {
      expect(fulfillUrlWithParameters('?foo=bar', { name: 'test' })).toEqual('?foo=bar&name=test')
    })
    it("'from url with the same param', 'params = {name: test}' -> '?name=test'", () => {
      expect(fulfillUrlWithParameters('?name=bar', { name: 'test' })).toEqual('?name=test')
    })
    it('from url with the same param', () => {
      const args = ['?foo=1&name=bar&bar=foo', { name: 'test' }]
      expect(fulfillUrlWithParameters(...args)).toEqual(expect.stringContaining('name=test'))
      expect(fulfillUrlWithParameters(...args)).toEqual(expect.stringContaining('bar=foo'))
      expect(fulfillUrlWithParameters(...args)).toEqual(expect.stringContaining('foo=1'))
    })
  })
})
