export const ORDER = {
  ASC: 'ASC',
  DESC: 'DESC',
}

export const SEARCH_PARAM = 'search'
export const LIMIT_PARAM = 'limit'

export const DEFAULT_LIMIT = 15
export const DEFAULT_PAGE = 1
export const OFFSET_PARAM = 'page'
