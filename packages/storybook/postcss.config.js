const path = require('path')
const importFrom = [path.resolve(__dirname, 'src/theme.css')]

module.exports = {
  plugins: [
    /**
     * @see https://preset-env.cssdb.org/features
     */
    require('postcss-preset-env')({ stage: 1 }),
    require('postcss-image-set-function')(),
    require('postcss-custom-media')({ importFrom }),
    require('postcss-custom-properties')({ importFrom }),
    require('postcss-nested')(),
  ],
}
