//eslint-env node
const path = require('path')

module.exports = {
  root: false,
  settings: {
    'import/resolver': {
      node: { paths: [path.resolve(__dirname, 'src')] },
      typescript: {
        // alwaysTryTypes: true, // always try to resolve types under `<roo/>@types` directory even it doesn't contain any source code, like `@types/unist`
        project: '.',
      },
    },
  },
}
