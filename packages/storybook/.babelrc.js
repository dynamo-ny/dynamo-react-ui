module.exports = {
  plugins: [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-nullish-coalescing-operator',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-syntax-dynamic-import',
    'transform-inline-environment-variables',
    '@babel/plugin-proposal-numeric-separator',
  ],
  presets: [
    '@babel/preset-react',
    ['@babel/preset-env', { modules: false }], // webpack needs
  ],
  env: {
    test: {
      presets: [['@babel/preset-env', { targets: { node: 'current' } }]],
    },
  },
}
