// setup file
// eslint-disable-next-line import/no-unassigned-import
import 'regenerator-runtime/runtime'

import { configure } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'

configure({ adapter: new Adapter() })
