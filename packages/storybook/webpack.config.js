/* eslint-disable import/no-nodejs-modules, import/no-commonjs */
const path = require('path')

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin')
const ModuleNotFoundPlugin = require('react-dev-utils/ModuleNotFoundPlugin')
const TerserPlugin = require('terser-webpack-plugin')
const safePostCssParser = require('postcss-safe-parser')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const isProduction = process.env.NODE_ENV === 'production'
const isAnalyse = isProduction && process.argv.includes('--analyze')
const isProductionProfile = isProduction && process.argv.includes('--profile')

const paths = {
  src: path.resolve(__dirname, 'src'),
  output: path.resolve(__dirname, '.build'),
  public: path.resolve(__dirname, 'public'),
  modules: path.resolve(__dirname, 'node_modules'),
  html: path.resolve(__dirname, 'public/index.ejs'),
  main: path.resolve(__dirname, 'src/index.js'),
}
const jsSuffix = isProduction ? '.[hash:8]' : ''
const cssSuffix = isProduction ? '.[contenthash:8]' : ''
const filenames = {
  js: `[name]${jsSuffix}.js`,
  jsChunk: `[name]${jsSuffix}.chunk.js`,
  css: `[name]${cssSuffix}.css`,
  cssChunk: `[name]${cssSuffix}.chunk.css`,
}

const loaders = {
  babel: {
    loader: 'babel-loader',
    options: {
      sourceMap: true,
    },
  },
  ts: {
    loader: 'ts-loader',
  },
  style: {
    loader: 'style-loader',
  },
  css: {
    loader: 'css-loader',
    options: {
      sourceMap: true,
      importLoaders: 1,
      modules: false,
    },
  },
  postcss: {
    loader: 'postcss-loader',
    options: {
      sourceMap: true,
    },
  },
  svg: {
    /**
     * @see https://github.com/boopathi/react-svg-loader/tree/master/packages/react-svg-loader
     */
    loader: '@svgr/webpack',
  },
  file: {
    loader: 'file-loader',
    options: {
      name: '[path][name].[ext]',
      outputPath: 'assets/',
    },
  },
  raw: {
    loader: 'raw-loader',
  },
}

module.exports = {
  context: paths.src,

  // Stop compilation early in production
  bail: isProduction,

  mode: isProduction ? 'production' : 'development',

  devtool: isProduction ? 'source-map' : 'cheap-module-source-map',

  entry: [
    // Include an alternative client for WebpackDevServer. A client's job is to
    // connect to WebpackDevServer by a socket and get notified about changes.
    // When you save a file, the client will either apply hot updates (in case
    // of CSS changes), or refresh the page (in case of JS changes). When you
    // make a syntax error, this client will display a syntax error overlay.
    // Note: instead of the default WebpackDevServer client, we use a custom one
    // to bring better experience for Create React App users. You can replace
    // the line below with these two lines if you prefer the stock client:
    // require.resolve('webpack-dev-server/client') + '?/',
    // require.resolve('webpack/hot/dev-server'),
    !isProduction && require.resolve('react-dev-utils/webpackHotDevClient'),

    paths.main,
  ].filter(Boolean),

  output: {
    // The build folder.
    path: isProduction ? paths.output : undefined,
    // Add /* filename */ comments to generated require()s in the output.
    pathinfo: !isProduction,
    // There will be one main bundle, and one file per asynchronous chunk.
    // In development, it does not produce real files.
    filename: filenames.js,
    // There are also additional JS chunk files if you use code splitting.
    chunkFilename: filenames.jsChunk,
    // webpack uses `publicPath` to determine where the app is being served from.
    // It requires a trailing slash, or the file assets will get an incorrect path.
    // We inferred the "public path" (such as / or /my-project) from homepage.
    publicPath: '/',
    // Point sourcemap entries to original disk location (format as URL on Windows)
    devtoolModuleFilenameTemplate: isProduction
      ? (info) => path.relative(paths.src, info.absoluteResourcePath).replace(/\\/g, '/')
      : (info) => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/'),
  },

  resolve: {
    modules: [paths.src, 'node_modules'],
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
  },

  module: {
    strictExportPresence: true,

    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [loaders.ts],
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
      {
        test: /\.jsx?$/,
        include: [paths.src],
        use: [loaders.babel],
      },
      {
        test: /\.css$/,
        // include: [paths.src],
        use: [
          isProduction ? MiniCssExtractPlugin.loader : loaders.style,
          loaders.css,
          loaders.postcss,
        ],
      },

      {
        test: /\.(jpe?g|png)/,
        loader: 'responsive-loader',
        include: [path.resolve(paths.src, 'img')],
      },
      {
        test: /\.(gif|svg)$/,
        include: [path.resolve(paths.src, 'img')],
        use: [loaders.file],
      },
      {
        test: /\.svg$/,
        include: [path.resolve(paths.src, 'icons')],
        use: [loaders.svg],
      },
      {
        test: /\.html$/,
        include: [path.resolve(paths.src, 'content')],
        use: [loaders.raw],
      },
    ],
  },

  plugins: [
    // Cleans the output directory
    new CleanWebpackPlugin(),

    // This gives some necessary context to module not found errors, such as
    // the requesting resource.
    new ModuleNotFoundPlugin(paths.src),

    //
    !isProduction && new WatchMissingNodeModulesPlugin(paths.modules),

    // Create an interactive treemap visualization of the contents of all your bundles.
    isAnalyse &&
      new (require('webpack-bundle-analyzer').BundleAnalyzerPlugin)({
        analyzerMode: 'static',
      }),

    // extract CSS in production
    isProduction &&
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: filenames.css,
        chunkFilename: filenames.cssChunk,
      }),
  ].filter(Boolean),

  optimization: {
    minimize: isProduction,

    moduleIds: 'hashed',

    ...(isAnalyse && {
      namedModules: true,
      namedChunks: true,
      moduleIds: 'named',
      chunkIds: 'named',
    }),

    minimizer: [
      // This is only used in production mode
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          parser: safePostCssParser,
          map: {
            // `inline: false` forces the sourcemap to be output into a
            // separate file
            inline: false,
            // `annotation: true` appends the sourceMappingURL to the end of
            // the css file, helping the browser find the sourcemap
            annotation: true,
          },
        },
        cssProcessorPluginOptions: {
          preset: ['default', { minifyFontValues: { removeQuotes: false } }],
        },
      }),

      // This is only used in production mode
      new TerserPlugin({
        terserOptions: {
          parse: {
            // We want terser to parse ecma 8 code. However, we don't want it
            // to apply any minification steps that turns valid ecma 5 code
            // into invalid ecma 5 code. This is why the 'compress' and 'output'
            // sections only apply transformations that are ecma 5 safe
            // https://github.com/facebook/create-react-app/pull/4234
            ecma: 8,
          },
          compress: {
            ecma: 5,
            warnings: false,
            // Disabled because of an issue with Uglify breaking seemingly valid code:
            // https://github.com/facebook/create-react-app/issues/2376
            // Pending further investigation:
            // https://github.com/mishoo/UglifyJS2/issues/2011
            comparisons: false,
            // Disabled because of an issue with Terser breaking valid code:
            // https://github.com/facebook/create-react-app/issues/5250
            // Pending further investigation:
            // https://github.com/terser-js/terser/issues/120
            inline: 2,
          },
          mangle: {
            safari10: true,
          },
          // Added for profiling in devtools
          keep_classnames: isProductionProfile,
          keep_fnames: isProductionProfile,
          output: {
            ecma: 5,
            comments: false,
            // Turned on because emoji and regex is not minified properly using default
            // https://github.com/facebook/create-react-app/issues/2488
            ascii_only: true,
          },
        },
        sourceMap: true,
      }),
    ],

    // Automatically split vendor and commons
    // https://twitter.com/wSokra/status/969633336732905474
    // https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366
    splitChunks: {
      chunks: 'all',
      name: isAnalyse, // show names when analyzing only
      maxSize: 150 * 1024,
    },
    // Keep the runtime chunk separated to enable long term caching
    // https://twitter.com/wSokra/status/969679223278505985
    // https://github.com/facebook/create-react-app/issues/5358
    runtimeChunk: {
      name: (entrypoint) => `runtime-${entrypoint.name}`,
    },
  },

  performance: isAnalyse
    ? {
        hints: 'error',
        maxAssetSize: 150 * 1024,
        maxEntrypointSize: 150 * 1024,
        assetFilter: (assetFilename) =>
          assetFilename.endsWith('.js') || assetFilename.endsWith('.css'),
      }
    : undefined,

  stats: isAnalyse
    ? {
        excludeAssets: [/\.(map|jpg)$/],
        logging: 'warn',
        children: false,
      }
    : 'errors-only',
}
