const projectConfig = require('../webpack.config')

module.exports = async ({ config, mode }) => {
  // `mode` has a value of 'DEVELOPMENT' or 'PRODUCTION'
  // You can change the configuration based on that.
  // 'PRODUCTION' is used when building the static version of storybook.

  // Merge PostCSS plugins
  const postcssOptions = findPostCssOptions(config).options
  if (typeof postcssOptions.plugins === 'function') {
    const getPlugins = postcssOptions.plugins
    postcssOptions.plugins = () => [...getPlugins(), ...require('../postcss.config').plugins]
  }
  console.log('PostCSS exclude:', postcssOptions.exclude)
  postcssOptions.exclude = 'node_modules'

  // Remove SVG from rules
  const svgRule = config.module.rules.find((rule) => String(rule.test).includes('svg|'))
  svgRule.test = new RegExp(svgRule.test.source.replace(/svg\|?/, ''))
  // Push our SVG handler
  config.module.rules.push(...findRules(projectConfig, 'svg'))

  // add .tsx? rules
  config.module.rules.push(...findRules(projectConfig, 'tsx'))

  // Fix module resolution (allows importing like this: "ui/form")
  config.resolve.modules = Array.from(
    new Set([...projectConfig.resolve.modules, ...config.resolve.modules]),
  )

  // Fix module resolution (add missing *.ts, etc.)
  config.resolve.extensions = Array.from(
    new Set([...projectConfig.resolve.extensions, ...config.resolve.extensions]),
  )

  // storysource addon
  config.module.rules.push({
    test: /\.stories\.jsx?$/,
    loaders: [
      {
        loader: require.resolve('@storybook/source-loader'),
        options: {
          prettierConfig: require('../../../prettier.config'),
        },
      },
    ],
    enforce: 'pre',
  })

  return config
}

function findRules(config, str) {
  return config.module.rules.filter((rule) => {
    return rule && rule.test && String(rule.test).includes(str)
  })
}
function findPostCssOptions(config) {
  const cssRule = config.module.rules.find((rule) => {
    return rule && rule.test && String(rule.test).endsWith('.css$/')
  })
  return (
    cssRule &&
    cssRule.use &&
    cssRule.use.find(({ loader }) => {
      return loader && loader.match && loader.match(/postcss\-loader/)
    })
  )
}
