import '@dynamo-ny/react-form/dist/index.css'
import 'theme.css'
import React from 'react'
import { addDecorator, addParameters } from '@storybook/react'

const cssReq = require.context('!!raw-loader!../src', true, /.\.css$/)
const cssTokenFiles = cssReq
  .keys()
  .map((filename) => ({ filename, content: cssReq(filename).default }))

// const svgIconsReq = require.context('!!raw-loader!../src', true, /.\.svg$/);
// const svgIconTokenFiles = svgIconsReq
//   .keys()
//   .map(filename => ({ filename, content: svgIconsReq(filename).default }));

addParameters({
  designToken: {
    files: {
      css: cssTokenFiles,
      // svgIcons: svgIconTokenFiles
    },
  },
})

addDecorator((fn) => (
  <div
    style={{
      display: 'flex',
      placeContent: 'center',
      margin: 'auto',
    }}
  >
    {fn()}
  </div>
))
