# Reuseable Components

Join our [Slack Channel](slack://dynamodevelopment.slack.com/messages/CLW5RACEN)

## Installation

```x-sh
yarn
```

## Development

We use [Storybook](https://storybook.js.org) for creating & documenting UI components:

```x-sh
yarn start
```

We use [Jest](https://jestjs.io) for running tests:

```x-sh
yarn test
```

or with watch mode

```x-sh
yarn test --watch
```

- Format code with [Prettier](https://prettier.io/)
- Lint **JS** with [ES Lint](https://eslint.org/)
- Lint **CSS** with [Stylelint](https://stylelint.io/)

```x-sh
yarn stylelint:fix
yarn lint:fix
```
