/* eslint-disable import/no-nodejs-modules, import/no-commonjs */

const path = require('path')
const importFrom = [path.resolve(__dirname, 'packages/react-form/src/theme.css')]

module.exports = {
  extends: ['stylelint-config-recommended', 'stylelint-prettier/recommended'],

  plugins: [
    'stylelint-prettier',
    'stylelint-order',
    'stylelint-media-use-custom-media',
    'stylelint-declaration-block-no-ignored-properties',
    'stylelint-declaration-use-variable',
    'stylelint-use-nesting',
    'stylelint-value-no-unknown-custom-properties',
  ],

  rules: {
    'prettier/prettier': true,
    'comment-empty-line-before': 'never',
    'no-descending-specificity': null,
    'no-duplicate-selectors': null,
    'max-empty-lines': 1,
    'selector-type-case': 'lower',
    'shorthand-property-no-redundant-values': true,
    'plugin/declaration-block-no-ignored-properties': true,
    'sh-waqar/declaration-use-variable': [['font-size', { ignoreValues: ['inherit'] }]],
    'order/order': ['custom-properties', 'declarations'],
    'order/properties-alphabetical-order': true,
    'csstools/use-nesting': 'always',
    'csstools/media-use-custom-media': ['always', { importFrom, except: [/>/] }],
    'csstools/value-no-unknown-custom-properties': [true, { importFrom }],
  },
  ignoreFiles: ['node_modules', 'build', 'dist', '*.js'],
}
